package models

import controllers._
import play.api.libs.json._

/*
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo
 * Fecha:        24/04/14
 * Hora:         05:45 PM
 */
trait ModeloRecuperacion extends Utilitarios {
  // ======================================  Recuperacion  ======================================
  case class Recuperacion(correo: String)
  object Recuperacion {
    implicit lazy val recuperacionR: Reads[Recuperacion] = Json.reads[Recuperacion]
  }

  // ======================================  RecuperarClave  ======================================
  case class RecuperarClave(clave: String, id: ObjectId)
  object RecuperarClave {
    implicit lazy val recuperarClaveR: Reads[RecuperarClave] = Json.reads[RecuperarClave]
  }
}
