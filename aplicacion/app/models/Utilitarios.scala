package models

import org.bson.types.ObjectId
import play.api.libs.json._
import org.joda.time.DateTime
import com.ciriscr.base.util.Fechas

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        25/02/14
 * Hora:         11:12 AM
 */

trait Utilitarios {
  private lazy val objectIdW = new Writes[ObjectId]{
      def writes(o: ObjectId): JsValue = JsString(o.toString)
    } // new
  private lazy val objectIdR = new Reads[ObjectId] {
      def reads(jv: JsValue): JsResult[ObjectId] = JsSuccess(new ObjectId(jv.as[String]))
    } // new

  private lazy val datetimeW = new Writes[DateTime]{
      def writes(o: DateTime): JsValue = JsString(Fechas.ISO.aString(o))
    } // new
  private lazy val datetimeR = new Reads[DateTime]{
      def reads(json: JsValue): JsResult[DateTime] = JsSuccess(Fechas.ISO.aDateTime(json.as[String]))
    } // new
  implicit lazy val objectidF: Format[ObjectId] = Format(objectIdR, objectIdW)
  implicit lazy val datetimeF: Format[DateTime] = Format(datetimeR, datetimeW)
}

