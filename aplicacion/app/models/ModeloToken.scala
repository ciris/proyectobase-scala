package models

import controllers._
import controllers.util.Token
import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/11/14
 * Hora:         2:34 PM
 */

trait ModeloToken extends Utilitarios {
  case class TokenModelo(idUsuario: ObjectId, asistidos: List[ObjectId], fecha: DateTime) {
    lazy val entidad: Token = new Token(idUsuario, asistidos, fecha)
  }//class
  implicit class AToken(p: Token) {
    def toModelo: TokenModelo =
      TokenModelo(p.idUsuario, p.asistidos, p.fechaCaducidad)
  }

  object TokenModelo {
    implicit lazy val procedimientoModeloF: Format[TokenModelo] = Json.format[TokenModelo]
  }
}
