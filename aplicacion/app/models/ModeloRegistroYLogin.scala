package models

import play.api.libs.json.{Writes, Reads, Json, Format}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        25/02/14
 * Hora:         11:11 AM
 */

trait ModeloRegistroYLogin extends ModeloUsuario {
  case class Credenciales(correo: String, password: String, recordarPor: Int) extends Recordar
  implicit lazy val credencialesJsonR: Reads[Credenciales] = Json.reads[Credenciales]

  case class Logueado(token: String, nombre: String, apellidos: String, id: String, asistidos: List[UsuarioSimple])
  object Logueado {
    implicit lazy val usuarioSimple = UsuarioSimple.usuarioSimpleW
  }
  implicit lazy val logueadoJsonF: Writes[Logueado] = Json.writes[Logueado]

  case class Reautenticacion(password: String, recordarPor: Int) extends Recordar
  implicit lazy val reautenticacionJsonR: Reads[Reautenticacion] = Json.reads[Reautenticacion]

  trait Recordar {
    val recordarPor: Int
    private lazy val diasMaximos = 15
    def recordarDurante: Int = if (recordarPor < diasMaximos) recordarPor else diasMaximos
  }
//  implicit lazy val recordarJsonR: Reads[Recordar] = Json.reads[Recordar]
} //trait
