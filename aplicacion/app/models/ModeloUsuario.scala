package models

import controllers._
import play.api.libs.json._
import com.ciriscr.base.entidades.usuarios.{Configuracion, Usuario}
import com.ciriscr.base.logica.elastic.entidades.ESUsuario

/*
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        16/01/14
 * Hora:         09:37 AM
 */
trait ModeloUsuario extends Utilitarios {
  // ======================================  UsuarioSimple  ======================================
  case class UsuarioSimple(nombre: String,
                           apellidos: String,
                           cedula: String,
                           habilitado: Boolean,
                           id: ObjectId)
  object UsuarioSimple {
    implicit lazy val usuarioSimpleW: Writes[UsuarioSimple] = Json.writes[UsuarioSimple]
  }

  // ======================================  UsuarioRegistro  ======================================
  case class UsuarioRegistro(nombre: String,
                             apellidos: String,
                             password: String,
                             correo: String,
                             cedula: String,
                             id: Option[ObjectId]) {
    lazy val entidad: Usuario = {
      val temp = (id: ObjectId) => new Usuario(nombre, apellidos, correo, correo, password, cedula, _id = id)
      id.map(id => temp(id)).getOrElse(temp(new ObjectId))
    }
  }
  object UsuarioRegistro {
    implicit lazy val usuarioRegistroR: Reads[UsuarioRegistro] = Json.reads[UsuarioRegistro]
  }

  // ======================================  UsuarioModelo  ======================================
  case class UsuarioModelo(nombre: String,
                           apellidos: String,
                           correo: String,
                           passwordActual: Option[String],
                           passwordNuevo: Option[String],
                           cedula: String,
                           configuracion: Configuracion,
                           id: ObjectId) {
    lazy val entidad: Usuario = {
      Usuario(nombre, apellidos, correo, correo, obtenerClave, cedula, configuracion, _id = id)
    }

    def paraCambiarClave: Boolean = passwordActual.isDefined && passwordNuevo.isDefined

    private def obtenerClave: String = {
      if (paraCambiarClave) {
        passwordNuevo.get
      } else {
        Usuario.PassSinCambio
      }
    }
  }
  object UsuarioModelo {
    implicit lazy val usuarioModeloF: Format[UsuarioModelo] = Json.format[UsuarioModelo]
    implicit lazy val configuracionModeloF: Format[Configuracion] = Json.format[Configuracion]
  }

  // ========================================  ListasUsuarios  ========================================

  case class ListaUsuarioSimple(cantidad: Int, lista: List[UsuarioSimple])
  case class ListaUsuarioModelo(cantidad: Int, lista: List[UsuarioModelo])

  // ======================================  UsuarioConvertions  ======================================
  implicit class UsuarioConvertions(u: Usuario) {
    /**
     * Convierte un usuario al modelo de salida con sólo la información necesaria para la aplicación
     * @return UsuarioSalida con sólo la información necesaria
     */
    lazy val toSimple = UsuarioSimple(u.nombre, u.apellidos, u.cedula, u.habilitado, u._id)
    lazy val toModelo = UsuarioModelo(u.nombre, u.apellidos, u.correo, None, None, u.cedula, u.configuracion, u._id)
  }

  object ListaUsuarioSimple {
    implicit lazy val listaUsuarioSimpleW: Writes[ListaUsuarioSimple] = Json.writes[ListaUsuarioSimple]
  }

  object ListaUsuarioModelo {
    implicit lazy val listaUsuarioModeloW: Writes[ListaUsuarioModelo] = Json.writes[ListaUsuarioModelo]
  }

  implicit lazy val usuarioESW: Writes[ESUsuario] = Json.writes[ESUsuario]
}
