import com.ciriscr.base.util.ConBitacora
import play.api.GlobalSettings

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        19/12/13
 * Hora:         08:17 AM
 */
object Global extends GlobalSettings with ConBitacora {

  import play.api.Application
  import akka.actor.ActorSystem

  private lazy val sistema = ActorSystem("iniciador")

  override def onStart(app: Application): Unit = {
    info("Inicializando la aplicación.")
    info("Registrando el classloader de play con salat.")
    import play.api.Play.current
    import com.ciriscr.base.bd.conf.ContextoSalat
    ContextoSalat.registrarClassLoader(current.classloader)
    indexarMongo()
    super.onStart(app)
  } //def
  override def onStop(app: Application): Unit = {
    debug("Deteniendo aplicación.")
    sistema.shutdown()
    super.onStop(app)
  } //def

  private def indexarMongo() {
    import akka.actor.ActorRef
    import controllers.util.MongoIndexer
    import com.ciriscr.base.bd.usuarios.{UsuarioDAO, SolicitudRecuperarClaveDAO}
    import com.ciriscr.base.entidades.usuarios.{Usuario, SolicitudRecuperarClave}


    def actor: ActorRef = MongoIndexer()(sistema)
    info("iniciando el indexado de base de datos")
    debug("indexando usuarios")
    actor ! MongoIndexer.Index[Usuario, UsuarioDAO](UsuarioDAO)

    debug("indexando recuperacion de clave")
    actor ! MongoIndexer.Index[SolicitudRecuperarClave, SolicitudRecuperarClaveDAO](SolicitudRecuperarClaveDAO)
  }

}//object
