package controllers

import play.api.mvc._
import controllers.util.SinAutenticar

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        13/02/14
 * Hora:         12:00 PM
 */
object Aplicacion extends Controller {
  def servir(archivo: String): Action[AnyContent] = controllers.Assets.at(path="/public/htmls/", file="plantilla.html")

} //object
