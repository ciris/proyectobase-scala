package controllers

import scala.Some
import play.api.mvc._
import models.ModeloRecuperacion
import controllers.util.SinAutenticar
import controllers.util.CodigosHTTPPersonalizados._
import com.ciriscr.base.logica.pedidos.PedidosRecuperacionClave

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo
 * Fecha:        4/24/14
 * Hora:         5:43 PM
 */

object RecuperarClave extends Controller with ConBitacora with ModeloRecuperacion {
  private lazy val pedidos = new PedidosRecuperacionClave()

  /**
   * Agrega un nuevo pedido de recuparación de clave
   * @note  '''PUT''' /api/usuarios/recuperar
   * @return
   *         - '''OK''': si no existe el usuario
   *         - '''Timeout''': si el actor duró mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def recuperar: EssentialAction = SinAutenticar.async(parse.json){ implicit request =>
    aJSONFuturo[Recuperacion](request){ modelo =>
      recuperacionBasica {
        pedidos.pedirRecuperacion(modelo.correo) { b => if (b) Created else NotFound }
      } //recuperacionBasica
    } //aJSONFuturo
  } //def

  /**
   * Para verificar si una recuperacion existe
   * @note  '''GET''' /api/usuarios/recuperar/:id
   * @param idRecuperacion
   * @return
   *         - '''OK''': si existe la solicitud
   *         - '''NotFound''': en case de no existeir la solicitud
   *         - '''IdInvalido''' si el id enviado no es válido
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''Timeout''': si el actor duró mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def existeRecuperacion(idRecuperacion: String): Action[AnyContent] = SinAutenticar.async{ implicit request =>
    idRecuperacion.aObjectId match {
      case Some(id) =>
        recuperacionBasica {
          pedidos.existeRecuperacion(id) { b => if (b) Ok else NotFound }
        } // recuperacionBasica
      case None => Future.successful(IdInvalido)
    }
  } //def

  /**
   * para recuperar la clave
   * @note  '''POST''' /api/usuarios/recuperar (id, clave)
   * @return
   *         - '''OK''': si se recuperó la clave
   *         - '''BadRequest''': si hay algún problema con la información enviada
   *         - '''Timeout''': si el actor duró mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def recuperarClave: EssentialAction = SinAutenticar.async(parse.json){ implicit request =>
    aJSONFuturo[RecuperarClave](request){ modelo =>
      recuperacionBasica {
        if (modelo.clave.nonEmpty) {
          pedidos.recuperarClave(modelo.id, modelo.clave) {
            _ match {
              case Some(id) => Ok(id.toString)
              case None => InternalServerError
            }
          }
        } else Future.successful(BadRequest)
      } //recuperacionBasica
    } //aJSONFuturo
  } //def

}
