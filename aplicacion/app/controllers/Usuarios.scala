package controllers

import play.api.mvc._
import com.ciriscr.base.logica.pedidos.PedidosUsuarios
import play.api.libs.json.Json
import com.ciriscr.base.logica.servicios.ServicioAutenticacion.Autenticar
import controllers.util.CodigosHTTPPersonalizados._
import models.{ModeloRegistroYLogin, ModeloUsuario}
import controllers.util.{Autenticado, SinAutenticar, GestorSesion}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        14/02/14
 * Hora:         08:26 AM
 */

object Usuarios extends Controller with ConBitacora with ModeloRegistroYLogin with ModeloUsuario {
  private lazy val pedidos = new PedidosUsuarios()

  /**
   * Registro de usuarios
   * @note  '''PUT''' /api/usuarios/registrar
   * @return
   *         - '''OK''': en caso de que el registro haya sido exitoso
   *         - '''YaExiste''' si ya existe el usuario
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''BadRequest''': en caso de que haya algún error en la información enviada
   *         - '''Timeout''': en caso de que el actor durara mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def registrar: EssentialAction = SinAutenticar.async(parse.json){ implicit request =>
    aJSONFuturo[UsuarioRegistro](request){ modelo =>
      recuperacionBasica {
        info("Pedido de registro recibido")
        debug("buscar si está disponible el login")
        val fut = pedidos.buscarPorLogin(modelo.correo){ _.isEmpty }
        fut.flatMap(bool => {
          if (bool) {
            debug("guardando el usuario")
            pedidos.guardarUsuario(modelo.entidad){
              _ match{
                case Some(_) => Created
                case None => InternalServerError
              } //match
            } //pedido
          } else {
            debug("login ya existe")
            Future.successful(YaExiste)
          }
        })
      } //recuperacionBasica
    } //aJSONFuturo
  } //def

  /**
   * Para autentificar el usuario en el sistema
   * @note  '''POST''' /api/usuarios/autenticar
   * @return
   *         - '''OK(token, nombre, apellidos, id, asistidos)''': en caso de autentificación exitosa
   *         - '''Unauthorized''' en caso de fallar la autentificación
   *         - '''BadRequest''': en caso de que haya algún error en la información enviada
   *         - '''Timeout''': en caso de que el actor durara mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def autenticar: EssentialAction = SinAutenticar.async(parse.json){ implicit request =>
    aJSONFuturo[Credenciales](request){ cred =>
      recuperacionBasica{
        info("pedido de autentificación recibido")
        val futuro = pedidos.autenticar(cred.correo, cred.password){
          _ match {
            case Autenticar.Autenticado(u) =>
              val token = GestorSesion.agregarSesion(u, cred.recordarDurante)
              val asistidos = Future.sequence {
                u.configuracion.asistidos.par.map{ a =>
                  pedidos.buscar(a){ u => u.toList.map(_.toSimple)}
                }.toList
              }
              val asd = asistidos.map(_.flatten).map { us =>
                Ok(Json.toJson(Logueado(token, u.nombre, u.apellidos, u._id.toString, us)))
              }
              asd
            case Autenticar.Fallido() => Future.successful(Unauthorized)
            case Autenticar.NoEncontrado() => Future.successful(Unauthorized)
          } // match
        } //autenticar
        futuro.flatMap(f => f)
      } //recuperacionBasica
    } //aJSONFuturo
  } //def

  /**
   * Para revalidar el token a partir de uno caducado y la clave del usuario
   * @note  '''PUT''' /api/usuarios/revalidar
   * @return
   *         - '''OK(token, nombre, apellidos, id, asistidos)''': en caso de autentificación exitosa
   *         - '''Unauthorized''' en caso de fallar la autentificación
   *         - '''BadRequest''': en caso de que haya algún error en la información enviada
   *         - '''Timeout''': en caso de que el actor durara mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def reautenticar: EssentialAction = SinAutenticar.async(parse.json) { implicit request =>
    aJSONFuturo[Reautenticacion](request){ modelo =>
      recuperacionBasica {
        GestorSesion.valor(request).map{token =>
          val usuario = token.idUsuario
          val futuro = pedidos.revalidar(usuario, modelo.password) {
            _ match {
              case Some(Autenticar.Autenticado(u)) =>
                val token = GestorSesion.agregarSesion(u, modelo.recordarDurante)
                val asistidos = Future.sequence {
                  u.configuracion.asistidos.par.map{ a =>
                    pedidos.buscar(a){ u => u.toList.map(_.toSimple)}
                  }.toList
                }
                val asd = asistidos.map(_.flatten).map { us =>
                  Ok(Json.toJson(Logueado(token, u.nombre, u.apellidos, u._id.toString, us)))
                }
                asd
              case Some(Autenticar.Fallido()) => Future.successful(Unauthorized)
              case Some(Autenticar.NoEncontrado()) => Future.successful(InternalServerError)
              case _ => Future.successful(BadRequest)
            } // match
          } // revalidar
          futuro.flatMap(f => f)
        }.getOrElse(Future.successful(BadRequest)) // valor
      } // recuperacionBasica
    } // aJSONFuturo
  }

  /**
   * Verifica si el login de un usuario ya existe en el sistema
   * @note  '''GET''' /api/usuarios/verificarLogin?login=
   * @param login texto para verificar si existe
   * @return
   *         - '''OK''': si no existe el usuario
   *         - '''YaExiste''' si ya existe el usuario
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''Timeout''': si el actor duró mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def disponibilidadLogin(login: String): EssentialAction = SinAutenticar.async{ request =>
    recuperacionBasica{
      info("pedido de busqueda de login disponible ")
      pedidos.buscarPorLogin(login){ ou => if (ou.isEmpty) Ok else YaExiste }
    } //recuperacionBasica
  } //def

  /**
   * Para actualizar la información del perfil de usuario
   * @note  '''PUT''' /api/usuarios/actualizar
   * @return
   *         - '''OK''': con el id del usuario que se actualizó
   *         - '''NotFound''' en caso se que no se encotrara el usuario por el correo o la autentificación fallara
   *         - '''BadRequest''': en caso de que haya algún error en la información enviada
   *         - '''Timeout''': si el actor duró mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def actualizar: EssentialAction = Autenticado.async(parse.json){ implicit request =>
    aJSONFuturo[UsuarioModelo](request){ modelo =>
      recuperacionBasica {
        if (request.esUsuario(modelo.id)) {
          val act = (oid: Option[ObjectId]) => {
            oid match {
              case Some(id) => Ok(id.toString)
              case None => InternalServerError
            } // match
          } // act

          if (modelo.paraCambiarClave) {
            val oUsu = pedidos.autenticar(modelo.correo, modelo.passwordActual.get)(a => a)
            oUsu.flatMap(_ match {
              case Autenticar.Autenticado(usu) => pedidos.actualizarUsuarioConClave(modelo.entidad.copy(
                correo = usu.correo, login = usu.login))(act)
              case _ => Future.successful(NotFound)
            }) // match .. flatMap
          } else {
            val oUsu = pedidos.buscarPorLogin(modelo.correo)(x => x)
            oUsu.flatMap(_ match {
              case Some(usu) => pedidos.actualizarUsuarioSinClave(modelo.entidad.copy(
                password = usu.password, correo = usu.correo, login = usu.login))(act)
              case None => Future.successful(NotFound)
            }) // match .. flatMap
          } // if..else paraCambiarClave
        } else Future.successful(BadRequest)
      } //recuperacionBasica
    } //aJSONFuturo
  } //def

  /**
   * Devuelve la información del usuario actualmente logueado
   * @note  '''GET''' /api/usuarios/
   * @return
   *         - '''OK(usuario)''': el usuario actualmente logueado
   *         - '''NotFound''': en caso de que el no se encontrara el usuario activo
   *         - '''Timeout''': en caso de que el actor durara mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def obtener: EssentialAction = Autenticado.async{ implicit request =>
    recuperacionBasica {
      pedidos.buscar(request.usuario) {
        _.map(u => { Ok(Json.toJson(u.toModelo)) }).getOrElse(NotFound)
      } // pedido
    } // recuperacionBasica
  } // def

  /**
   * Busca usuarios por medio de su nombre
   * @note '''GET''' /api/usuarios/buscar?texto= &cantidad=
   * @param texto frase a buscar
   * @param cantidad default => 10. Cantidad de resultados a obtener
   * @return
   *         - '''OK(listaUsuariosES)''': los usuarios correspondientes a la busqueda
   *         - '''Timeout''': en caso de que el actor durara mucho
   *         (ver <a href="https://bitbucket.org/ciris/mediciris/wiki/Codigos%20HTTP%20personalizados">codigos personalizados</a>)
   *         - '''InternalServerError''': en caso se existir algún error inesperado
   */
  def buscar(texto: String, cantidad: Int): Action[AnyContent] = Autenticado.async{ implicit request =>
    recuperacionBasica{
      pedidos.buscarPorTexto(texto, cantidad){ res => Ok(Json.toJson(res)) }
    } // recuperacioBasica
  } // def

} //object
