package controllers.util

import com.ciriscr.base.entorno.ValoresEntorno
import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        08/05/14
 * Hora:         11:41 AM
 */
object Scripts extends ConBitacora{
  private val ambiente = ValoresEntorno.ambienteFrontEnd
  lazy val usarCompilados =  ambiente == "produccion"
  debug(s"Usando scripts compilados? $usarCompilados")
} //object
