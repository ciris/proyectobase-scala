package controllers.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/11/14
 * Hora:         3:45 PM
 */

private[util] object Failures {
  sealed trait Failure extends Throwable

  // Four types of possible failures here
  case object HeaderNoEncontrada extends Failure
  case object AuthorizationNoSoportada extends Failure
  case object AuthorizationDeprecada extends Failure
  case object TokenVencido extends Failure
}
