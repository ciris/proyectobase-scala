package controllers.util

import com.ciriscr.base.logica.util.{FabricaActoresSimple, ActorGenerico}
import com.ciriscr.base.bd.{DAOCompanion, CirisDAO}
import com.ciriscr.base.entidades.BaseAny

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        04/03/14
 * Hora:         03:37 PM
 */

private class ActorMongoIndexer extends ActorGenerico {
  import MongoIndexer._

  def recibir: Receive = {
    case Index(dao) =>
      dao.asegurarIndices()
      harakiri()
  }
}

object MongoIndexer extends FabricaActoresSimple {
  def clazz: Class[_] = classOf[ActorMongoIndexer]

  case class Index[A <: BaseAny, B <: CirisDAO[A]](dao: DAOCompanion[B])
}
