package controllers.util

import com.ciriscr.base.entorno.ValoresEntorno
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent._
import org.bson.types.ObjectId
import scala.util.Try
import org.joda.time.DateTime
import com.ciriscr.base.util.{ConBitacora, Fechas}
import CodigosHTTPPersonalizados._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        14/02/14
 * Hora:         10:24 AM
 */
trait UtilitariosControladores extends Results with ConBitacora {
  implicit val EC = play.api.libs.concurrent.Execution.Implicits.defaultContext

  def aJSON[A](request: Request[JsValue])(procesar: A => Result)
              (implicit rds : play.api.libs.json.Reads[A]): Result = {
    request.body.validate[A].fold(a => BadRequest(a.mkString(", ")), procesar)
  } //def

  def aJSONFuturo[A](request: Request[JsValue])(procesar: A => Future[Result])
                    (implicit rds : play.api.libs.json.Reads[A]): Future[Result] = {
    request.body.validate[A].fold(
      a => {
        if (ValoresEntorno.ambienteBackEnd == "DEBUG")
          debug(s"${a.mkString(", ")}")
        Future.successful(BadRequest(a.mkString(", ")))
      },
      v => procesar(v))
  } //def

  /**
   * Agrega dos recuperaciones en caso de que un futuro falle:
   * TimeoutException y cualquier otra excepción.
   * @param futuro: Future[Result]
   * @return Future[Result] (Timeout si el actor se cae, InternalServerError por cualquier otro error)
   */
  def recuperacionBasica(futuro: Future[Result]): Future[Result] = {
    futuro.recover{
      case e: TimeoutException => Timeout
      case e =>
        if (ValoresEntorno.ambienteBackEnd == "DEBUG")
          debug(s"${e.getMessage}\n\t${e.getStackTrace.mkString("\n\t")}")
        InternalServerError
    } //recover
  } //def

  implicit class StringObjectId(id: String){
    def aObjectId: Option[ObjectId] = {
      Try{new ObjectId(id)}.toOption
    } //def
  } //class

  implicit class StringDateTime(fecha: String){
    def aDateTime: Option[DateTime] = Try{Fechas.ISO.aDateTime(fecha)}.toOption
  } //def
} //trait
