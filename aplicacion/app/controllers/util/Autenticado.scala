package controllers.util

import org.jasypt.exceptions.EncryptionOperationNotPossibleException
import play.api.mvc.{Result, ActionBuilder, WrappedRequest, Request, Results}
import org.bson.types.ObjectId
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        19/12/13
 * Hora:         01:26 PM
 */
case class RequestAutenticado[A](token: Token, request: Request[A])
  extends WrappedRequest[A](request) {
  def esIdAsistido(id: ObjectId): Boolean = esUsuario(id) || token.asistidos.contains(id)
  def esUsuario(id: ObjectId): Boolean = id == token.idUsuario
  def usuario: ObjectId = token.idUsuario
}

object Autenticado extends ActionBuilder[RequestAutenticado] {

  def invokeBlock[A](request: Request[A], block: (RequestAutenticado[A]) => Future[Result]): Future[Result] = {
    import Failures._
    import Results._
    import CodigosHTTPPersonalizados._
    val futSR = GestorSesion.validar(request) match {
      case Success(token) => block(RequestAutenticado(token, request))
      case Failure(TokenVencido) => Future.successful(TokenInvalido)
      case Failure(AuthorizationDeprecada) => Future.successful(NotAcceptable)
      case Failure(HeaderNoEncontrada) => Future.successful(BadRequest)
      case Failure(AuthorizationNoSoportada) => Future.successful(AutenticacionIncorrecta)
      case Failure(e: EncryptionOperationNotPossibleException) => Future.successful(InternalServerError)
      case Failure(e) => Future.successful(Unauthorized)
    } // validar sesion
    futSR.map(_.withHeaders("X-Frame-Options" -> "deny", "X-Content-Type-Options" -> "nosniff"))
  } //def
} // object

object SinAutenticar extends ActionBuilder[Request] {
  def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
    block(request).map(_.withHeaders("X-Frame-Options" -> "deny", "X-Content-Type-Options" -> "nosniff"))
  }
}