package controllers.util

import com.ciriscr.base.entidades.usuarios.Usuario
import org.bson.types.ObjectId
import org.joda.time.DateTime

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/11/14
 * Hora:         2:22 PM
 */

class Token(val idUsuario: ObjectId, val asistidos: List[ObjectId], val fechaCaducidad: DateTime)

object Token {
  def apply(u: Usuario, fechaCaducidad: DateTime): Token = {
    new Token(u._id, u.configuracion.asistidos, fechaCaducidad)
  }
}
