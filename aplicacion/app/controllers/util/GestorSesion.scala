package controllers.util

import controllers._
import models.ModeloToken
import org.joda.time.DateTime
import play.api.libs.json.Json
import controllers.util.Failures._
import scala.util.{Success, Failure, Try}
import com.ciriscr.base.util.Fechas._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        04/01/14
 * Hora:         04:41 PM
 */
object GestorSesion extends ModeloToken {

  import play.api.mvc._
  import play.mvc.Http.HeaderNames._
  import com.ciriscr.base.entidades.usuarios.Usuario

  /**
   * genera un toquen para autentificar los pedidos futuros al sistema
   * @param usuario
   * @param request
   * @tparam A
   * @return
   */
  def agregarSesion[A](usuario: Usuario, cantidadDiasRecordar: Int)(implicit request: Request[A]): String = {
    val fechaCaducidad = DateTime.now().plusDays(cantidadDiasRecordar).inicioDeDia
    val token = Token(usuario, fechaCaducidad)
    Encriptor.encriptar(request.headers.get(USER_AGENT), Json.toJson(token.toModelo).toString())
  }

  /**
   * @param request
   */
  def invalidarSesion(implicit request: Request[AnyContent]): Boolean =
    true    //TODO implementar algún modo de saber si un token está invalido o no

  /**
   * Valida si el token es valido o no
   * @param request
   * @tparam A
   * @return Option[ObjectId] con el id del usuario conectado
   */
  def validar[A](implicit request: Request[A]): Try[Token] =
    valor(request).flatMap(t => if (t.fechaCaducidad.isAfterToday) Success(t) else Failure(TokenVencido) )

  def valor[A](implicit request: Request[A]): Try[Token] =
    desencriptarToken(request).map(t => Json.parse(t).as[TokenModelo].entidad)

  private def desencriptarToken[A](request: Request[A]): Try[String] = {
    request.headers.get(AUTHORIZATION).map{ s =>
      s.split(" ").toList match {
          case "JWT" :: (b: String) :: Nil =>
            Try { Encriptor.desencriptar(request.headers.get(USER_AGENT), b) }
          case "Basic" :: _ :: Nil =>
            Failure(AuthorizationDeprecada)
          case _ => Failure(AuthorizationNoSoportada)
      }
    }.getOrElse(Failure(HeaderNoEncontrada))
  }

  private object Encriptor {

    def encriptar(userAgent: Option[String], texto: String): String = crypter(userAgent).encrypt(texto)

    def desencriptar(userAgent: Option[String], cryptoTexto: String): String = crypter(userAgent).decrypt(cryptoTexto)

    private def crypter(userAgent: Option[String]) = {
      import org.jasypt.encryption.pbe.StandardPBEStringEncryptor

      val iterations: Int = 1783

      val stringEncriptor = new StandardPBEStringEncryptor()
      stringEncriptor.setPassword(userAgent.getOrElse("NoUserAgent"))
      stringEncriptor.setKeyObtentionIterations(iterations)
      stringEncriptor
    }
  }

}//object