package controllers.util

import play.api.mvc.Results._
/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        30/01/14
 * Hora:         09:20 AM
 */
trait CodigosHTTPPersonalizados {
  //      Códigos
  lazy val TIMEOUT = 530
  lazy val ID_INVALIDA= 470
  lazy val YA_EXISTE = 471
  lazy val TOKEN_INVALIDO = 472
  lazy val AUTENTICACION_INCORRECTA = 473


  //      Estados
  lazy val YaExiste = new Status(YA_EXISTE)
  lazy val Timeout = new Status(TIMEOUT)
  lazy val IdInvalido = new Status(ID_INVALIDA)
  lazy val TokenInvalido = new Status(TOKEN_INVALIDO)
  lazy val AutenticacionIncorrecta = new Status(AUTENTICACION_INCORRECTA)
} //trait

object CodigosHTTPPersonalizados extends CodigosHTTPPersonalizados