/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        14/02/14
 * Hora:         10:23 AM
 */
package object controllers
  extends com.ciriscr.base.logica.Imports
  with com.ciriscr.base.logica.Implicits
  with util.UtilitariosControladores {

  type ConBitacora = com.ciriscr.base.util.ConBitacora

  implicit val sistema = SistemaActores("SA-Mediciris")
  private lazy val tiempoMilis = 10
  implicit val timeout = Timeout(tiempoMilis, java.util.concurrent.TimeUnit.SECONDS)
} //package object
