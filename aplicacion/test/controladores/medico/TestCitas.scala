package controladores.medico

import play.api.test._
import controladores.BasicTest
import play.libs.Json

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/11/14
 * Hora:         6:10 PM
 */

class TestCitas extends PlaySpecification with BasicTest {

  private implicit val timeout = akka.util.Timeout(30000)
  private val idCitaValido = "5318cf2e44ae9fc55deb6409"
  private val idUsuarioValido = "52fbae48e4b04f46fafd8728"

  //obterner
  "prueba del servicio 'obtener' de citas con id de citas y usuario inválidos" in {
    val result = controllers.Citas.obtener("", "")(authRequest)

    status(result) must equalTo(ID_INVALIDA)
    contentType(result) must beNone
    charset(result) must beNone
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "prueba del servicio 'obtener' de citas con id de citas válida y usuario inválidos" in {
    val result = controllers.Citas.obtener(idCitaValido, "")(authRequest)

    status(result) must equalTo(ID_INVALIDA)
    contentType(result) must beNone
    charset(result) must beNone
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "prueba del servicio 'obtener' de citas con id de citas inválida y usuario válidos" in {
    val result = controllers.Citas.obtener("", idUsuarioValido)(authRequest)

    status(result) must equalTo(ID_INVALIDA)
    contentType(result) must beNone
    charset(result) must beNone
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "prueba del servicio 'obtener' de citas con id de citas y usuario válidos" in {
//    fileContent("../citas.json")

    val result = controllers.Citas.obtener(idCitaValido, idUsuarioValido)(authRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("application/json")
    charset(result) must beSome("utf-8")
    contentAsJson(result) must beEqualTo(Json.parse(fileContent("../citas.json")).)
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

}
