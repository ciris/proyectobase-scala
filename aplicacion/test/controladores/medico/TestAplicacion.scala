package controladores.medico

import play.api.test._
import scala.util.Random
import controladores.BasicTest

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/11/14
 * Hora:         4:11 PM
 */

class TestAplicacion extends PlaySpecification with BasicTest {

  //test de index
  "renderizado de 'index' con archivo vacío" in {
    val result = controllers.Aplicacion.index("")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo 'footer'" in {
    val result = controllers.Aplicacion.index("footer")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo 'recuperarIngreso'" in {
    val result = controllers.Aplicacion.index("recuperarIngreso")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo 'login'" in {
    val result = controllers.Aplicacion.index("login")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo 'navegacion'" in {
    val result = controllers.Aplicacion.index("navegacion")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo 'registro'" in {
    val result = controllers.Aplicacion.index("registro")(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  "renderizado de 'index' con archivo cualquier texto" in {
    val result = controllers.Aplicacion.index(Random.nextString(9))(simpleRequest)

    status(result) must equalTo(NOT_FOUND)
    contentType(result) must beNone
    charset(result) must beNone
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  //test de medico
  "renderizado de 'medico' con archivo cualquier texto" in {
    val result = controllers.Aplicacion.medico(Random.nextString(9))(simpleRequest)

    status(result) must equalTo(OK)
    contentType(result) must beSome("text/html")
    charset(result) must beSome("utf-8")
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

  //test de prueba
  "renderizado de 'prueba' con archivo cualquier texto" in {
    val result = controllers.Aplicacion.prueba(Random.nextString(9))(simpleRequest)

    status(result) must equalTo(NOT_FOUND)
    contentType(result) must beNone
    charset(result) must beNone
    header("X-Frame-Options", result) must beSome("deny")
    header("X-Content-Type-Options", result) must beSome("nosniff")
  }

}
