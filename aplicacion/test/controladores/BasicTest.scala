package controladores

import controllers.util.CodigosHTTPPersonalizados
import play.api.test.FakeRequest
import play.mvc.Http.HeaderNames._
import scala.io.Source
import scalax.io.Resource

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/11/14
 * Hora:         6:22 PM
 */

trait BasicTest extends CodigosHTTPPersonalizados {
  //tokens validos por 1000 días y solo para estas pruebas
  private val tokenUsuario = "snsOW9PiL8t56ejti0Z0FMqHWgIBs/VoUk0KQVLDn4NklPkVU0c1f/z0EyZR3OxGlg8ZR1bWQVASwV9nGTFCLFnS85ZGYR6N"
  private val tokenAsistente = "6pgvXNFIhx3c9/978KBoJJG/Iu2TXYspGcCQI2j5bbgkayaw391xnoKxe5mP/QS6DD0Fmdjdd36tju/BU73/bKMiMkdTjWwF9VomJAk5UJHjWlvpzyRwZ+xbEuXb2V0l"
  val authUsuario: String = s"Basic $tokenUsuario"
  val authAsistente: String = s"Basic $tokenAsistente"

  val authRequest = FakeRequest().withHeaders(AUTHORIZATION -> authUsuario)
  val simpleRequest = FakeRequest()

  def fileContent(filename: String): String = {
    import java.io.File
    val file = new File(filename)
    println(s"# file: ${file.getAbsolutePath}")
    Source.fromFile(file).getLines().mkString("\n")
  }
}
