/*global module*/
(function() {
  "use strict";
  module.exports = function(grunt) {

    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-requirejs");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-clean");

    var taskConfig = {
      pkg: grunt.file.readJSON("package.json"),
      less: {
        build: {
          files: {
            "public/css/<%= pkg.name %>.css": "public/less/main.less"
          }
        }
      },
      requirejs: {
        compile: {
          options: {
            baseUrl: "./",
            mainConfigFile: "public/javascript/inicio/build.js",
            name: "public/javascript/inicio/main.js", // assumes a production build using almond
            out: "public/javascript/inicio/main-built.js"
          }
        }
      },
      clean: [
        "public/css/"
      ],
      delta: {
        less: {
          files: ["public/less/*.*", "public/less/**/*.*"],
          tasks: ["less"]
        }
      }
    };

    grunt.initConfig(grunt.util._.extend(taskConfig));
    grunt.renameTask("watch", "delta");
    grunt.registerTask("watch", ["clean", "less:build", "delta"]);
    grunt.registerTask("default", ["watch"]);

  };
})();