/*global require*/
require(["angular", "lodash", "moment"], function(angular, _, moment) {
  "use strict";

  var modulo = angular.module("Proyecto.servicios", []);

  modulo.factory("Usuarios", ["$http",
    function($http) {
      var funciones = {};

      funciones.obtener = function() {
        return $http.get("/api/usuarios/");
      };

      funciones.actualizar = function(perfil) {
        return $http.put("/api/usuarios/actualizar", perfil);
      };

      funciones.buscar = function(texto, cantidad) {
        return $http.get("/api/usuarios/buscar", {
          params: {
            texto: texto,
            cantidad: cantidad
          }
        });
      };
      return funciones;
    }
  ]);
});