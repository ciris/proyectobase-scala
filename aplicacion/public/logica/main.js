/**
 * Created by piousp on 24/02/14.
 */
/*global require, document*/
require.config({
  paths: {
    "angular": "/public/bower_components/angular/angular",
    "angularI18N": "/public/bower_components/angular-i18n/angular-locale_es-cr",
    "ngAnimate": "/public/bower_components/angular-animate/angular-animate",
    "ngBindOnce": "/public/bower_components/angular-bindonce/bindonce",
    "ngRoute": "/public/bower_components/angular-route/angular-route",
    "ngTouch": "/public/bower_components/angular-touch/angular-touch",
    "ngScroll": "/public/bower_components/angular-scroll/angular-scroll",
    "snap": "/public/bower_components/snapjs/snap",
    "ngSnap": "/public/bower_components/angular-snap/angular-snap",
    "ngMoment": "/public/bower_components/angular-moment/angular-moment",
    "ngSortable": "/public/bower_components/ng-sortable/dist/ng-sortable",
    "ngTruncate": "/public/bower_components/angular-truncate/src/truncate",
    "ngFileUploadShim": "/public/bower_components/ng-file-upload/angular-file-upload-shim",
    "ngFileUpload": "/public/bower_components/ng-file-upload/angular-file-upload",
    "uiBootstrap": "/public/bower_components/angular-bootstrap/ui-bootstrap",
    "uiBootstrapTpls": "/public/bower_components/angular-bootstrap/ui-bootstrap-tpls",
    "loadingBar": "/public/bower_components/angular-loading-bar/build/loading-bar",
    "ngFx": "/public/bower_components/ngFx/dist/ngFx",
    "ngStorage": "/public/bower_components/ngstorage/ngStorage",
    "moment": "/public/bower_components/moment/moment",
    "momentES": "/public/bower_components/moment/lang/es",
    "lodash": "/public/bower_components/lodash/dist/lodash",
    "humanizeDuration": "/public/bower_components/humanize-duration/humanize-duration",
    "Chart": "/public/bower_components/Chart/index",
    "CKEDITOR": "/public/libs/ckeditor/ckeditor"
  },
  shim: {
    ngFileUploadShim: {
      exports: "ngFileUploadShim"
    },
    angular: {
      exports: "angular",
      deps: ["ngFileUploadShim"]
    },
    angularI18N: {
      exports: "angularI18N",
      deps: ["angular"]
    },
    ngRoute: {
      exports: "ngRoute",
      deps: ["angular"]
    },
    ngScroll: {
      exports: "ngScroll",
      deps: ["angular"]
    },
    uiBootstrap: {
      exports: "uiBootstrap",
      deps: ["angular"]
    },
    uiBootstrapTpls: {
      exports: "uiBootstrapTpls",
      deps: ["uiBootstrap"]
    },
    ngBindOnce: {
      exports: "ngBindOnce",
      deps: ["angular"]
    },
    ngAnimate: {
      exports: "ngAnimate",
      deps: ["angular"]
    },
    ngTouch: {
      exports: "ngTouch",
      deps: ["angular"]
    },
    ngStorage: {
      exports: "ngStorage",
      deps: ["angular"]
    },
    snap: {
      exports: "snap"
    },
    ngSnap: {
      exports: "ngSnap",
      deps: ["angular", "snap"]
    },
    ngFileUpload: {
      exports: "ngFileUpload",
      deps: ["angular"]
    },
    ngTruncate: {
      exports: "ngTruncate",
      deps: ["angular"]
    },
    ngSortable: {
      exports: "ngSortable",
      deps: ["angular"]
    },
    gsap: {
      exports: "gsap"
    },
    ngFx: {
      exports: "ngFx",
      deps: ["ngAnimate"]
    },
    loadingBar: {
      exports: "loadingBar",
      deps: ["angular"]
    },
    lodash: {
      exports: "_"
    },
    momentES: {
      exports: "momentES",
      deps: ["moment"]
    },
    ngMoment: {
      exports: "ngMoment",
      deps: ["angular", "momentES"]
    },
    humanizeDuration: {
      exports: "humanizeDuration"
    },
    CKEDITOR: {
      exports: "CKEDITOR"
    }
  },
  priority: ["angular"]
});

require([
  "require",
  "ngFileUploadShim",
  "angular",
  "./modulo",
  "angularI18N",
  "momentES"
], function(require, shim, angular, modulo) {
  "use strict";
  angular.element(document).ready(function() {
    angular.bootstrap(document, [modulo.name]);
  });
});