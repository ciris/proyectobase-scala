/*global document, define*/
define([
  "angular",
  "ngAnimate",
  "ngRoute",
  "ngTouch",
  "ngBindOnce",
  "ngScroll",
  "ngSnap",
  "ngMoment",
  "ngTruncate",
  "ngSortable",
  "uiBootstrapTpls",
  "ngFx",
  "loadingBar",
  "./comun/modulo",
  "./rutas",
  "./controladores",
  "./servicios",
  "./comps/todos",
  "./login/modulo"
], function(angular, funciones) {
  "use strict";

  var modulo = angular.module("Proyecto", [
    "ngRoute",
    "ngAnimate",
    "ngTouch",
    "ui.bootstrap",
    "pasvaz.bindonce",
    "angular-loading-bar",
    "duScroll",
    "snap",
    "ngFx",
    "ui.sortable",
    "truncate",
    "Proyecto.comun",
    "Proyecto.rutas",
    "Proyecto.controladores",
    "Proyecto.servicios",
    "Proyecto.comps",
    "Proyecto.login"
  ]);

  modulo.run([
    "$rootScope", "$location", "Credenciales", "Token", "Configuraciones", "RutasLibres",
    function($root, $location, Credenciales, Token, Configuraciones, RutasLibres) {
      $root.location = $location;

      function invalido() {
        $location.path("/inicio/credenciales");
      } //function

      function verificarToken(token) {
        Token.verificarToken(token)["catch"](function(data) {
          if (data.status === 472) {
            $location.path("/inicio/relogin");
          } else {
            $location.path("/inicio/credenciales");
          }
        });
      } //function

      if (!RutasLibres.esLibre($location.path())) {
        if (!Credenciales.estaLogueado()) {
          $location.path("/inicio/credenciales");
        } else {
          verificarToken(Credenciales.credenciales().token);
          Configuraciones.setHeaders(Credenciales.credenciales().token);
          $root.$on("$routeChangeStart", function() {
            verificarToken(Credenciales.credenciales().token);
          });
        }
      }

    } //run
  ]);

  return modulo;
});