/*global define*/
define(["angular", "lodash"], function(angular, _) {
  "use strict";
  var modulo = angular.module("Proyecto.comun.alertas", []);

  modulo.controller("AlertasCtrl", ["$scope", "Alertas",
    function($scope, Alertas) {
      $scope.alertas = Alertas;
      $scope.cerrar = function() {
        delete Alertas.alerta;
      };
    }
  ]);

  modulo.factory("Alertas", function() {
    var funciones = {};

    function oo(mensaje, defecto) {
      return (_.isUndefined(mensaje)) ? defecto : mensaje;
    } //function

    function alertaExito(status, mensaje) {
      var temp = {
        tipo: "success",
        titulo: "¡Éxito!",
        icono: "fa-check"
      };
      switch (status) {
        case 200:
          temp.mensaje = oo(mensaje, "Se ha procesado su pedido con éxito");
          break;
        case 201:
          temp.mensaje = oo(mensaje, "Se ha guardado el objeto con éxito");
          break;
        case 204:
          temp.mensaje = oo(mensaje, "Se ha eliminado el objeto con éxito");
          break;
        default:
          break;
      }
      return temp;
    } //function

    function alertaInfo(status, mensaje) {
      var temp = {
        tipo: "info",
        titulo: "Error de solicitud",
        icono: "fa-info"
      };
      switch (status) {
        case 400:
          temp.mensaje = oo(mensaje, "Faltan datos para poder completar su pedido");
          break;
        case 403:
          temp.mensaje = oo(mensaje, "Credenciales inválidos");
          break;
        case 404:
          temp.mensaje = oo(mensaje, "Elemento no encontrado");
          break;
        case 409:
          temp.mensaje = oo(mensaje, "Hay un conflicto en el horario de la nueva cita");
          break;
        case 470:
          temp.mensaje = oo(mensaje, "La identificación del documento no es válida");
          break;
        case 471:
          temp.mensaje = oo(mensaje, "El documento ya existe.");
          break;
        default:
          break;
      }
      return temp;
    } //function

    function alertaError(status, mensaje) {
      var temp = {
        tipo: "danger",
        titulo: "Error",
        icono: "fa-fire"
      };
      switch (status) {
        case 500:
          temp.mensaje = oo(mensaje, "Se ha producido un error inesperado. Por favor, inténtelo más tarde.");
          break;
        case 530:
          temp.mensaje = oo(mensaje, "Su pedido ha tardado más de lo esperado. Por favor, inténtelo más tarde.");
          break;
        default:
          break;
      }
      return temp;
    } //function

    function generar(status, mensaje) {
      var temp;
      if (status >= 200 && status < 300) {
        temp = alertaExito(status, mensaje);
      }
      if (status >= 400 && status < 500) {
        temp = alertaInfo(status, mensaje);
      }
      if (status >= 500) {
        temp = alertaError(status, mensaje);
      }
      return temp;
    } //function

    funciones.agregar = function(status, mensaje) {
      funciones.alerta = generar(status, mensaje);
    };
    funciones.alerta = undefined;

    return funciones;
  });

  return modulo;
});