/**
 * Created by piousp on 13/02/14.
 */
/*global define*/
define(["angular", "lodash", "moment", "ngStorage"], function(angular, _, moment) {
  "use strict";
  var modulo = angular.module("Proyecto.comun.servicios", ["ngStorage"]);

  modulo.factory("Fechas", function() {
    var funciones = {};
    funciones.formato = "YYYY-MM-DDTHH:mm:ss.Z";
    funciones.deServidor = function(fecha) {
      return moment(fecha, funciones.formato);
    };
    return funciones;
  });

  modulo.factory("RutasLibres", function() {
    var funciones = {};
    funciones.rutas = ["/", "/reingreso", "/inicio/credenciales"];
    funciones.esLibre = function(ruta) {
      return !_.isUndefined(_.find(funciones.rutas, function(r) {
        return r === ruta;
      }));
    };
    return funciones;
  });

  modulo.factory("Credenciales", ["$localStorage", "$sessionStorage",
    function($localStorage, $sessionStorage) {
      var funciones = {};
      funciones.local = $localStorage;
      funciones.sesion = $sessionStorage;
      $localStorage.$default({
        recordarPor: 3
      });

      funciones.recordarPor = function() {
        return $localStorage.recordarPor;
      };

      funciones.loguear = function(credenciales) {
        $sessionStorage.credenciales = credenciales;
      };

      funciones.iniciar = function(recordar, credenciales) {
        if (recordar) {
          $localStorage.credenciales = credenciales;
        }
        funciones.loguear(credenciales);
      };

      funciones.credenciales = function() {
        return $sessionStorage.credenciales;
      };

      function recordar() {
        var temp = $localStorage.credenciales;
        if (!_.isUndefined(temp) && _.isUndefined(funciones.credenciales())) {
          funciones.loguear(temp);
        }
      } //function

      funciones.estaLogueado = function() {
        recordar();
        return !_.isUndefined(funciones.credenciales());
      };

      funciones.borrarSession = function() {
        delete $sessionStorage.credenciales;
      };

      funciones.borrarCredenciales = function() {
        funciones.borrarSession();
        delete $localStorage.credenciales;
      };
      return funciones;
    }
  ]);

  modulo.factory("Configuraciones", ["$http",
    function($http) {
      var funciones = {};
      funciones.setHeaders = function(token) {
        $http.defaults.headers.common.Authorization = "JWT " + token;
      };
      return funciones;
    }
  ]);

  modulo.factory("Token", ["$http",
    function($http) {
      var funciones = {};

      funciones.obtener = function() {
        return $http.get("/api/usuarios/");
      };

      funciones.actualizar = function(perfil) {
        return $http.put("/api/usuarios/actualizar", perfil);
      };

      funciones.verificarToken = function(token) {
        var head = {
          headers: {
            "Authorization": "JWT " + token
          }
        };
        return $http.get("/api/verificacion/token", head);
      };

      funciones.revalidar = function(clave, recordarPor) {
        var obj = {
          password: clave,
          recordarPor: recordarPor
        };
        return $http.put("/api/usuarios/revalidar", obj);
      };

      return funciones;
    }
  ]);



  modulo.factory("ServiciosGlobales", [
    "$q",
    "$timeout",
    function($q, $timeout) {
      var funciones = {};

      funciones.aFuturo = function(valor, timeout) {
        try {
          var futuro = $q.defer();
          $timeout(function() {
            futuro.resolve(valor);
          }, (_.isUndefined(timeout)) ? 100 : timeout);
          return futuro.promise;
        } catch (ex) {
          return ex;
        } //catch
      };

      return funciones;
    }
  ]);
  return modulo;
});