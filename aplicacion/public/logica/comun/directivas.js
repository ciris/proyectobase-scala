/**
 * Created by piousp on 18/02/14.
 */
/*global define, window*/
define(["angular", "CKEDITOR", "lodash"], function(angular, CKEDITOR, _) {
  "use strict";
  var modulo = angular.module("Proyecto.comun.directivas", []);

  modulo.directive("cisCargando", function() {
    return {
      restrict: "A",
      scope: {
        cargando: "="
      },
      transclude: true,
      templateUrl: "/htmls/partials/ciscargando.html"
    };
  });

  modulo.directive("passwordIguales", [

    function() {
      return {
        restrict: "A",
        scope: true,
        require: "ngModel",
        link: function(scope, elem, attrs, control) {
          var checker = function() {
            var e1 = scope.$eval(attrs.ngModel),
              e2 = scope.$eval(attrs.passwordIguales);
            return e1 === e2;
          };
          scope.$watch(checker, function(n) {
            control.$setValidity("unique", n);
          });
        } //function
      }; //return
    }
  ]);

  modulo.directive("cisCkeditor", function() {
    return {
      restrict: "A",
      replace: true,
      template: "<div><textarea ng-model='modelo' class='form-control'></textarea></div>",
      link: function(scope, elem, attr, cisCkeditorCtrl) {
        cisCkeditorCtrl.init(elem);
      },
      scope: {
        modelo: "=",
        disabled: "="
      },
      controller: "cisCkeditorCtrl"
    };
  });

  function cisCkeditorCtrl($scope, $timeout) {
    var editor, id, elemento;
    this.init = function(elem) {
      elemento = elem;
      CKEDITOR.replace(elem.find("textarea")[0]);
    };

    CKEDITOR.on("instanceReady", function(ev) {
      editor = ev.editor;
      var id = elemento.find("div").attr("id").slice(4);
      CKEDITOR.instances[id].on("afterCommandExec", function(ev) {
        $timeout(function() {
          $scope.modelo = CKEDITOR.instances[id].getData();
        });
      });
      CKEDITOR.instances[id].on("key", function(ev) {
        $timeout(function() {
          $scope.modelo = CKEDITOR.instances[id].getData();
        });
      });
    });
    $scope.$watch("disabled", function(val) {
      if (editor) {
        editor.setReadOnly(_.isUndefined(val) ? false : true);
      }
    });
  } //function

  modulo.controller("cisCkeditorCtrl", ["$scope", "$timeout", cisCkeditorCtrl]);

  modulo.directive("cisMostrarPassword", function() {
    return {
      restrict: "A",
      templateUrl: "'/htmls/globales/password.html'",
      scope: {
        modelo: "=",
        clases: "@"
      }
    };
  });

  modulo.directive("cisAtras", ["$window",
    function($window) {
      return {
        restrict: "A",
        link: function(scope, element, attrs) {
          element.on("click", function() {
            $window.history.back();
          });
        }
      };
    }
  ]);

});