/*global define*/
define(["angular", "./servicios", "./directivas", "./alertas", "./filtros"], function(angular) {
  "use strict";
  var modulo = angular.module("Proyecto.comun", [
    "Proyecto.comun.servicios",
    "Proyecto.comun.directivas",
    "Proyecto.comun.alertas",
    "Proyecto.comun.filtros"
  ]);
  return modulo;
});