/*global define*/
define(["angular", "lodash", "moment", "humanizeDuration"],
  function(angular, _, moment, humanizeDuration) {
    "use strict";
    var modulo = angular.module("Proyecto.comun.filtros", []);


    modulo.filter("Fecha", ["Fechas",
      function(Fechas) {
        return function(date, format) {
          return (_.isUndefined(date)) ? "No Definido" : Fechas.deServidor(date).format(format);
        }; //function
      }
    ]);

    modulo.filter("Edad", function() {
      return function(fecha) {
        var temp = moment(utils.fechas.hoy).startOf("day").diff(moment(fecha).startOf("day"));
        return (_.isUndefined(fecha)) ? "No definido" : humanizeDuration(temp, "es");
      };
    });

    modulo.filter("Duracion", function() {
      return function(dur) {
        return (_.isUndefined(dur)) ? "No definida" : moment.duration(dur, "minutes").humanize();
      };
    });

    modulo.filter("TamanoHumano", function() {
      return function(bytes) {
        return (_.isUndefined(bytes)) ? "Indefinido" : (Math.round((bytes / 1048576) * 100) / 100) + "MB";
      };
    });
  });