/*global FileReader, define, window */
define(["angular", "lodash"], function(angular, _) {
  "use strict";

  var modulo = angular.module("Proyecto.controladores", []);

  modulo.controller("ControladorResultados", ["$scope", "$routeParams", "resultados",
    function($scope, $routeParams, resultados) {
      $scope.busqueda = $routeParams.busqueda;
      $scope.resultados = resultados;
    }
  ]);

  modulo.controller("ControladorCredencialesInvalidos", ["Credenciales", "$timeout", "$location",
    function(Credenciales, $timeout, $location) {
      Credenciales.borrarCredenciales();
      $timeout(function() {
        $location.path("/");
      }, 3000);
    }
  ]);

  modulo.controller("ControladorRelogin", [
    "$scope",
    "$location",
    "$timeout",
    "Credenciales",
    "Token",
    function($scope, $location, $timeout, Credenciales, Token) {
      var temp = Credenciales.credenciales();
      $scope.usuario = temp.nombre + " " + temp.apellidos;
      $scope.revalidar = function(pass) {
        $scope.bloqueo = true;
        Token.revalidar(pass, +Credenciales.recordarPor()).then(
          function(cred) {
            Credenciales.iniciar(true, cred);
            $timeout(function() {
              $location.path("/inicio");
            }, 100);
          },
          function() {
            $location.path("/inicio/credenciales/");
          }
        )["finally"](function() {
          $scope.bloqueo = false;
        });
      };
    }
  ]);

  modulo.controller("ControladorNavegacion", [
    "$scope",
    "$location",
    "$timeout",
    "Credenciales",
    "RutasLibres",
    function($scope, $location, $timeout, Credenciales, RutasLibres) {
      function buscar(texto) {
        $location.path("/inicio/resultados/" + texto);
      }
      $scope.buscar = buscar;
      $scope.colapsado = true;
      $scope.colapsar = function() {
        $scope.colapsado = !$scope.colapsado;
      };
      $scope.rutaLibre = function() {
        return RutasLibres.esLibre($location.path());
      };

      $scope.salir = function() {
        $location.path("/");
        Credenciales.borrarCredenciales();
      };
      $scope.logueado = Credenciales.estaLogueado;
      $scope.nombreUsuario = function() {
        if (Credenciales.estaLogueado()) {
          var temp = Credenciales.credenciales();
          return temp.nombre + " " + temp.apellidos;
        } //if
      };

    }
  ]);

  modulo.controller("ControladorPerfil", [
    "$rootScope",
    "$scope",
    "ServicioLogin",
    "ServicioAlertas",
    "$sessionStorage",
    "perfil",
    function($root, $scope, Login, Alertas, $sessionStorage, perfil) {
      var copiaSeguridad;
      $scope.perfil = perfil;

      $scope.iniciarCambio = function(perfil) {
        copiaSeguridad = angular.copy(perfil);
        $scope.pagina = "cambiarPass";
      };

      $scope.iniciarEdicion = function(perfil) {

        delete perfil.passwordActual;
        delete perfil.passwordNuevo;
        delete perfil.passwordNuevoComp;
        copiaSeguridad = angular.copy(perfil);
        $scope.pagina = "cambiarPerfil";
      };

      function reset() {
        $scope.pagina = undefined;
        try {
          $scope.per.$setPristine();
          $scope.passwd.$setPristine();
        } catch (ex) {}
      }

      $scope.cancelarEdicion = function() {
        $scope.perfil = copiaSeguridad;
        reset();
      };

      $scope.guardar = function(perfil) {
        $scope.bloqueo = true;
        Login.actualizar(perfil).then(
          function(id) {
            reset();
            var data = angular.copy($sessionStorage.usuario);
            data.nombre = perfil.nombre;
            data.apellidos = perfil.apellidos;
            $sessionStorage.usuario = data;
            $scope.bloqueo = false;
          },
          function(respuesta) {
            $root.alerta = Alertas.alerta(respuesta.status);
            $scope.bloqueo = false;
          }
        );
      };
    }
  ]);

  modulo.controller("ControladorAjustes", [
    "$rootScope",
    "$scope",
    "$sessionStorage",
    "$localStorage",
    function($root, $scope, $sessionStorage, $localStorage) {
      function encontrarTema(att, temas) {
        return _.find(temas, function(tema) {
          return tema.tema === att;
        });
      }
      var respaldo;
      $scope.configuraciones = {};
      $scope.configuraciones.recordarPor = +$localStorage.recordarPor;



      $scope.iniciarEdicion = function() {
        $scope.edicion = true;
        respaldo = angular.copy($scope.configuraciones);
      };

      $scope.salvarTema = function() {

      };

      $scope.guardar = function() {
        $localStorage.recordarPor = +$scope.configuraciones.recordarPor;
        $scope.edicion = false;
      };
    }
  ]);
});