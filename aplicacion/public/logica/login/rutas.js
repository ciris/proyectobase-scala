/**
 * Created by piousp on 13/02/14.
 */
/*global define, window*/
define(["angular", "lodash"], function(angular, _) {
  "use strict";
  var modulo = angular.module("Proyecto.login.rutas", ["Proyecto.comun.servicios"]);

  function plantilla(nombre) {
    return "/htmls/partials/login/" + nombre + ".html";
  } //function

  modulo.config(["$routeProvider",
    function($routeProvider) {

      $routeProvider.when("/", {
        templateUrl: plantilla("inicio"),
        controller: "ControladorLogin",
        resolve: {
          res: ["$location", "Credenciales",
            function($location, Credenciales) {
              if (Credenciales.estaLogueado()) {
                $location.path("/reingreso");
              }
            }
          ]
        } //resilve
      });

      $routeProvider.when("/reingreso", {
        templateUrl: plantilla("reingreso"),
        controller: "ControladorReingreso"
      });

      $routeProvider.when("/recuperaringreso", {
        templateUrl: plantilla("recuperarIngreso"),
        controller: "ControladorRecuperacion",
        resolve: {
          vista: ["Login",
            function(Login) {
              var params = {}; //TODO
              if (_.isUndefined(params)) {
                return "nueva";
              }
              return Login.existeRecuperacion(params).then(
                function(res) {
                  return "existe";
                },
                function(res) {
                  return "nueva";
                }
              );
            }
          ]
        }
      });

    } //function
  ]);
  return modulo;
});