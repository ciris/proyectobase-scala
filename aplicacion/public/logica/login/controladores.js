/**
 * Created by piousp on 13/02/14.
 */
/*global define*/
define(["angular", "lodash"], function(angular, _) {
  "use strict";
  var modulo = angular.module("Proyecto.login.controladores", []);

  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  modulo.controller("ControladorLogin", [
    "$scope",
    "$location",
    "$timeout",
    "Login",
    "Alertas",
    "Credenciales",
    function($scope, $location, $timeout, Login, Alertas, Credenciales) {
      $scope.autenticar = function(credenciales) {
        $scope.bloqueado = true;
        credenciales.recordarPor = credenciales.recordarme ? Credenciales.recordarPor() : 1;
        Login.autenticar(credenciales).then(
          function(resp) {
            Credenciales.iniciar(credenciales.recordarme, resp);
            $timeout(function() {
              $location.path("/inicio");
            }, 100);
          },
          function(data) {
            $scope.form.$setPristine();
            Alertas.agregar(403);
            delete $scope.credenciales.password;
          }
        ).finally(function() {
          $scope.bloqueado = false;
          $scope.enviado = true;
        });
      }; //function
    }
  ]);

  modulo.controller("ControladorReingreso", [
    "$scope",
    "$timeout",
    "$location",
    "Credenciales",
    "Token",
    function($scope, $timeout, $location, Credenciales, Token) {
      $scope.estado = "verificado";
      var token;

      function malo() {
        Credenciales.borrarCredenciales();
        $scope.estado = "malo";
        $timeout(function() {
          $location.path("/");
        }, 3000);
      } //function
      try {
        Token.verificarToken(Credenciales.credenciales().token).then(
          function() {
            $scope.estado = "bueno";
            $scope.usuario = Credenciales.credenciales().nombre;
            $timeout(function() {
              $location.path("/inicio");
            }, 3000);
          }, malo
        );
      } catch (err) {
        console.error(err);
        malo();
      }
    }
  ]);

  modulo.controller("ControladorRecuperacion", [
    "$scope",
    "$location",
    "Login",
    "Alertas",
    "vista",
    function($scope, $location, Login, Alertas, vista) {
      $scope.vista = vista;
      $scope.datos = {
        correo: ""
      };
      $scope.vacio = _.isUndefined;
      $scope.realizar = function(correo, form) {
        $scope.bloqueo = true;
        console.debug(correo);
        Login.pedirRecuperacion(correo).then(
          function() {
            Alertas.agregar(200, "Se ha realizado el pedido de recuperación de contraseña." +
              " En breve recibirá un correo a la dirección " + correo);
          },
          function(res) {
            Alertas.agregar(res.status, "El correo no está registrado");
          }
        )["finally"](function() {
          $scope.bloqueo = false;
          delete $scope.datos.correo;
          form.$setPristine();
        });
      };

      $scope.recuperar = function(clave) {
        $scope.bloqueo = true;
        Login.recuperar({}, clave).then( //TODOs
          function() {
            $location.path("/login");
            Alertas.agregar(200, "Se ha cambiado su contraseña correctamente");
          },
          function(res) {
            Alertas.agregar(res.status);
          }
        )["finally"](function() {
          $scope.bloqueo = false;
          delete $scope.correo;
          $scope.form.$setPristine();
        });
      };
    }
  ]);

  modulo.controller("ControladorForm", [
    "$rootScope",
    "$scope",
    "ServiciosIndex",
    "ServicioAlertas",
    function($root, $scope, ServiciosIndex, Alertas) {
      $scope.enviando = false;
      $scope.correoValidado = false;
      $scope.validarCorreo = function(correo, campo) {
        if (validateEmail(correo)) {
          $root.cargando = true;
          try {
            ServiciosIndex.buscarPorCorreo(correo).then(
              function() {
                $root.cargando = false;
                $scope.correoValidado = true;
                campo.$setValidity("yaexiste", true);
              },
              function(data) {
                $root.cargando = false;
                switch (data.status) {
                  case 471:
                    campo.$setValidity("yaexiste", false);
                    $scope.correoValidado = false;
                    break;
                  default:
                    $root.alerta = Alertas.alerta(400, "No se ha podido realizar la validación del correo");
                    $scope.correoValidado = true;
                    break;
                } //switch
              }
            );
          } catch (ex) {
            $scope.correoValidado = true;
          } //catch
        } //if
      }; //function

      $scope.enviar = function(usuario) {
        $root.cargando = true;
        ServiciosIndex.registrarUsuario(usuario).then(
          function(data) {
            $scope.usuario = undefined;
            $scope.formRegistro.$setPristine();
            $root.cargando = false;
            $root.alerta = Alertas.alerta(data.status);
          },
          function(data) {
            $root.cargando = false;
            $root.alerta = Alertas.alerta(data.status);
          }
        );
      }; //function
    }
  ]);
});