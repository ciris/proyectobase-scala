/*global define*/
define(["angular"], function(angular) {
  "use strict";
  var modulo = angular.module("Proyecto.login.directivas", []);

  return modulo;
});