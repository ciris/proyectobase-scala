/*global define*/
define(["angular", "./servicios", "./directivas", "./rutas", "./controladores"], function(angular) {
  "use strict";
  var modulo = angular.module("Proyecto.login", [
    "Proyecto.login.servicios",
    "Proyecto.login.directivas",
    "Proyecto.login.rutas",
    "Proyecto.login.controladores"
  ]);
  return modulo;
});