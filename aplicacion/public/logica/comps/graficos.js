/*global require*/
require(["angular", "Chart", "lodash"], function(angular, Chart, _) {
  "use strict";
  var modulo = angular.module("Proyecto.comps.graficos", []);

  var colores = {
    success: {
      r: 151,
      g: 207,
      b: 32
    },
    primary: {
      r: 73,
      g: 99,
      b: 116
    },
    info: {
      r: 19,
      g: 186,
      b: 208
    },
    warning: {
      r: 255,
      g: 207,
      b: 32
    },
    danger: {
      r: 255,
      g: 76,
      b: 60
    }
  };

  function aColorJSON(color) {
    var colorhex = colores[color] ? colores[color] : colores.primary;

    function rgba(color, a) {
      function aColor(r, g, b) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
      } //function
      return aColor(color.r, color.g, color.b, a);
    } //function

    return {
      fillColor: rgba(colorhex, 0.2),
      strokeColor: rgba(colorhex, 0.8),
      pointColor: rgba(colorhex, 0.9),
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: rgba(colorhex, 1),
    };
  } //function

  function datasetConColor(datasets) {
    return _.map(datasets, function(dataset) {
      var col = aColorJSON(dataset.color);
      col.data = dataset.data;
      return col;
    });
  } //function

  modulo.directive("cisChartLine", [

    function() {
      return {
        restrict: "EA",
        replace: true,
        templateUrl: "/htmls/inicio/partials/comps/graficos.html",
        scope: {
          labels: "=",
          datasets: "=",
        },
        controller: "cisChartCtrl",
        link: function(scope, elem, attrs, cisChartCtrl) {
          cisChartCtrl.init(elem);
        } //link
      };
    }
  ]);

  function cisChartCtrl($scope, $timeout) {
    var self = this;
    this.init = function(elem) {
      self.$elem = elem;
    };

    $scope.cargar = function() {
      $scope.cargado = true;
      var canvas = self.$elem.find("canvas")[0];
      $timeout(function() {
        var chart = new Chart(canvas.getContext("2d")).Line({
          labels: $scope.labels,
          datasets: datasetConColor($scope.datasets)
        }, {
          responsive: true,
          bezierCurveTension: 0.2,
        });
        chart.resize();
      }, 50);
    };

  } //function

  modulo.controller("cisChartCtrl", ["$scope", "$timeout", cisChartCtrl]);
});