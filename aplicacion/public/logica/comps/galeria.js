/*global define*/
define(["angular", "lodash"], function(angular, _) {
  "use strict";
  var modulo = angular.module("Proyecto.comps.galeria", []);

  modulo.directive("cisGaleria", [

    function() {
      return {
        restrict: "EA",
        replace: "true",
        templateUrl: "/htmls/inicio/partials/comps/galeria.html",
        scope: {
          imagenes: "=",
          cargando: "="
        },
        controller: "cisGaleriaCtrl"
      };
    }
  ]);
  modulo.controller("cisGaleriaCtrl", ["$scope", "$modal",
    function($scope, $modal) {
      $scope.vacio = _.isEmpty;
      $scope.abrir = function(imagen) {
        $modal.open({
          templateUrl: "lightbox.html",
          controller: "LightBoxCtrl",
          size: "lg",
          resolve: {
            imagen: function() {
              return imagen;
            }
          } //resolve
        });
      }; //abrir

    } //controller
  ]);
  modulo.controller("LightBoxCtrl", ["$scope", "$modalInstance", "imagen", "Imagenes",
    function($scope, $modalInstance, imagen, Imagenes) {
      console.debug(imagen);
      $scope.imagen = imagen;
      Imagenes.obtener(imagen.id).then(
        function(url) {
          $scope.url = url;
        }
      );
    } //controller
  ]);

  modulo.factory("Imagenes", ["$http", "Actores",
    function($http, Actores) {
      var funciones = {};
      funciones.obtener = function(id) {
        return $http.get("/api/imagenes/" + id, {
          params: {
            idUsuario: Actores.actorActual.id
          }
        });
      };
      return funciones;
    }
  ]);

  return modulo;
});