/*global define*/
define(
  ["angular",
    "./selectormes",
    "./subidaimagenes",
    "./galeria",
    "./graficos"
  ], function(angular) {
    "use strict";
    var modulo = angular.module("Proyecto.comps", [
      "Proyecto.comps.selectormes",
      "Proyecto.comps.subidaimagenes",
      "Proyecto.comps.galeria",
      "Proyecto.comps.graficos"
    ]);
    return modulo;
  });