/*global define, window, FileReader*/
define(["angular", "lodash", "ngFileUpload"], function(angular, _) {
  "use strict";
  var module = angular.module("Proyecto.comps.subidaimagenes", ["angularFileUpload"]);
  module.directive("cisSubidaImagenes", [

    function() {
      return {
        restrict: "EA",
        templateUrl: "/htmls/inicio/partials/comps/subidaimagenes.html",
        scope: {
          url: "@",
          id: "@",
          resultados: "="
        },
        controller: "SubidaImagenesCtrl"
      };
    }
  ]);

  var aceptados = ["image/png", "image/jpeg", "image/bmp", "image/tiff"];

  function filtrarAceptados(archivos) {
    return _.reject(archivos, function(archivo) {
      return _.isUndefined(_.find(aceptados, function(tipo) {
        return tipo === archivo.type;
      }));
    });
  } //function

  function componer(url, archivo) {
    return {
      url: url,
      archivo: archivo,
      subido: "no",
      progreso: 0,
      valido: function() {
        return !_.isUndefined(this.titulo) && !_.isUndefined(this.descripcion);
      }
    };
  } //function

  function datosForm(url, id, usuario, imagen, titulo, descripcion) {
    var urlCompleto = url + "?id=" + id + "&idUsuario=" + usuario;
    return {
      url: urlCompleto,
      method: "PUT",
      headers: {
        "Content-Type": "multipart/form-data; boundary=----Ciris5330963b44ae2234e6cb7105"
      },
      data: {
        imagen: imagen,
        titulo: titulo,
        descripcion: descripcion
      }
    };
  } //function

  function quitar(archivos, url) {
    return _.reject(archivos, function(archivo) {
      return archivo.url === url;
    });
  } //function

  module.controller("SubidaImagenesCtrl", [
    "$scope", "$timeout", "$upload", "Alertas", "Actores",
    function($scope, $timeout, $upload, Alertas, Actores) {
      $scope.archivos = [];
      $scope.vacio = _.isUndefined;
      $scope.resultados = [];
      $scope.todosValidos = function() {
        return !_.every($scope.archivos, function(el) {
          return el.valido();
        });
      };
      $scope.eliminar = function(archivos) {
        $scope.archivos = _.difference($scope.archivos, archivos);
      };
      $scope.seleccionar = function($files) {
        var filtrados = filtrarAceptados($files);
        _.forEach(filtrados, function(archivo) {
          if (window.FileReader) {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(archivo);
            fileReader.onload = function(e) {
              $timeout(function() {
                $scope.archivos.push(componer(e.target.result, archivo));
              });
            };
          } //if
        });
      }; //seleccionar

      $scope.iniciarTodos = function() {
        _.forEach($scope.archivos, $scope.iniciar);
      };

      $scope.iniciar = function(archivo) {
        archivo.bloqueado = true;
        var form = datosForm($scope.url, $scope.id, Actores.actorActual.id,
          archivo.archivo, archivo.titulo, archivo.descripcion);
        var futuro = $upload.upload(form).then(function(data) {
          $scope.resultados.push(data.data);
          archivo.subido = "si";
          Alertas.agregar(201);
          $timeout(function() {
            $scope.archivos = quitar($scope.archivos, archivo.url);
          }, 1500);
        }, function(error) {
          console.error(error);
          Alertas.agregar(error.status);
        }, function(evt) {
          archivo.progreso = parseInt(100.0 * evt.loaded / evt.total, 10);
        }).finally(function() {
          archivo.bloqueado = false;
        });
      };
    } //controller
  ]);

  return module;
});