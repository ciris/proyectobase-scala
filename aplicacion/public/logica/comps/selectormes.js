/*global define*/
define(["angular", "lodash", "moment"], function(angular, _, moment) {
  "use strict";
  var modulo = angular.module("Proyecto.comps.selectormes", []);

  modulo.directive("cisSelectormes", [

    function() {
      return {
        restrict: "EA",
        replace: "true",
        templateUrl: "/htmls/inicio/partials/comps/selectormes.html",
        scope: {
          fecha: "=",
          fin: "=",
          inicio: "="
        },
        controller: "cisSelectormesCtrl"
      };
    }
  ]);
  modulo.controller("cisSelectormesCtrl", ["$scope",
    function($scope) {
      $scope.puedeRetroceder = function(fecha, limite) {
        return _.isUndefined(limite) || fecha.year() > limite.year() ||
          (fecha.year() === limite.year() && fecha.month() > limite.month());
      };
      $scope.puedeAvanzar = function(fecha, limite) {
        return _.isUndefined(limite) || fecha.year() < limite.year() ||
          (fecha.year() === limite.year() && fecha.month() < limite.month());
      };

      $scope.retroceder = function() {
        if ($scope.puedeRetroceder($scope.fecha, $scope.inicio)) {
          $scope.fecha = moment($scope.fecha).subtract("M", 1);
        }
      };

      $scope.avanzar = function() {
        if ($scope.puedeAvanzar($scope.fecha, $scope.fin)) {
          $scope.fecha = moment($scope.fecha).add("M", 1);
        }
      };
    }
  ]);
  return modulo;
});