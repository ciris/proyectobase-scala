/**
 * Created by piousp on 09/04/14.
 */
/*global define*/
define(["angular"], function(angular) {
  "use strict";
  var modulo = angular.module("Proyecto.directivas", []);
  return modulo;
});