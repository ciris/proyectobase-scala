/**
 * Created by piousp on 13/02/14.
 */
/*global define, window*/
define(["angular"], function(angular) {
  "use strict";
  var modulo = angular.module("Proyecto.rutas", []);

  function plantilla(nombre) {
    return "/htmls/partials/" + nombre + ".html";
  } //function

  modulo.config([
    "$routeProvider",
    "$locationProvider",
    "$httpProvider",
    function($routeProvider, $locationProvider, $httpProvider) {
      if (window.history && window.history.pushState) {
        $locationProvider.html5Mode(true);
      } //if

      $httpProvider.interceptors.push(["$q", "$location", "RutasLibres",
        function($q, $location, RutasLibres) {
          return {
            "responseError": function(response) {
              if (response.status === 401 && !RutasLibres.esLibre($location.path())) {
                $location.path("/inicio/credenciales");
              }
              return $q.reject(response);
            }
          };
        }
      ]);

      $routeProvider.when("/inicio", {
        templateUrl: plantilla("inicio")
      });

      $routeProvider.when("/inicio/credenciales", {
        templateUrl: plantilla("credencialesinvalidos"),
        controller: "ControladorCredencialesInvalidos"
      });

      $routeProvider.when("/inicio/relogin", {
        templateUrl: plantilla("relogin"),
        controller: "ControladorRelogin"
      });

      $routeProvider.otherwise({
        templateUrl: plantilla("noencontrado")
      });
    }
  ]);
});