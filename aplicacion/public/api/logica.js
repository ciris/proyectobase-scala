/*global angular, _*/
(function() {
  "use strict";
  var modulo = angular.module("Proyecto", ["ui.bootstrap", "ngFx", "duScroll", "hljs"]);

  modulo.factory("Archivo", ["$http",
    function($http) {
      return {
        servir: function(archivo) {
          return $http.get("/public/api/" + archivo);
        }
      };
    }
  ]);

  modulo.controller("APICtrl", ["$scope", "Archivo",
    function($scope, Archivo) {
      $scope.apis = [];
      $scope.docs = [{
        nombre: "Selecciones",
        archivo: "selecciones.json"
      }];
      $scope.cargar = function(archivo) {
        Archivo.servir(archivo).then(
          function(data) {
            $scope.apis = [data.data];
          },
          function(error) {
            console.error(error);
          }
        );
      };
    }
  ]);
  modulo.filter("Trim", function() {
    return function(texto) {
      return texto.trim();
    }; //function
  });
  modulo.directive("cisRecurso", [

    function() {
      return {
        restrict: "EA",
        templateUrl: "/public/api/servicio.html",
        scope: {
          recurso: "="
        },
        controller: ["$scope", "$parse",
          function($scope, $parse) {
            $scope.vacio = function(obj) {
              return (_.isUndefined(obj) || _.isEmpty(obj));
            };

            $scope.toPrettyJSON = function(obj) {
              return JSON.stringify(obj, null, 2);
            };
          }
        ]
      };
    }
  ]);
})();