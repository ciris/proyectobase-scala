package com.ciriscr.base.logica.servicios

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.usuarios.SolicitudRecuperarClave
import com.ciriscr.base.logica.util.{ServicioEnvioCorreo, FabricaActoresSimple}
import com.ciriscr.base.bd.usuarios.SolicitudRecuperarClaveDAO

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/24/14
 * Hora:         12:12 PM
 */

private class ActorServicioRecuperacionClave extends ActorPreguntaRespuestaGenerico {
  import ServicioRecuperacionClave._

  protected def actorbd = ServicioDAOGenerico[SolicitudRecuperarClave, SolicitudRecuperarClaveDAO](SolicitudRecuperarClaveDAO())

  protected def preguntas: Receive = {

    case ExisteRecuperacion(id) =>
      debug("pedido de verificar si existe una recuperación")
      val mensaje = ServicioDAOGenerico.Contar("_id" -> id)
      procesarPreguntas(sender, mensaje)

    case ObtenerRecuperacion(id) =>
      debug("pedido de obtener una recuperacion recibido")
      val mensaje = ServicioDAOGenerico.BuscarPorId(id)
      procesarPreguntas(sender, mensaje)

    case PedirRecuperacion(login) =>
      debug("pedido de recuperar clave solicitado")
      context become recuperar(sender, login)
      val mensaje = ServicioUsuarios.EncontrarPorLogin(login)
      ServicioUsuarios() ! mensaje

    case BorrarSolicitud(id) =>
      debug("pedido de borrar una solicitud recibido")
      val mensaje = ServicioDAOGenerico.EliminarPorId(id)
      procesarPreguntas(sender, mensaje)
  }

  protected def respuestas(responderA: ActorRef): Receive = {
    case ServicioDAOGenerico.Contar.Resultado(cant) =>
      procesarRespuesta(responderA, ExisteRecuperacion.Resultado(cant > 0))

    case ServicioDAOGenerico.BuscarPorId.Resultado(oEnt) =>
      procesarRespuesta(responderA, ObtenerRecuperacion.Resultado(oEnt.map { case x: SolicitudRecuperarClave => x }))

    case ServicioDAOGenerico.EliminarPorId.Resultado(oid) =>
      procesarRespuesta(responderA, BorrarSolicitud.Resultado(oid.isDefined))

    case Fin(oid) =>
      procesarRespuesta(responderA, PedirRecuperacion.Resultado(oid))
  }

  private def recuperar(origen: ActorRef, login: String): Receive = {
    case ServicioUsuarios.EncontrarPorLogin.Resultado(oUsu) =>
      debug("usuario a recuperar encontrado")
      oUsu match {
        case Some(u) =>
          val mensaje = ServicioDAOGenerico.Insertar(new SolicitudRecuperarClave(u._id))
          actorbd ! mensaje
        case None =>
          context become respuestas(origen)
          self ! Fin(false)
      }

    case ServicioDAOGenerico.Insertar.Resultado(oid) =>
      debug("solicitud insertada, enviando correo")
      oid match {
        case Some(id) =>
          val mensaje = ServicioEnvioCorreo.EnviarRecuperacion(id, login)
          ServicioEnvioCorreo() ! mensaje

        case None =>
          context become respuestas(origen)
          self ! Fin(false)
      }

    case ServicioEnvioCorreo.EnviarRecuperacion.Enviado(b) =>
      debug("correo enviado")
      context become respuestas(origen)
      self ! Fin(b)

  }

  private case class Fin(completada: Boolean)

}

object ServicioRecuperacionClave extends FabricaActoresSimple {

  protected def clazz: Class[_] = classOf[ActorServicioRecuperacionClave]

  case class PedirRecuperacion(login: String)
  object PedirRecuperacion{case class Resultado(pedida: Boolean)}

  case class ObtenerRecuperacion(id: ObjectId)
  object ObtenerRecuperacion{case class Resultado(id: Option[SolicitudRecuperarClave])}

  case class ExisteRecuperacion(id: ObjectId)
  object ExisteRecuperacion { case class Resultado(existe: Boolean) }

  case class BorrarSolicitud(id: ObjectId)
  object BorrarSolicitud { case class Resultado(borrado: Boolean) }

}
