package com.ciriscr.base.logica.elastic.util

import com.ciriscr.base.logica.elastic.entidades.ESUsuario
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.usuarios.Usuario

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        27/02/14
 * Hora:         04:30 PM
 */

trait TransformarES {

  implicit def objectIdToString(id: ObjectId): String = id.toString

  implicit class aESUsuario(u: Usuario) {
    def aES: ESUsuario = ESUsuario(u._id, u.nombre, u.apellidos, u.correo, u.login, u.cedula)
  }

}
