package com.ciriscr.base.logica.pedidos

import scala.concurrent.ExecutionContext
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.servicios.ServicioUsuarios
import com.ciriscr.base.logica.servicios.ServicioRecuperacionClave

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/24/14
 * Hora:         1:32 PM
 */

class PedidosRecuperacionClave()(implicit fabrica: ActorRefFactory, timeout: Timeout, EC: ExecutionContext) {
  import akka.pattern.ask

  private def actor = ServicioRecuperacionClave()

  /**
   * Realiza un pedido de recuperación de contraseña para el usuario.
   * @param correo: String
   * @tparam A
   * @return
   */
  def pedirRecuperacion[A](correo: String)(procesar: Boolean => A): Future[A] = {
    val futuro = actor ? ServicioRecuperacionClave.PedirRecuperacion(correo)
    futuro.mapTo[ServicioRecuperacionClave.PedirRecuperacion.Resultado].map(a => procesar(a.pedida))
  } //def

  /**
   * Verifica si existe la recuperacion
   * @param id
   * @param procesar
   * @tparam A
   * @return
   */
  def existeRecuperacion[A](id: ObjectId)(procesar: Boolean => A): Future[A] = {
    val futuro = actor ? ServicioRecuperacionClave.ExisteRecuperacion(id)
    futuro.mapTo[ServicioRecuperacionClave.ExisteRecuperacion.Resultado].map(a => procesar(a.existe))
  }

  def recuperarClave[A](idRecuperacion: ObjectId, claveNueva: String)(procesar: Option[ObjectId] => A): Future[A] = {
    val futuro = (actor ? ServicioRecuperacionClave.ObtenerRecuperacion(idRecuperacion))
      .mapTo[ServicioRecuperacionClave.ObtenerRecuperacion.Resultado].map(_.id.toList)
    val fCambioClave = futuro.flatMap { t =>
        Future.sequence {
          t.map { f =>
            val oUsu = (ServicioUsuarios() ? ServicioUsuarios.Obtener(f._idUsuario))
              .mapTo[ServicioUsuarios.Obtener.Resultado].map(_.obj.toList)
            oUsu.flatMap{ r =>
              Future.sequence {
                r.map { u =>
                    (ServicioUsuarios() ? ServicioUsuarios.ActualizarClave(u.copy(password = claveNueva)))
                      .mapTo[ServicioUsuarios.Actualizar.Resultado].map(_.id)
                }
              }
            }
          } // map
        } // sequence
    } // flatMap
    val fTemp1 = fCambioClave.map(_.map(_.flatten).flatten.headOption)
    fTemp1.onSuccess {
      case Some(id) =>
        actor ? ServicioRecuperacionClave.BorrarSolicitud(idRecuperacion)
    }

    for {
      t1 <- fTemp1
    } yield procesar(t1)
  }

}
