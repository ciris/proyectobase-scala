package com.ciriscr.base.logica.correos

import com.ciriscr.base.logica.amazon.Direcciones
import org.bson.types.ObjectId

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/24/14
 * Hora:         4:43 PM
 */

class CorreoRecuperacion(id: ObjectId, to: String) extends Correo {
  private lazy val link = s"www.galeniscr.com/recuperaringreso?q=$id"
  def asunto: String = "Recuperación de su contraseña - Galenis"
  def body: String = {
    <html>
      <body >
        <div style="background: url('http://galeniscr.com/recursos/imagenes/fondo.jpg') no-repeat center center; padding: 2em;">
          <div style="background: rgba(0, 0, 0, 0.7);
                  padding: 1em;
                  font-size: 16px;
                  margin: 0 auto;
                  border-radius: 6px;">
            <h1 style="color: #ffffff;">Recuperación de contraseña</h1>
            <hr style="height: 5px;
                   border-top: 0;
                   background: #c4e17f;
                   border-radius: 5px;
                   margin: 20px 0 !important;
                   background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);"/>
            <p style="color: #ffffff">Estimado Usuario:</p>
            <p style="color: #ffffff">Se ha recibido una petición para recuperar su contraseña. Si usted realizó esta petición entonces proceda a
              darle click en el siguiente enlace, de lo contrario ignore este correo.</p>
            <br/>
            <a href={link} style="color: #FF7518">{link}</a>
            <hr style="height: 5px;
                   border-top: 0;
                   background: #c4e17f;
                   border-radius: 5px;
                   margin: 20px 0 !important;
                   background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                   background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);"/>
            <p style="color: #ffffff">Muchas gracias por preferirnos,</p>
            <p style="color: #ffffff">El equipo de Galenis.</p>
            <img src="http://galeniscr.com/recursos/imagenes/galenis-ver-contra.png" height="40px" />
            <a style="color: #FF7518" href="http://www.galeniscr.com">www.galeniscr.com</a>
          </div>
        </div>
      </body>
    </html>
  }.toString
  def emails: Direcciones = Direcciones(List(to), Nil, Nil)
}
