package com.ciriscr.base.logica.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        10/02/14
 * Hora:         04:01 PM
 */

case class ResultadoNoEncontradoException() extends Exception("No se encontraron resultados en la búsqueda.")
