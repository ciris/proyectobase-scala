package com.ciriscr.base.logica.elastic

import scala.collection.JavaConversions._
import org.elasticsearch.node.NodeBuilder.nodeBuilder
import org.elasticsearch.action.{ActionResponse, ActionRequest, ActionRequestBuilder}
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.action.delete.DeleteResponse
import org.elasticsearch.action.admin.indices.mapping.delete.DeleteMappingResponse
import org.elasticsearch.index.query.FilterBuilder
import com.ciriscr.base.util.ConBitacora
import com.ciriscr.base.logica.elastic.util.{Filter, Query}
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.client.ElasticsearchClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.common.settings.ImmutableSettings


/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        08/01/14
 * Hora:         11:13 AM
 */

private[elastic] object ESCliente extends ConBitacora {
  private implicit def ejecutarYObtener[A <: ActionRequest[A], B <: ActionResponse, C <: ActionRequestBuilder[A, B, C, D], D <: ElasticsearchClient[D]](a: ActionRequestBuilder[A, B, C, D]) =
  //No pregunte como pero todos esos type paremeters hacen que funcione el método
    a.execute().actionGet()

  private lazy val nodo = nodeBuilder().client(true).clusterName("mediciris").node()
  private lazy val cliente = {
//    nodo.client()
    val port = 9300
    val settings = ImmutableSettings.settingsBuilder()
      .put("cluster.name", "mediciris")
      .put("client.transport.sniff", true).build()
    new TransportClient(settings)
      .addTransportAddress(new InetSocketTransportAddress("localhost", port))
  }

  def buscar(indice: String, tipo: String, query: Query): SearchResponse = {
    val result = cliente.prepareSearch(indice).setTypes(tipo).setQuery(query.build)
    debug(s"Buscar con este query: ${query.build}")
    result
  }


  def buscarFiltrado(indice: String, tipo: String, query: Query, filtros: Filter): SearchResponse = {
    val result = cliente.prepareSearch(indice).setTypes(tipo).setQuery(query.build).setPostFilter(filtros.build)
    debug(s"Buscar con filtro con este query: ${query.build}")
    debug(s"Buscar con filtro con este filtro: ${filtros.build}")
    result
  }

  def agregar(indice: String, tipo: String, id: String, documento: Map[String, AnyRef]): IndexResponse = {
    val result = cliente.prepareIndex(indice, tipo, id).setSource(mapAsJavaMap(documento))
    debug(s"Agregar al indice /$indice/$tipo/$id el documento: $documento")
    result
  }

  def eliminar(indice: String, tipo: String, id: String): DeleteResponse = {
    val result = cliente.prepareDelete(indice, tipo, id)
    debug(s"Eliminar del indice /$indice/$tipo el id: $id")
    result
  }

  def borrarIndice(indice: String, tipo: String): DeleteMappingResponse = {
    val result = cliente.admin().indices().prepareDeleteMapping(indice).setType(tipo).execute().actionGet()
    debug(s"Borrar por completo el indice /$indice/$tipo")
    result
  }

  def apagar() {
    nodo.close()
    cliente.close()
  }
}
