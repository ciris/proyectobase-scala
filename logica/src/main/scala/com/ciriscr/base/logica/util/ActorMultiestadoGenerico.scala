package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        31/07/14
 * Hora:         04:29 PM
 */

trait ActorMultiestadoGenerico extends ActorGenerico {

  protected def actorbd: ActorRef

  protected def cambiarEstado(estado: => Receive) {
    debug("cambiando de estado")
    context become estado
  }

  protected def devolverResultado(origen: ActorRef, mensaje: AnyRef, debugMsg: String) {
    debug(debugMsg)
    origen ! mensaje
    harakiri()
  }

}
