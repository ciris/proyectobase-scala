package com.ciriscr.base.logica.elastic

import com.ciriscr.base.logica.elastic.util.Imports
import Imports._
import com.ciriscr.base.logica.Imports._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        20/01/14
 * Hora:         03:12 PM
 */
private[elastic] abstract class ESActorGenerico[A <: ESAny](protected val index: String, protected val tipo: String) extends ActorGenerico {
  import ESActorGenerico._

  def recibir: Receive = mensajesGenericos

  def mensajesGenericos: Receive = {
    case Buscar(s, id, cantidad) =>
      val (cant, lista) = buscar(s, id.map(_.toString).getOrElse(""), cantidad)
      sender ! Buscar.Resultados[A](cant, lista)
      harakiri()

    case p: Insertar[A] =>
      sender ! Insertar.Resultado(agregar(p.u))
      harakiri()

    case Eliminar(id) =>
      sender ! Eliminar.Resultado(eliminar(id))
      harakiri()

    case u: Actualizar[A] =>
      sender ! Actualizar.Resultado(actualizar(u.u))
      harakiri()

    case BorrarIndice() =>
      sender ! BorrarIndice.Resultado(borrarIndice())
      harakiri()
  }

  protected def buscar(s: String, id: => String, cantidad: Int): (Long, List[A])
  protected def agregar(p: A): String = ESCliente.agregar(index, tipo, p.id, p.toMap).getId
  protected def eliminar(id: String): Boolean = ESCliente.eliminar(index, tipo, id).getId.nonEmpty
  protected def actualizar(p: A): Boolean = agregar(p).nonEmpty
  protected def borrarIndice(): Boolean = { ESCliente.borrarIndice(index, tipo); true }
}

object ESActorGenerico {
  case class Buscar(texto: String, id: Option[ObjectId] = None, cantidad: Int)
  object Buscar{case class Resultados[A <: ESAny](cantidad: Long, res: List[A])}
  case class Insertar[A <: ESAny](u: A)
  object Insertar{case class Resultado(_id: String) {def res: ObjectId = new ObjectId(_id)}}
  case class Eliminar(id: String) { def this(u: ESAny) = this(u.id) }
  object Eliminar{case class Resultado(res: Boolean)}
  case class Actualizar[A <: ESAny](u: A)
  object Actualizar{case class Resultado(res: Boolean)}
  case class BorrarIndice()
  object BorrarIndice{case class Resultado(res: Boolean)}
}
