package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        24/02/14
 * Hora:         03:19 PM
 */

trait ActorPreguntaRespuestaGenerico extends ActorMultiestadoGenerico {

  protected def actorbd: ActorRef
  final def recibir: Receive = preguntas
  protected def preguntas: Receive
  protected def respuestas(responderA: ActorRef): Receive

  protected def procesarPreguntas(origen: ActorRef, mensaje: AnyRef) {
    cambiarEstado(respuestas(origen))
    debug(s"Enviando pregunta a ActorBD: ${actorbd.toString()}")
    actorbd ! mensaje
  } // def

  protected def procesarRespuesta(origen: ActorRef, mensaje: AnyRef) {
    devolverResultado(origen, mensaje, "Respuesta de los servicios bd recibida. Contestando al originador")
  }

} // trait