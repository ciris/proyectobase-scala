package com.ciriscr.base.logica.elastic.util

import org.elasticsearch.index.query._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        13/01/14
 * Hora:         01:17 PM
 */

trait Query {
  def build: BaseQueryBuilder
}

case class TermQuery(term: (String, String)) extends Query {
    def build: TermQueryBuilder = QueryBuilders.termQuery(term._1, term._2)
}

case class BooleanQuery(must: List[(String, String)], mustNot: List[(String, String)],
                        should: List[(String, String)]) extends Query {
  def build: BaseQueryBuilder = {
    def repeatMust(bqb: BoolQueryBuilder, l: List[(String, String)]): BoolQueryBuilder = {
      if (l.nonEmpty) repeatMust(bqb.must(TermQuery(l.head).build), l.tail)
      else bqb
    }
    def repeatMustNot(bqb: BoolQueryBuilder, l: List[(String, String)]): BoolQueryBuilder = {
      if (l.nonEmpty) repeatMustNot(bqb.mustNot(TermQuery(l.head).build), l.tail)
      else bqb
    }
    def repeatShould(bqb: BoolQueryBuilder, l: List[(String, String)]): BoolQueryBuilder = {
      if (l.nonEmpty) repeatShould(bqb.should(TermQuery(l.head).build), l.tail)
      else bqb
    }

    repeatMust(repeatMustNot(repeatShould(QueryBuilders.boolQuery(), must), mustNot), should)
  }
}

case class MatchQuery(term: (String, String)) extends Query {
  def build: BaseQueryBuilder = QueryBuilders.matchQuery(term._1, term._2)
}

case class RangeQuery[A](field: String, from: Option[Numeric[A]] = None, to: Option[Numeric[A]] = None) extends Query {
  def build: BaseQueryBuilder = {
    val q = QueryBuilders.rangeQuery(field)
    val f = from.map(n => q.from(n)).getOrElse(q)
    to.map(n => f.to(n)).getOrElse(f)
  }
}

case class MatchAllQuery() extends Query {
  def build: BaseQueryBuilder = QueryBuilders.matchAllQuery()
}

case class StringQuery(text: String) extends Query {
  def build: BaseQueryBuilder = QueryBuilders.queryString(text)
}

case class WildcardQuery(term: (String, String)) extends Query {
  def build: BaseQueryBuilder = QueryBuilders.wildcardQuery(term._1, term._2)
}

case class MultiMatchQuery(text: String, fields: List[String]) extends Query {
  def build: BaseQueryBuilder =
    QueryBuilders.multiMatchQuery(text, fields:_*).`type`(MatchQueryBuilder.Type.PHRASE_PREFIX)
}

case class BoostingQuery(positivo: Option[Query], negativo: Option[Query], boost: Float = 1, negBoost: Float = 1) extends Query {
  override def build: BaseQueryBuilder = {
    val q = QueryBuilders.boostingQuery().boost(boost).negativeBoost(negBoost)
    val p = positivo.map(a => q.positive(a.build)).getOrElse(q)
    negativo.map(b => p.negative(b.build)).getOrElse(p)
  }
}

case class IdsQuery(ids: Seq[String]) extends Query {
  override def build: BaseQueryBuilder = QueryBuilders.idsQuery(ids:_*)
}
