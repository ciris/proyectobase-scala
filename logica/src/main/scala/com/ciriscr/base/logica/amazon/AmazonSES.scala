package com.ciriscr.base.logica.amazon

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsyncClient
import com.amazonaws.services.simpleemail.model._
import com.amazonaws.regions.{Region, Regions}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        27/03/14
 * Hora:         02:40 PM
 */

private object AmazonSES {                                           //Loads the file AwsCredentials.properties located in the resources directory
  private final val client = new AmazonSimpleEmailServiceAsyncClient(new ClasspathPropertiesFileCredentialsProvider())
  client.setRegion(Region.getRegion(Regions.US_WEST_2))

  def sendEmail(request: SendEmailRequest): SendEmailResult = client.sendEmail(request)
}

case class Direcciones(to: List[String], cc: List[String], bcc: List[String])
case class AmazonEmail(from: String, emails: Direcciones, asunto: String, mensaje: String) {
  import scala.collection.JavaConversions._

  implicit def aJavaList[A](list: List[A]): java.util.List[A] = seqAsJavaList[A](list)

  private def toAmazonEmailRequest: SendEmailRequest = {
    val destinos = new Destination(emails.to)
    destinos.setCcAddresses(emails.cc)
    destinos.setBccAddresses(emails.bcc)
    val message = new Message(new Content(asunto), new Body().withHtml(new Content(mensaje)))
    new SendEmailRequest(from, destinos, message)
  }

  def send(): SendEmailResult = AmazonSES.sendEmail(toAmazonEmailRequest)
}
