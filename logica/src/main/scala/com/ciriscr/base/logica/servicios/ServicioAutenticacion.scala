package com.ciriscr.base.logica.servicios

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.util.ConBitacora
import com.ciriscr.base.entidades.usuarios.Usuario
import com.ciriscr.base.logica.util.FabricaActoresSimple

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        18/12/13
 * Hora:         09:06 AM
 */
trait ServicioAutenticacion extends ConBitacora{

  import scala.util.{Success, Failure}

  /**
   * Valida si la contraseña digitada es igual a la contraseña guardada en base de datos
   * @param usuario: Option[Usuario]
   * @param pass: String Contraseña digitada
   * @return Success(Usuario) si el password está correcto y el usuario se obtuvo correctamente desde la base datos,
   *         Failure(ExcepcionAutenticacion) si el password no coincide
   *         Failure(ElementoNoEncontrado) si el usuario no se obtuvo correctamente de la base de datos
   */
  def validarClave(usuario: Option[Usuario], pass: String): ServicioAutenticacion.Autenticar.Resultado = {
    debug("Validando clave")
    import com.ciriscr.base.logica.Encriptor
    usuario match{
      case Some(u) if Encriptor.validarClave(pass, u.password) => ServicioAutenticacion.Autenticar.Autenticado(u)
      case None => ServicioAutenticacion.Autenticar.NoEncontrado()
      case _ => ServicioAutenticacion.Autenticar.Fallido()
    } //match
  } //def
} //trait

private class ActorServicioAutentificacion() extends ActorPreguntaRespuestaGenerico {

  import ServicioAutenticacion._
  import com.ciriscr.base.bd.usuarios.UsuarioDAO

  private var password = ""
  private lazy val logica = new Object with ServicioAutenticacion
  protected lazy val actorbd = ServicioDAOGenerico[Usuario, UsuarioDAO](UsuarioDAO())

  def preguntas: Receive = {
    case Autenticar(correo, password) =>
      debug("Pedido de Autenticación recibido.")
      this.password = password
      val mensaje = ServicioDAOGenerico.BuscarUno("correo" -> correo)
      procesarPreguntas(sender, mensaje)
  } //def

  def respuestas(responderA: ActorRef): Receive = {
    case ServicioDAOGenerico.BuscarUno.Resultado(res) =>
      debug("Respuesta de los servicios bd recibida. Validando contraseña y contestando al originador")
      procesarRespuesta(responderA, logica.validarClave(res.map{case u: Usuario => u}, password))
  } //def


} //class

object ServicioAutenticacion extends FabricaActoresSimple{
  val clazz: Class[_] = classOf[ActorServicioAutentificacion]

  case class Autenticar(correo: String, password: String)
  object Autenticar{
    sealed trait Resultado
    case class Autenticado(usuario: Usuario) extends Resultado
    case class Fallido() extends Resultado
    case class NoEncontrado() extends Resultado
  } //object

  case object ExcepcionAutenticacion extends Throwable("Credenciales Inválidos")
} //object
