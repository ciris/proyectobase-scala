package com.ciriscr.base.logica.bd

import com.ciriscr.base.logica.util.FabricaActoresSimple
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.logica.elastic.ESIndexador
import org.joda.time.DateTime

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        27/03/14
 * Hora:         01:09 PM
 */

trait Interceptador {

}

private class ActorInterceptor[A <: BaseAny] extends ActorGenerico {
  import Interceptador._

  def recibir: Receive = {
    case Interceptado(op, a) =>
      import Operacion._
      debug("manejando la intercepción")
      op match {
        case Insertar =>
          info("Interceptor -> Insertar")
          ESIndexador[A]() ! ESIndexador.Agregar(a)
          a match {
            case _ =>
          }

        case Eliminar =>
          info("Interceptor -> Eliminar")
          ESIndexador[A]() ! ESIndexador.Eliminar(a)
          a match {
            case _ =>
          }

        case Actualizar =>
          info("Interceptor -> Actualizar")
          ESIndexador[A]() ! ESIndexador.Actualizar(a)
          a match {
            case _ =>
          }
        case _ =>
      }

    case ServicioDAOGenerico.ActualizarEntidad.Resultado(oid) => harakiri()
  }
}

object Interceptador extends FabricaActores {
  def apply[A <: BaseAny](nombre: Option[String] = None)(implicit fabrica: ActorRefFactory): ActorRef = {
    this.nuevo(nombre, Props(classOf[ActorInterceptor[A]]))
  } //def

  case class Interceptado[A](operacion: Operacion.Value, a: A)
}

object Operacion extends Enumeration {
  val Eliminar, Insertar, Actualizar, Buscar = Value
}
