package com.ciriscr.base.logica.amazon

import java.io.{InputStream, FileOutputStream, File}
import java.net.URL
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.auth.{AWSCredentials, ClasspathPropertiesFileCredentialsProvider}
import com.amazonaws.services.s3.model.{StorageClass, PutObjectRequest}
import com.ciriscr.base.entorno.ValoresEntorno
import org.joda.time.{Period, ReadablePeriod, DateTime}
import scala.sys.process.BasicIO
import org.joda.time

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        24/03/14
 * Hora:         03:35 PM
 */

object AmazonS3 {
                                                //Loads the file AwsCredentials.properties located in the resources directory
  private final val client = new AmazonS3Client(new AWSCredentials {
                                                  override def getAWSAccessKeyId: String = ValoresEntorno.awsAccess
                                                  override def getAWSSecretKey: String = ValoresEntorno.awsSecret
                                                })

  /**
   * Upload a file to Amazon S3
   * @param file the File to upload
   * @param bucketName the bucket where the file will be stored
   */
  def putFile(file: File, bucketName: String, internalPath: Option[String])(ss: StorageClass) {
    val key = s"${internalPath.map(p => s"$p/").getOrElse("")}${file.getName}"
    val putObjectReq = new PutObjectRequest(bucketName, key, file)
    client.putObject(putObjectReq.withStorageClass(ss))
  }

  /**
   * Download the file from Amazon S3
   * @param bucketName the bucket where the file is stored
   * @param fileName the internal path of the file inside the bucket
   * @param destinationFilePath the local filepath where the file will be saved
   * @return the File references in the local machine
   */
  def getFile(bucketName: String, internalPath: Option[String], fileName: String, destinationFilePath: String): File = {
    val path = s"$bucketName${internalPath.map(p => s"/$p").getOrElse("")}"
    val objecto = client.getObject(path, fileName)
    val length = objecto.getObjectMetadata.getContentLength
    saveFile(destinationFilePath, objecto.getObjectContent, length)
  }

  /**
   * Download the inputStream into the local machine
   * @param destinationFilePath the local filepath where the file will be saved
   * @param stream the stream from the Amazon S3 file
   * @param length the length of the file to download
   * @return the File references in the local machine
   */
  private def saveFile(destinationFilePath: String, stream: InputStream, length: Long): File = {
    val file = new File(destinationFilePath)
    BasicIO.transferFully(stream, new FileOutputStream(file))
    file
  }

  /**
   * Deletes a file from Amazon S3. If the file is the only one inside a carpet, this carpet will be eliminated as well.
   * @param bucketName the bucket where the file is stored
   * @param fileName the internal path of the file inside the bucket
   */
  def deleteFile(bucketName: String, internalPath: Option[String], fileName: String) {
    val path = s"$bucketName${internalPath.map(p => s"/$p").getOrElse("")}"
    client.deleteObject(path, fileName)
  }

  /**
   * Generate a pre-signed url string that allows to use in web applications
   * @param bucketName the bucket where the file is stored
   * @param fileName the internal path of the file inside the bucket
   * @param expires the time in seconds since epoch (1/1/1970) where that link will expire and become an ilegal url (managed by Amazon S3)
   * @return the url as an string of the file
   */
  def getPresignedURLString(bucketName: String, internalPath: Option[String], fileName: String, expires: DateTime): String =
    getPresignedURL(bucketName, internalPath, fileName, expires).toString

  def getPresignedURLString(bucketName: String, internalPath: Option[String], fileName: String, expiresIn: ReadablePeriod): String =
    getPresignedURLString(bucketName, internalPath, fileName, DateTime.now().plus(expiresIn))

  /**
   * Generate a pre-signed url that allows to use in web applications
   * @param bucketName the bucket where the file is stored
   * @param fileName the internal path of the file inside the bucket
   * @param expires the time in seconds since epoch (1/1/1970) where that link will expire and become an ilegal url (managed by Amazon S3)
   * @return the url of the file, null if file is not in Amazon S3
   */
  def getPresignedURL(bucketName: String, internalPath: Option[String], fileName: String, expires: DateTime): URL = {
    val path = s"$bucketName${internalPath.map(p => s"/$p").getOrElse("")}"
    client.generatePresignedUrl(path, fileName, expires.toDate)
  }

  def getPresignedURL(bucketName: String, internalPath: Option[String], fileName: String, expiresIn: ReadablePeriod): URL =
    getPresignedURL(bucketName, internalPath, fileName, DateTime.now().plus(expiresIn))

  /**
   * Check if the bucket exists, if not it creates a bucket in Amazon S3
   * @param bucketName the bucket name to create
   */
  def createBucket(bucketName: String) {
    if (!client.doesBucketExist(bucketName))
      client.createBucket(bucketName)
  }

  /**
   * Deletes a bucket from Amazon S3, the bucket most be empty to be succesfully deleted.
   * @param bucketName the bucket to delete
   */
  def deleteBucket(bucketName: String) {
    client.deleteBucket(bucketName)
  }
}
