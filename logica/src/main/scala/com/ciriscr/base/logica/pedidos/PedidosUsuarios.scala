package com.ciriscr.base.logica.pedidos

import scala.util.Try
import scala.concurrent.ExecutionContext
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.usuarios.Usuario
import com.ciriscr.base.logica.elastic.entidades.ESUsuario
import com.ciriscr.base.logica.elastic.{ESActorGenerico, ESUsuarios}
import com.ciriscr.base.logica.servicios.{ServicioUsuarios, ServicioAutenticacion}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        17/01/14
 * Hora:         01:27 PM
 */
class PedidosUsuarios()(implicit fabrica: ActorRefFactory, timeout: Timeout, EC: ExecutionContext) {
  import akka.pattern.ask

  private def actor = ServicioUsuarios()

  def existeRecuperacion[A](id: ObjectId)(procesar: Option[Boolean] => A): Future[A] = {
    Future.successful(procesar(Some(false)))
  }

  /**
   * Realiza un pedido de autenticación para el usuario.
   * @param correo: String
   * @param passwd: String
   * @param procesar
   * @tparam A
   * @return
   */
  def autenticar[A](correo: String, passwd: String)(procesar: ServicioAutenticacion.Autenticar.Resultado => A): Future[A] = {
    val futuro = ServicioAutenticacion() ? ServicioAutenticacion.Autenticar(correo, passwd)
    futuro.mapTo[ServicioAutenticacion.Autenticar.Resultado].map(a => procesar(a))
  } //def

  def revalidar[A](idUsuario: ObjectId, passwd: String)(procesar: Option[ServicioAutenticacion.Autenticar.Resultado] => A): Future[A] = {
    val fut = buscar(idUsuario){ opUsu =>
      Future.sequence {
        opUsu.map { usu =>
          autenticar(usu.login, passwd)(a => a)
        }.toList
      }.map(_.headOption)
    }.flatMap(f => f)
    fut.map(a => procesar(a))
  }

  /**
   * Guarda a un usuario en la base de datos
   * @param usuario
   * @param procesarResultado
   * @tparam A
   * @return
   */
  def guardarUsuario[A](usuario: Usuario)(procesarResultado: Option[ObjectId] => A): Future[A] = {
    val futuro = actor ? ServicioUsuarios.Registrar(usuario)
    futuro.mapTo[ServicioUsuarios.Registrar.Resultado].map(a => procesarResultado(a.id))
  } //def

  /**
   * Busca un usuario por medio de su login
   * @param login
   * @param procesar
   * @tparam A
   * @return Future[A]
   */
  def buscarPorLogin[A](login: String)(procesar: Option[Usuario] => A): Future[A] = {
    val futuro = actor ? ServicioUsuarios.EncontrarPorLogin(login)
    futuro.mapTo[ServicioUsuarios.EncontrarPorLogin.Resultado].map(a => procesar(a.usuario))
  } //def

  def actualizarUsuarioConClave[A](usuario: Usuario)(procesar: Option[ObjectId] => A): Future[A] = {
    val futuro = actor ? ServicioUsuarios.ActualizarClave(usuario)
    futuro.mapTo[ServicioUsuarios.Actualizar.Resultado].map(a => procesar(a.id))
  }

  def actualizarUsuarioSinClave[A](usuario: Usuario)(procesar: Option[ObjectId] => A): Future[A] = {
    val futuro = actor ? ServicioUsuarios.Actualizar(usuario)
    futuro.mapTo[ServicioUsuarios.Actualizar.Resultado].map(a => procesar(a.id))
  }

  def buscar[A](id: ObjectId)(procesar: Option[Usuario] => A): Future[A] = {
    val futuro = actor ? ServicioUsuarios.Obtener(id)
    futuro.mapTo[ServicioUsuarios.Obtener.Resultado].map(a => procesar(a.obj))
  }

  def buscarPorTexto[A](texto: String, cantidad: Int)(procesar: List[ESUsuario] => A): Future[A] = {
    val fut = ESUsuarios() ? ESActorGenerico.Buscar(texto, None, cantidad)
    fut.mapTo[ESActorGenerico.Buscar.Resultados[ESUsuario]].map(t => procesar(t.res))
  } //def
} //class
