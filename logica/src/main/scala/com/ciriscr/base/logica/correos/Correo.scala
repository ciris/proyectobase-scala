package com.ciriscr.base.logica.correos

import com.ciriscr.base.logica.amazon.{Direcciones, AmazonEmail}
import com.amazonaws.services.simpleemail.model.SendEmailResult

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        4/24/14
 * Hora:         4:50 PM
 */

trait Correo {
  def asunto: String
  def body: String
  def emails: Direcciones

  def from: String = "servidor@galeniscr.com"
  def toAmazonEmail: AmazonEmail = AmazonEmail(from, emails, asunto, body)
  def send(): SendEmailResult = toAmazonEmail.send()
}
