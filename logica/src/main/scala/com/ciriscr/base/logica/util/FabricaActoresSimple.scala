package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        07/01/14
 * Hora:         09:34 AM
 */
trait FabricaActoresSimple extends FabricaActores {
  protected def clazz: Class[_]
  def apply(nombre: String)(implicit fabrica: ActorRefFactory): ActorRef = this.nuevo(nombre, Props(clazz))
  def apply(nombre: Option[String] = None)(implicit fabrica: ActorRefFactory): ActorRef = this.nuevo(nombre, Props(clazz))
} //trait