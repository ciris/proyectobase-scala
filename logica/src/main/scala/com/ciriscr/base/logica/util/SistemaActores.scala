package com.ciriscr.base.logica.util

import akka.actor.ActorSystem
import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador: beto
 * Fecha: 18/10/2013
 * Hora: 9:43 AM
 */
object SistemaActores extends ConBitacora{
  private lazy val sistema: (Option[String], Option[String]) => ActorSystem = { (nombre, config) =>
    nombre.map{ nom =>
      config.map(conf => cargar(nom, conf)).getOrElse{
        debug(s"Generando un sistema de actores con el nombre '$nom'")
        ActorSystem(nom)
      }}.getOrElse{
      debug(s"Generando un sistema de actores genérico sin nombre")
      ActorSystem()
    } //getOrElse
  } //val

  /**
   * Genera un sistema de actores genérico sin nombre
   * @return ActorSystem
   */
  def apply(): ActorSystem = crear(None, None)

  /**
   * Genera un sistema de actores con el nombre provisto.
   * @param nombre: String el nombre para el sistema de actores
   * @return ActorSystem
   */
  def apply(nombre: String): ActorSystem = crear(Some(nombre), None)

  /**
   * Genera un sistema de actores con el nombre y la configuración provista
   * @param nombre: String el nombre para el sistema de actores
   * @param config: String el nombre de la configuración
   * @return ActorSystem
   */
  def apply(nombre: String, config: String): ActorSystem = crear(Some(nombre), Some(config))

  /**
   * Genera un sistema de actores con el nombre y la configuración provista si ambos están definidos
   * @param nombre: Option[String]
   * @param config: Option[String]
   * @return ActorySystem
   */
  private def crear(nombre: Option[String] = None, config: Option[String] = None): ActorSystem = sistema(nombre, config)

  /**
   * Genera un sistema de actores con el nombre y la configuración provista
   * @param nombre: String el nombre para el sistema de actores
   * @param config: String el nombre de la configuración
   * @return ActorSystem
   */
  private def cargar(nombre: String, config: String): ActorSystem = {
    import com.typesafe.config.ConfigFactory
    debug(s"Generando un sistema de actores con el nombre '$nombre' y la configuración '$config'")
    ActorSystem(nombre, ConfigFactory.load.getConfig(config))
  } //def
}//object