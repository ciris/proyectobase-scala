package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.BaseAny

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        04/02/14
 * Hora:         08:23 AM
 */
trait MensajesGenericos[A <: BaseAny]{
  case class Obtener(id: ObjectId)
  object Obtener{case class Resultado(obj: Option[A])}

  case class Registrar(a: A)
  object Registrar{case class Resultado(id: Option[ObjectId])}

  case class Multiples(ids: List[ObjectId])
  object Multiples{case class Resultados(res: Iterator[A])}
} //trait