package com.ciriscr.base.logica.elastic

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        16/01/14
 * Hora:         11:45 AM
 */

trait ESAny {
  def toMap: Map[String, AnyRef]
  def id: String
}
