package com.ciriscr.base.logica.elastic.util

import org.elasticsearch.index.query.{FilterBuilder, FilterBuilders}
import scala.collection.JavaConversions._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        06/02/14
 * Hora:         02:42 PM
 */

trait Filter {
  def build: FilterBuilder
}

/**
 * Filters documents that only have the provided ids
 * @param types tipos de indice
 * @param ids ids de los documentos a buscar
 */
case class IdsFilter(types: Seq[String], ids: Seq[String]) extends Filter {
  def build: FilterBuilder = FilterBuilders.idsFilter(types:_*).addIds(ids:_*)
}

/**
 * A limit filter limits the number of documents (per shard) to execute on
 * @param limit numero
 */
case class LimitFilter(limit: Int) extends Filter {
  def build: FilterBuilder = FilterBuilders.limitFilter(limit)
}

/**
 * Filters documents matching the provided document / mapping type
 * @param tipo el tipo de indice
 */
case class TypeFilter(tipo: String) extends Filter {
  def build: FilterBuilder = FilterBuilders.typeFilter(tipo)
}

/**
 * A filter that matches on all documents
 *
 * O sea, filtra todo
 */
case class MatchAllFilter() extends Filter {
  def build: FilterBuilder = FilterBuilders.matchAllFilter()
}

/**
 * Filters documents where a specific field has no value in them.
 * @param name field to filter
 */
case class MissingFilter(name: String, exist: Boolean = false, hasNullValue: Boolean = false) extends Filter {
  override def build: FilterBuilder = FilterBuilders.missingFilter(name).existence(exist).nullValue(hasNullValue)
}

/**
 * A filter that filters out matched documents using a query. Can be placed within queries that accept a filter.
 * @param filter filter to exclude
 */
case class FilterNot(filter: Filter) extends Filter {
  override def build: FilterBuilder = FilterBuilders.notFilter(filter.build)
}

/**
 * Filters documents with fields that have values within a certain numeric range. Similar to
 * range filter, except that it works only with numeric values, and the filter execution
 * works differently.
 *
 * The numeric range filter works by loading all the relevant field values into memory, and
 * checking for the relevant docs if they satisfy the range requirements. This requires more
 * memory since the numeric range data are loaded to memory, but can provide a significant
 * increase in performance. Note, if the relevant field values have already been loaded to memory,
 * for example because it was used in facets or was sorted on, then this filter should be used.
 */
case class NumericRangeFilter[A](name: String, from: Option[Numeric[A]], to: Option[Numeric[A]],
                                 includeLowerBound: Boolean = false, includeUpperBound: Boolean = false) extends Filter {
  override def build: FilterBuilder = {
    val r = FilterBuilders.rangeFilter(name)
    val a = from.map(f => r.from(f)).getOrElse(r)
    to.map(t => a.to(t)).getOrElse(a)
      .includeLower(includeLowerBound)
      .includeUpper(includeUpperBound)
  }
}

/**
 * A filter allowing to define scripts as filters
 *
 * ejemplo:
 *    ScriptFilter("doc['age'].value > param1", Map(param1 -> 21))
 *
 * @param script
 * @param params
 */
case class ScriptFilter(script: String, params: Map[String, AnyRef]) extends Filter {
  override def build: FilterBuilder = FilterBuilders.scriptFilter(script).params(mapAsJavaMap(params))
}

/**
 * Filters documents that have fields that contain a term (not analyzed). Similar to term query, except that it acts as a filter.
 *
 * Para busquedas exactas
 * @param term tupla del termino
 */
case class TermFilter(term: (String, Any)) extends Filter {
  override def build: FilterBuilder = FilterBuilders.termFilter(term._1, term._2)
}

/**
 * Filters documents that have fields that match any of the provided terms (not analyzed)
 * @param name el campo
 * @param values valores que puede tener
 */
case class TermsFilter(name: String, values: Seq[AnyRef]) extends Filter {
  override def build: FilterBuilder = FilterBuilders.termsFilter(name, values:_*)
}

/**
 * Wraps any query to be used as a filter
 * @param query el query para filtrar
 */
case class QueryFilter(query: Query) extends Filter {
  override def build: FilterBuilder = FilterBuilders.queryFilter(query.build)
}

/**
 * Filters documents that have fields containing terms with a specified prefix (not analyzed). Similar to
 * phrase query, except that it acts as a filter
 * @param term el campo y valor
 */
case class PrefixFilter(term: (String, String)) extends Filter {
  override def build: FilterBuilder = FilterBuilders.prefixFilter(term._1, term._2)
}

/**
 * A filter that matches documents using OR boolean operator on other queries
 * @param filtros
 */
case class OrFilter(filtros: Seq[Filter]) extends Filter {
  override def build: FilterBuilder = FilterBuilders.orFilter(filtros.map(_.build):_*)
}

/**
 * Filters documents where a specific field has a value in them
 * @param campo
 */
case class ExistsFilter(campo: String) extends Filter {
  override def build: FilterBuilder = FilterBuilders.existsFilter(campo)
}

/**
 * A filter that matches documents using AND boolean operator on other filters
 * @param filtros
 */
case class AndFilter(filtros: Seq[Filter]) extends Filter {
  override def build: FilterBuilder = FilterBuilders.andFilter(filtros.map(_.build):_*)
}

case class BooleanFilter(must:Seq[Filter], mustNot: Seq[Filter],
                         should: Seq[Filter]) extends Filter {

  override def build: FilterBuilder =
    FilterBuilders.boolFilter()
      .must(must.map(_.build):_*)
      .mustNot(mustNot.map(_.build):_*)
      .should(should.map(_.build):_*)
}

// falta implementar HasChildFilter, HasParentFilter, GeoShapeFilter, GeoPolygonFilter
//                   GeoDistanceRangeFilter, GeoDistanceFilter, GeoBoundingBoxFilter
