package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.correos.CorreoRecuperacion

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        28/01/14
 * Hora:         02:19 PM
 */
private class ActorServicioEnvioCorreo extends ActorGenerico {
  import ServicioEnvioCorreo._


  def recibir: Receive = {
    case EnviarRecuperacion(id, correo) =>
      debug(s"pedido de enviar el correo de recuperacion recibido")
      new CorreoRecuperacion(id, correo).send()
      sender ! EnviarRecuperacion.Enviado(true)

  } //def
} //class

object ServicioEnvioCorreo extends FabricaActores{
  private lazy val sa = SistemaActores("Mediciris-Correos")
  def apply(nombre: Option[String] = None): ActorRef = {
    this.nuevo(nombre, Props(classOf[ActorServicioEnvioCorreo]))(sa)
  } //def

  case class EnviarRecuperacion(id: ObjectId, correo: String)
  object EnviarRecuperacion { case class Enviado(seEnvio: Boolean) }
} //object
