package com.ciriscr.base.logica

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        19/12/13
 * Hora:         11:22 AM
 */
object Encriptor {

  private lazy val cantidadIteraciones = 5853
  import org.jasypt.digest.StringDigester

  def encriptar(clave: String): String = getDigester.digest(clave)

  private def getDigester: StringDigester = {
    import org.jasypt.digest.StandardStringDigester
    val digester = new StandardStringDigester()
    digester.setAlgorithm("SHA-512")
    digester.setIterations(cantidadIteraciones)
    digester
  } //def

  def validarClave(clave: String, password:String): Boolean = getDigester.matches(clave, password)
} //Encriptor