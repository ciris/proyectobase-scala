package com.ciriscr.base.logica.elastic

import com.ciriscr.base.logica.elastic.util.Imports
import Imports._
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.BaseAny

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        22/01/14
 * Hora:         01:41 PM
 */

private class ActorESIndexador[A <: BaseAny] extends ActorGenerico {
  import ESIndexador._
  import com.ciriscr.base.entidades.usuarios.Usuario

  def recibir: Receive = {
    case add: Agregar[A] =>
      add.a match {
        case u: Usuario => ESUsuarios() ! ESActorGenerico.Insertar(u.aES)
        case _ =>
      }
    case up: Actualizar[A] =>
      up.a match {
        case u: Usuario => ESUsuarios() ! ESActorGenerico.Actualizar(u.aES)
        case _ =>
      }
    case rm: Eliminar[A] =>
      rm.a match {
        case u: Usuario => ESUsuarios() ! ESActorGenerico.Eliminar(rm.a._id)
        case _ =>
      }
    case ESActorGenerico.Eliminar.Resultado(p) => harakiri()
    case ESActorGenerico.Actualizar.Resultado(p) => harakiri()
    case ESActorGenerico.Insertar.Resultado(p) => harakiri()
  }
}

object ESIndexador extends FabricaActores {
  /**
   * Devuelve una función que crea un nuevo ActorRef del tipo A cuando se le aplica una conexión concreta de base de datos.
   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
   * del sistema.
   * @param nombre: Option[String] nombre para el actor. Si no hay nombre, se usa el nombre por defecto de akka.
   * @return ActorRef
   */
  def apply[A <: BaseAny](nombre: Option[String] = None): ActorRef = {
    this.nuevo(nombre, Props(classOf[ActorESIndexador[A]]))
  } //def

  case class Agregar[A](a: A)
  case class Actualizar[A](a: A)
  case class Eliminar[A](a: A)
}
