package com.ciriscr.base.logica.bd

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.entidades.util.BorradoLogico

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        23/12/13
 * Hora:         04:25 PM
 */
private class ActorServicioDAOGenerico[A <: BaseAny with BorradoLogico, +B <: CirisDAO[A]](dao: B) extends ActorGenerico {
  protected lazy val logica = LogicaGenerica[A, B](dao)
  import ServicioDAOGenerico._

  def recibir: Receive = buscar orElse actualizar orElse insertar orElse eliminar

  def buscar: Receive = {
    case Contar(filtros) =>
      debug("Pedido de contado recibido. Llamando a la lógica")
      val res = logica.contar(filtros)
      debug("Búsqueda lista. Contestando al sender")
      sender ! Contar.Resultado(res)
      harakiri()

    case BuscarPorId(id) =>
      debug("Pedido de búsqueda por id recibido. Llamando a la lógica")
      val res = logica.buscarPorId(id)
      debug(s"Búsqueda lista. Contestando al sender: $sender")
      sender ! BuscarPorId.Resultado(res)
      harakiri()

    case Buscar(filtros, orden, limit, skip) =>
      debug("Pedido de búsqueda recibido. Llamando a la lógica")
      debug(s"Query: $filtros")
      val (cant, res) = logica.buscar(filtros, orden, limit, skip)
      debug("Búsqueda lista. Contestando al sender")
      sender ! Buscar.Resultados(cant, res)
      harakiri()

    case BuscarUno(filtros) =>
      debug("Pedido de búsqueda única recibido. Llamando a la lógica")
      val res = logica.buscarUno(filtros)
      debug("Búsqueda lista. Contestando al sender")
      sender ! BuscarUno.Resultado(res)
      harakiri()
  }

  def insertar: Receive = {
    case a: Insertar[A] =>
      debug("Pedido de inserción recibido. Llamando a la lógica")
      val res = logica.insertar(a.a)
      debug("Interceptando")
      interceptar(res, Operacion.Insertar)
      debug("Inserción lista. Contestando al sender")
      sender ! Insertar.Resultado(res)
      harakiri()
  }

  def actualizar: Receive = {
    case Actualizar(valor, query, upsert, multi) =>
      debug("Pedido de actualización recibido. Llamando a la lógica")
      val res = logica.actualizar(valor, query, upsert, multi)
      debug("Interceptando")
      interceptar(res, Operacion.Actualizar)
      debug("Actualización lista. Contestando al sender")
      sender ! Actualizar.Resultado(res)
      harakiri()

    case a: ActualizarEntidad[A] =>
      debug("Pedido de actualizar entidad recibido. Llamando a la lógica")
      val res = logica.actualizar(a.entidad, a.query, a.upsert, a.multi)
      debug("Interceptando")
      interceptar(res, Operacion.Actualizar)
      debug("Actualización lista. Contestando al sender")
      sender ! ActualizarEntidad.Resultado(res)
      harakiri()

    case BuscarYActualizar(query, update, orden, upsert, nuevo) =>
      debug("Pedido de buscar y actualizar recibido. Llamando a la lógica")
      val res = logica.buscarYActualizar(query, update, orden, upsert, nuevo)
      debug("Interceptando")
      interceptar(res.map(_._id), Operacion.Actualizar)
      debug("Actualización lista. Contestando al sender")
      sender ! BuscarYActualizar.Resultado(res)
      harakiri()

  }

  def eliminar: Receive = {
    case EliminarPorId(id) =>
      debug("Pedido de eliminar por id recibido. Llamando a la lógica")
      val res = logica.eliminar(id)
      debug("Interceptando")
      interceptar(res, Operacion.Eliminar)
      debug("Eliminación lista. Contestando al sender")
      sender ! EliminarPorId.Resultado(res)
      harakiri()

    case e: EliminarEntidad[A] =>
      debug("Pedido de eliminar entidad recibido. Llamando a la lógica")
      val res = logica.eliminar(e.entidad)
      debug("Interceptando")
      interceptar(res, Operacion.Eliminar)
      debug("Eliminación lista. Contestando al sender")
      sender ! EliminarEntidad.Resultado(res)
      harakiri()

    case Eliminar(filtros) =>
      debug("Pedido de eliminar recibido. Llamando a la lógica")
      val res = logica.eliminar(filtros)
      debug("Interceptando")
      interceptar(res, Operacion.Eliminar)
      debug("Eliminación lista. Contestando al sender")
      sender ! Eliminar.Resultado(res)
      harakiri()

    case EliminarForzado(id) =>
      debug("Pedido de eliminar por id recibido. Llamando a la lógica")
      val res = logica.eliminarForzado(id)
      debug("Interceptando")
      interceptar(res, Operacion.Eliminar)
      debug("Eliminación lista. Contestando al sender")
      sender ! EliminarForzado.Resultado(res)
      harakiri()

    case e: EliminarEntidadForzado[A] =>
      debug("Pedido de eliminar entidad recibido. Llamando a la lógica")
      val res = logica.eliminarForzado(e.entidad)
      debug("Interceptando")
      interceptar(res, Operacion.Eliminar)
      debug("Eliminación lista. Contestando al sender")
      sender ! EliminarEntidadForzado.Resultado(res)
      harakiri()
  } //def

  private def interceptar(id: Option[ObjectId], op: Operacion.Value) { interceptar(id.toList, op) }
  private def interceptar(l: List[ObjectId], op: Operacion.Value) {
    l.foreach(id => logica.buscarPorIdForzado(id).map((a: A) =>
      Interceptador[A]()(context.system) ! Interceptador.Interceptado(op, a)
    ) )
  }

} //class

object ServicioDAOGenerico extends FabricaActores{

  /**
   * Devuelve una función que crea un nuevo ActorRef del tipo B cuando se le aplica una conexión concreta de base de datos.
   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
   * del sistema.
   * @param dao: B el dao a usar con el actor
   * @param nombre: Option[String] nombre para el actor. Si no hay nombre, se usa el nombre por defecto de akka.
   * @param fabrica: ActorRefFactory para instanciar los actores
   * @return ActorRef
   */
  def apply[A <: BaseAny, B <: CirisDAO[A]](dao: B, nombre: Option[String] = None)(implicit fabrica: ActorRefFactory): ActorRef = {
    this.nuevo(nombre, Props(classOf[ActorServicioDAOGenerico[A, B]], dao))
  } //def

  case class Contar(filtros: Map[String, Any])
  object Contar{case class Resultado(cuantos: Long)}
  case class BuscarPorId(id: ObjectId)
  object BuscarPorId{case class Resultado[A <: BaseAny](entidad: Option[A])}
  case class Buscar(filtros: Map[String, Any], orden: Map[String, Any] = Map.empty[String, Any], limit: Int = 0, skip: Int = 0)
  object Buscar{case class Resultados[A <: BaseAny](cantidad: Int, res: Iterator[A])}
  case class BuscarUno(filtros: Map[String, Any])
  object BuscarUno{case class Resultado[A <: BaseAny](res: Option[A])}
  case class Insertar[A <: BaseAny](a: A)
  object Insertar{case class Resultado(id: Option[ObjectId])}
  case class ActualizarEntidad[A <: BaseAny](entidad: A,query: Map[String, Any],upsert: Boolean = false,multi: Boolean = false)
  object ActualizarEntidad{case class Resultado(id: Option[ObjectId])}
  @deprecated("Si ocupa este mensaje favor validar con los demás si es requerido, crea problemas de auditoría y ES si se utiliza multi=false",
              "22 de enero 2014")
  case class Actualizar[A <: BaseAny](valor: Map[String, Any],query: Map[String, Any],upsert: Boolean,multi: Boolean)
  object Actualizar{case class Resultado(ids: List[ObjectId])}
  case class EliminarPorId(id: ObjectId)
  object EliminarPorId{case class Resultado(id: Option[ObjectId])}
  case class EliminarEntidad[A <: BaseAny](entidad: A)
  object EliminarEntidad{case class Resultado[A <: BaseAny](id: Option[ObjectId])}
  case class Eliminar(filtros: Map[String, Any])
  object Eliminar{case class Resultado(ids: List[ObjectId])}
  case class EliminarForzado(id: ObjectId)
  object EliminarForzado { case class Resultado(id: Option[ObjectId]) }
  case class EliminarEntidadForzado[A](entidad: A)
  object EliminarEntidadForzado { case class Resultado(id: Option[ObjectId]) }
  case class BuscarYActualizar(query: Map[String, Any], update: Map[String, Any], orden: Map[String, Any] = Map.empty,
                               upsert: Boolean = false, retornarNuevo: Boolean = false)
  object BuscarYActualizar { case class Resultado[A <: BaseAny](res: Option[A]) }
} //object
