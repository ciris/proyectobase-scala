package com.ciriscr.base.logica.elastic.util

import org.elasticsearch.action.search.SearchResponse
import scala.collection.JavaConversions._
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.elastic.ESCliente

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo
 * Fecha:        15/01/14
 * Hora:         10:47 AM
 */

private[elastic] object Imports extends TransformarES {

  implicit lazy val sistema = {
    val s = SistemaActores("ESsystems")
    s.registerOnTermination(ESCliente.apagar())
    s
  }
  implicit def toMap(t: SearchResponse): List[Map[String, AnyRef]] =
    t.getHits.map(r => mapAsScalaMap(r.sourceAsMap()).toMap).toList

  implicit class ExtractorMap(mapa: Map[String, AnyRef]){
    def string(campo: String): String = mapa.get(campo).map(_.toString).getOrElse("")
  } //class
} //object
