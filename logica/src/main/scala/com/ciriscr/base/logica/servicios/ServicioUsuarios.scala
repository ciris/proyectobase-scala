package com.ciriscr.base.logica.servicios

import com.ciriscr.base.entidades.usuarios.Usuario
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.util.{MensajesGenericos, FabricaActoresSimple}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        19/12/13
 * Hora:         11:18 AM
 */
trait ServicioUsuarios{
  def encriptarPassword(usuario: Usuario): Usuario = {
    import com.ciriscr.base.logica.Encriptor
    usuario.copy(password = Encriptor.encriptar(usuario.password))
  } //def
}

private class ActorServicioUsuarios() extends ActorPreguntaRespuestaGenerico {
  import ServicioUsuarios._
  import com.ciriscr.base.bd.usuarios.UsuarioDAO

  private lazy val logica = new Object with ServicioUsuarios
  protected def actorbd = ServicioDAOGenerico[Usuario, UsuarioDAO](UsuarioDAO())

  def preguntas: Receive = {
    case Registrar(usuario) =>
      debug("Pedido de Crear usuario recibido.")
      val mensaje = ServicioDAOGenerico.Insertar(logica.encriptarPassword(usuario))
      procesarPreguntas(sender, mensaje)

    case EncontrarPorLogin(login) =>
      debug("Pedido de Encontrar por correo recibido.")
      val mensaje = ServicioDAOGenerico.BuscarUno("login" -> login)
      procesarPreguntas(sender, mensaje)

    case Obtener(id) =>
      debug(s"Pedido de obtener un usuario por su id recibido: $id")
      val mensaje = ServicioDAOGenerico.BuscarPorId(id)
      procesarPreguntas(sender, mensaje)

    case Actualizar(usuario) =>
      debug("Pedido de actualizar el usuario sin cambiar la clave recibido")
      val mensaje = ServicioDAOGenerico.ActualizarEntidad(usuario, "_id" -> usuario._id)
      procesarPreguntas(sender, mensaje)

    case ActualizarClave(usuario) =>
      debug("Pedido de actualizar el usuario cambiando la clave recibido")
      val mensaje = ServicioDAOGenerico.ActualizarEntidad(logica.encriptarPassword(usuario), "_id" -> usuario._id)
      procesarPreguntas(sender, mensaje)

    case Asistentes(usuario, pagina, cantidad) =>
      debug("Pedido de obtener los asistentes de un usuario recibido")
      val mensaje = ServicioDAOGenerico.Buscar("configuracion.asistidos" -> usuario,
        Map("nombre" -> 1, "apellidos" -> 1), cantidad, pagina)
      procesarPreguntas(sender, mensaje)

    case AgregarAsistido(usuario, asistido) =>
      debug("Pedido de agregar un asistido recibido")
      val mensaje = ServicioDAOGenerico.BuscarPorId(usuario)
      context become agregarAsistido(sender, asistido)
      actorbd ! mensaje

  } //def

  def respuestas(responderA: ActorRef): Receive = {
    case ServicioDAOGenerico.Insertar.Resultado(id) =>
      procesarRespuesta(responderA, Registrar.Resultado(id))

    case ServicioDAOGenerico.BuscarUno.Resultado(res) =>
      procesarRespuesta(responderA, EncontrarPorLogin.Resultado(res.map{case u: Usuario => u}))

    case ServicioDAOGenerico.BuscarPorId.Resultado(res) =>
      procesarRespuesta(responderA, Obtener.Resultado(res.map{case x: Usuario => x}))

    case ServicioDAOGenerico.ActualizarEntidad.Resultado(oid) =>
      procesarRespuesta(responderA, Actualizar.Resultado(oid))

    case ServicioDAOGenerico.Buscar.Resultados(cant, it) =>
      procesarRespuesta(responderA, Asistentes.Resultados(cant, it.toList.map{ case u: Usuario => u }))

  } //def

  def agregarAsistido(origen: ActorRef, asistido: ObjectId): Receive = {
    case ServicioDAOGenerico.BuscarPorId.Resultado(oEnt) =>
      oEnt match {
        case Some(usu: Usuario) =>
          val u = usu.copy(configuracion = usu.configuracion.copy(asistidos = (asistido :: usu.configuracion.asistidos).distinct))
          actorbd ! ServicioDAOGenerico.ActualizarEntidad(u, "_id" -> u._id)
        case _ =>
          origen ! AgregarAsistido.Resultado(None)
          harakiri()
      }

    case ServicioDAOGenerico.ActualizarEntidad.Resultado(oid) =>
      origen ! AgregarAsistido.Resultado(oid)
      harakiri()
  }
}

object ServicioUsuarios extends FabricaActoresSimple with MensajesGenericos[Usuario]{

  val clazz: Class[_] = classOf[ActorServicioUsuarios]

  case class EncontrarPorLogin(login: String)
  object EncontrarPorLogin{case class Resultado(usuario: Option[Usuario])}

  case class PorClinica(clinica: ObjectId, pagina: Int, cantidad: Int)
  object PorClinica{case class Resultados(cantidad: Int, usuarios: Iterator[Usuario])}

  case class RelacionarAClinica(usuario: ObjectId, clinica: ObjectId)
  object RelacionarAClinica{case class Resultado(id: Option[ObjectId])}

  case class ActualizarClave(usuario: Usuario)
  case class Actualizar(usuario: Usuario)
  object Actualizar { case class Resultado(id: Option[ObjectId]) }

  case class RecuperarClave(id: ObjectId, nuevaClave: String)
  object RecuperarClave

  case object UsuarioNoEncontrado extends Exception("Usuario no encontrado")

  case class Asistentes(idUsuario: ObjectId, pagina: Int, cantidad: Int)
  object Asistentes { case class Resultados(cantidad: Int, lista: List[Usuario]) }

  case class AgregarAsistido(idUsuario: ObjectId, idAsistido: ObjectId)
  object AgregarAsistido { case class Resultado(id: Option[ObjectId]) }
}