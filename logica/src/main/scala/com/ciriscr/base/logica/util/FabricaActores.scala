package com.ciriscr.base.logica.util

import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        24/10/13
 * Hora:         11:23 AM
 */
abstract class FabricaActores extends ConBitacora{

  /**
   * Crea un nuevo ActorRef del tipo A.
   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
   * del sistema.
   * @param nombre: String nombre para el actor.
   * @param fabrica: ActorRefFactory (implicit) de donde crear el actor
   * @return ActorRef
   */
  protected def nuevo(nombre: String, props: Props)(implicit fabrica: ActorRefFactory): ActorRef = nuevo(Some(nombre), props)

  /**
   * Crea un nuevo ActorRef del tipo A.
   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
   * del sistema.
   * @param nombre: String nombre para el actor.
   * @param fabrica: ActorRefFactory (implicit) de donde crear el actor
   * @return ActorRef
   */
  protected def nuevo(nombre: Option[String] = None, props: Props)(implicit fabrica: ActorRefFactory): ActorRef = {
    nombre match{
      case Some(nom) => fabrica.actorOf(props, nom)
      case None => fabrica.actorOf(props)
    } //match
  } //def
} //trait