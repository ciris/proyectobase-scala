package com.ciriscr.base.logica.elastic.entidades

import com.ciriscr.base.logica.elastic.ESAny
import com.ciriscr.base.logica.elastic.util.Imports
import Imports._
import com.ciriscr.base.entidades.usuarios.Usuario

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        16/01/14
 * Hora:         11:47 AM
 */
case class ESUsuario(id: String, nombre: String, apellidos: String, correo: String, login: String, cedula: String) extends ESAny {
  def toMap: Map[String, AnyRef] =
    Map("id" -> id, "nombre" -> nombre, "apellidos" -> apellidos, "correo" -> correo, "login" -> login, "cedula" -> cedula)
  override def toString: String = s"$nombre $apellidos"
}

object ESUsuario {
  def construir(u: Usuario): ESUsuario = ESUsuario(u._id, u.nombre, u.apellidos, u.correo, u.login, u.cedula)
}
