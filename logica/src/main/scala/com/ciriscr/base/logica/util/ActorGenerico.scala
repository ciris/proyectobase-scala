package com.ciriscr.base.logica.util

import akka.actor.Actor
import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        14/10/13
 * Hora:         11:14 AM
 *
 * Envoltorio personalizado de actores que extiende de akka.actor.Actor y de com.ciriscr.neodatos.util.logs.Loggeable.
 * Implementa el método receive rodeándolo con una especie de try/catch: si el mensaje es desconocido, lanza una excepción
 * MensajeDesconocidoException(). Para agregar nuevos casos al receive, este trait provee un método abstracto recibir.
 */
trait ActorGenerico extends Actor with ConBitacora{
  final def receive: Receive = recibir orElse mensajeDesconocido

  def recibir: Receive

  /**
   * Para el caso en que no se tenga definida la función parcial con el
   * mensaje provisto.
   * @return Receive
   */
  def mensajeDesconocido: Receive = {
    case a =>
      val mensaje = s"No sé qué hacer con este mensaje: $a"
      error(mensaje)
      val exc = new MensajeDesconocidoException(mensaje)
      sender ! akka.actor.Status.Failure(exc)
      throw exc
      harakiri()
  } //def

  def harakiri(){
    context.stop(self)
  } //def
}//trait

case class MensajeDesconocidoException(mensaje: Any) extends Exception(mensaje.toString)