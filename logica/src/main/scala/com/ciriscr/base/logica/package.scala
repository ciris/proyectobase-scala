package com.ciriscr.base

import com.ciriscr.base.util.Lector

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        14/01/14
 * Hora:         11:31 AM
 */
package object logica {

  object Imports extends Imports

  trait Imports extends Implicits
  with com.ciriscr.base.bd.Imports {

    val Props = akka.actor.Props
    val Timeout = akka.util.Timeout
    val SistemaActores = com.ciriscr.base.logica.util.SistemaActores
    val MensajeDesconocidoException = com.ciriscr.base.logica.util.MensajeDesconocidoException
    val ServicioDAOGenerico = com.ciriscr.base.logica.bd.ServicioDAOGenerico
    val Future = scala.concurrent.Future

    type ActorPreguntaRespuestaGenerico = com.ciriscr.base.logica.util.ActorPreguntaRespuestaGenerico
    type ActorMultiestadoGenerico = com.ciriscr.base.logica.util.ActorMultiestadoGenerico
    type Receive = akka.actor.Actor.Receive
    type FabricaActores = com.ciriscr.base.logica.util.FabricaActores
    type ActorGenerico = com.ciriscr.base.logica.util.ActorGenerico
    type ActorRefFactory = akka.actor.ActorRefFactory
    type Props = akka.actor.Props
    type ActorRef = akka.actor.ActorRef
    type Timeout = akka.util.Timeout
    type Lector[A, B] = com.ciriscr.base.util.Lector[A, B]
    type Future[+A] = scala.concurrent.Future[A]
    type Try[+A] = scala.util.Try[A]
    type MensajeDesconocidoException = com.ciriscr.base.logica.util.MensajeDesconocidoException
  } //trait

  object Implicits extends Implicits
  trait Implicits extends com.ciriscr.base.bd.Implicits {

    implicit def implicitsLector[A, B]: ((A) => B) => Lector[A, B] = com.ciriscr.base.util.Lector.aLector[A, B] _

  } //trait
} //package object
