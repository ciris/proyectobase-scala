package com.ciriscr.base.logica.util

import java.io.File
import javax.imageio.ImageIO

import net.coobird.thumbnailator.Thumbnails

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        6/21/14
 * Hora:         11:22 PM
 */

object ImageUtil {

  def resize(inFile: File, outFile: File, w: Int, h: Int) { Thumbnails of inFile size(w, h) toFile outFile }

  def crop(inFile: File, outFile: File, x: Int, y: Int, wight: Int, heigth: Int) {
    ImageIO.write(ImageIO.read(inFile).getSubimage(x, y, wight, heigth), "png", outFile)
  }

}
