package com.ciriscr.base.logica.elastic

import com.ciriscr.base.logica.elastic.entidades.ESUsuario
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.elastic.util.{Imports, MultiMatchQuery}
import Imports._
import com.ciriscr.base.logica.elastic.util.MultiMatchQuery

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        17/01/14
 * Hora:         11:00 AM
 */

//private class ESPruebaUsuario() extends ESActorGenerico[ESUsuario]("prueba", "usuarios") {
//
//  /**
//   * busca usuarios por el nombre, correo o login
//   * @param texto texto a buscar
//   * @return Lista con los ids de los usuarios y el texto a con nombre del usuario
//   */
//  protected def buscar(texto: String, id: => String): List[ESUsuario] = {
//    def mapearRegistro(m: Map[String, AnyRef]): Option[ESUsuario] = {
//      m.get("id").map(v =>
//        ESUsuario(v.toString, m.get("nombre").map(_.toString).getOrElse(""),
//          m.get("nombre").map(_.toString).getOrElse(""),
//          m.get("correo").map(_.toString).getOrElse(""),
//          m.get("login").map(_.toString).getOrElse(""),
//          m.get("cedula").map(_.toString).getOrElse(""))
//      )
//    }
//
//    if (texto.size >= 3) {
//      val response = ESCliente.buscar(index, tipo, MultiMatchQuery(texto, List("nombre", "apellidos", "correo", "login", "cedula")))
//      response.flatMap(mapearRegistro)
//    } else Nil
//  }
//}
//
//object ESPruebaUsuario extends FabricaActores {
//
//  /**
//   * Devuelve una función que crea un nuevo ActorRef del tipo A cuando se le aplica una conexión concreta de base de datos.
//   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
//   * del sistema.
//   * @param nombre: Option[String] nombre para el actor. Si no hay nombre, se usa el nombre por defecto de akka.
//   * @param fabrica: ActorRefFactory para instanciar los actores
//   * @return ActorRef
//   */
//  def apply(nombre: Option[String] = None)(implicit fabrica: ActorRefFactory): ActorRef = {
//    this.nuevo(nombre, Props(classOf[ESPruebaUsuario]))
//  } //def
//}
