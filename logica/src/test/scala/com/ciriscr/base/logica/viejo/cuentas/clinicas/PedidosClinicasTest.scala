package com.ciriscr.base.logica.viejo.cuentas.clinicas

import org.scalatest.FunSuite
import com.ciriscr.base.util.ConBitacora
//import com.ciriscr.mediciris.logica.Imports._
//import com.ciriscr.mediciris.logica.ValoresDefault._
//import scala.concurrent.Await
//import com.ciriscr.mediciris.entidades.util.DatosContacto
//import com.ciriscr.mediciris.entidades.cuentas.clinicas.Clinica
//import com.ciriscr.mediciris.bd.cuentas.clinicas.ClinicaDAO
//import com.ciriscr.mediciris.entidades.cuentas.Cuenta
//import org.joda.time.DateTime
//import ServicioClinicas.ClinicaCompuesta
//import com.ciriscr.mediciris.bd.cuentas.CuentaDAO
//import com.ciriscr.mediciris.bd.usuarios.UsuarioDAO
//import com.ciriscr.mediciris.entidades.usuarios.Usuario

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        21/01/14
 * Hora:         10:59 AM
 */
class PedidosClinicasTest extends FunSuite with ConBitacora{
//  lazy val pedidos = new PedidosClinicas()
//  lazy val dao = LogicaGenerica[Clinica, ClinicaDAO](ClinicaDAO())
//  lazy val daocu = LogicaGenerica[Cuenta, CuentaDAO](CuentaDAO())
//  lazy val daous = LogicaGenerica[Usuario, UsuarioDAO](UsuarioDAO())

//  test("Insertar una clínica"){
//    val prop = new ObjectId
//    val cuenta = new ObjectId
//    val clinica = Clinica("Prueba", "XX", DatosContacto(Nil, Nil), Nil, Nil, prop, cuenta)
//    val futuro = pedidos.guardar(clinica){
//      case None => fail("No se guardó la clínica")
//      case Some(id) =>
//        val res = dao.buscarUno("_id" -> id)
//        res match{
//          case Some(c) => assert(clinica === c, "No es la misma clínica")
//          case None => fail("No se encontró la clínica guardada")
//        } //match
//    } //pedido
//    Await.result(futuro, timeout.duration)
//    dao.eliminar(clinica._id)
//  } //test
//
//  test("Actualizar una clínica"){
//    datosDePrueba{ (prop, cuenta, clinica) =>
//      val n = clinica.copy(nombre = "Otro")
//      val futuro = pedidos.guardar(n){
//        case None => fail("No se actualizó la clínica")
//        case Some(id) =>
//          val res = dao.buscarUno("_id" -> id)
//          res match{
//            case Some(c) => assert(n === c, "No es la misma clínica")
//            case None => fail("No se encontró la clínica guardada")
//          } //match
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datos
//  } //test
//
//  test("Listar una clínica"){
//    datosDePrueba{ (prop, cuenta, clinica) =>
//      val futuro = pedidos.listar(prop, cuenta, 0, 0){ it =>
//        assert(it.toList === List(clinica), "EL listado no es igual")
//      } //pedidos
//      Await.result(futuro, timeout.duration)
//    } //datos
//  } //def
//
//  test("Obtener una clínica por id"){
//    datosDePrueba{ (prop, cuenta, clinica) =>
//      val futuro = pedidos.obtener(clinica._id){
//        case Some(c) => assert(c === clinica, "Las clínicas no son iguales")
//        case None => fail("No se encontró la clínica")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datos
//  } //test
//
//  test("Clínicas relacionadas del usuario"){
//    val cuenta = Cuenta("", new ObjectId, new DateTime())
//    val clinica = Clinica("", "", DatosContacto(Nil, Nil), Nil, Nil, new ObjectId, cuenta._id)
//    val usuario = Usuario("", "", "", "", "", "", true, List(clinica._id))
//    val comp = ClinicaCompuesta(clinica, cuenta)
//    daous.insertar(usuario)
//    dao.insertar(clinica)
//    daocu.insertar(cuenta)
//    try{
//      val futuro = pedidos.clinicasRelacionadas(usuario._id, 0, 0){ it =>
//        assert(List(comp) === it.toList)
//      }
//      Await.result(futuro, timeout.duration)
//      dao.eliminar(clinica)
//      daocu.eliminar(cuenta)
//      daous.eliminar(usuario)
//    }catch{
//      case e: Throwable =>
//        dao.eliminar(clinica)
//        daocu.eliminar(cuenta)
//        daous.insertar(usuario)
//        fail(e)
//    }
//  } //test
//
//
//  def datosDePrueba(procesar: (ObjectId, ObjectId, Clinica) => Any) = {
//    val prop = new ObjectId
//    val cuenta = new ObjectId
//    val clinica = Clinica("Prueba", "XX", DatosContacto(Nil, Nil), Nil, Nil, prop, cuenta)
//    dao.insertar(clinica)
//    procesar(prop, cuenta, clinica)
//    dao.eliminar(clinica._id)
//  } //def
} //test
