package com.ciriscr.base.logica.elastic

import akka.util.Timeout
import scala.reflect.ClassTag
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import com.ciriscr.base.logica.Imports._

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        16/01/14
 * Hora:         02:36 PM
 */

trait FutureResolver {

  protected implicit val sistema = SistemaActores()
  protected implicit val timeout = Timeout(10000)

  protected implicit def resolver[A, B <: Any](f: => Future[Any])(implicit tag: ClassTag[A]): A = {
    Await.result[A](f.mapTo[A], Duration(10L, "second"))
  }
}
