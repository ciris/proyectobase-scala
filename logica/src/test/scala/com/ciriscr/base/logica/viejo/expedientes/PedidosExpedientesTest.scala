package com.ciriscr.base.logica.viejo.expedientes

import com.ciriscr.base.logica.Imports._
import scala.concurrent.Await
import com.ciriscr.base.logica.ValoresDefault._
import com.ciriscr.base.entidades.expediente._
import org.scalatest.FunSuite
//import com.ciriscr.mediciris.entidades.util.DatosContacto
//import com.ciriscr.mediciris.bd.expediente.ExpedienteDAO
//import com.ciriscr.mediciris.bd.cuentas.clinicas.ClinicaDAO
//import com.ciriscr.mediciris.entidades.cuentas.clinicas.Clinica
//import com.ciriscr.mediciris.entidades.usuarios.Usuario
//import com.ciriscr.mediciris.bd.usuarios.UsuarioDAO
//import org.joda.time.DateTime
//import com.ciriscr.mediciris.daos.expediente.CitaDAO
//import com.ciriscr.mediciris.logica.pedidos.PedidosExpedientes
//import com.ciriscr.mediciris.logica.servicios.ServicioExpedientes

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        30/01/14
 * Hora:         07:40 AM
 */
class PedidosExpedientesTest extends FunSuite{

//  val pedidos = new PedidosExpedientes()
//
//  val dao = LogicaGenerica[Expediente, ExpedienteDAO](ExpedienteDAO())
//  val daoc = LogicaGenerica[Cita, CitaDAO](CitaDAO())
//
//
//  test("Agregar una cita en un calendario vacío"){
//    val cuenta, clinica, usuario, exp = new ObjectId
//    val expediente = Expediente("", "", "", cuenta, clinica, usuario)
//    val cita = Cita(new DateTime(), new DateTime, cuenta, clinica, usuario, exp)
//    val futuro = pedidos.agregarCita(expediente, cita){ res =>
//      res match{
//        case ServicioExpedientes.AgregarCita.Agregada(id) =>
//          if(id.isEmpty) fail("No se insertó la cita")
//        case a: ServicioExpedientes.AgregarCita.Error => fail(a.toString)
//      } //match
//    } //pedidos
//    try{
//      Await.result(futuro, timeout.duration)
//      dao.eliminar(expediente)
//      daoc.eliminar(cita)
//    }catch{
//      case e: Throwable =>
//        dao.eliminar(expediente)
//        daoc.eliminar(cita)
//        fail(e)
//    } //cathc
//  } //test
} //class
