package com.ciriscr.base.logica.amazon

import org.scalatest.FunSuite
import java.io.File
import org.joda.time.{DateTime, Period}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        25/03/14
 * Hora:         02:04 PM
 */

class TestMedicirisS3 extends FunSuite {

  trait S3 {
    val client = GalenisS3
  }

  test("agregar archivo") {
    new S3 {
      client.putFile(new File("logica/archivo"), None)
      client.putFile(new File("logica/archivo"), Some("prueba"))
    }
  }

  test("obtener archivo") {
    new S3 {
      client.getFile("archivo", None, "./temp1").delete()
      client.getFile("archivo", Some("prueba"), "./temp2").delete()
    }
  }

  test("borrar archivo") {
    new S3 {
      client.deleteFile("archivo", None)
      client.deleteFile("archivo", Some("prueba"))
    }
  }

  test("presigned image") {
    new S3 {
      client.putFile(new File("logica/5330c44c44ae67038a5a540f.jpeg"), Some("para firmar"))
      println(client.getPresignedURLString("5330c44c44ae67038a5a540f.jpeg", Some("para firmar"), DateTime.now().plusMinutes(15)))
    }
  }

}
