package com.ciriscr.base.logica.elastic

import akka.pattern.ask
import org.bson.types.ObjectId
import scala.concurrent.ExecutionContext.Implicits.global
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import com.ciriscr.base.logica.elastic.entidades.ESUsuario
import scala.annotation.tailrec
import scala.util.Try

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        16/01/14
 * Hora:         02:22 PM
 */

class TestOperacionesUsuarios extends FunSuite with FutureResolver with BeforeAndAfterAll {

//  val usu1 = ESUsuario(new ObjectId().toString, "Edurdo", "Obando", "eobandob@gmail.com", "eobandob@gmail.com", "111970650")
//  val usu2 = ESUsuario(new ObjectId().toString, "Eduardo", "Obando", "eobrandob@gmail.com", "eobrndob@gmail.com", "111970651")
//  val usu3 = ESUsuario(new ObjectId().toString, "Eduardo", "Obando", "eobrandob@gmail.com", "eobrandob@gmail.com", "111970652")
//  val usu4 = ESUsuario(new ObjectId().toString, "Eduardos", "Obando", "eobandob@gmail.com", "eobandob@gmail.com", "111970653")
//
//  private def borrarIndice() {
//    resolver[ESActorGenerico.BorrarIndice.Resultado, Boolean](ESPruebaUsuario() ? ESActorGenerico.BorrarIndice()).res
//    repetir(false, Nil)
//  }
//
//  private def repetir(parar: Boolean, resultado: List[String]): Boolean = {
//    repetir(parar, resultado, "obando", "")
//  }
//
//  @tailrec
//  private def repetir(parar: Boolean, resultado: List[String], texto: String, id: String): Boolean = {
//    if (!parar) {
//      val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar(texto, id)).res.map(_.id).toList.sorted
//      repetir(r == resultado, resultado, texto, id: String)
//    } else true
//  }
//
//  override def beforeAll() {
//    Try{borrarIndice()}
//    val l = List(
//      resolver[ESActorGenerico.Insertar.Resultado, ObjectId](ESPruebaUsuario() ? ESActorGenerico.Insertar(usu1))._id,
//      resolver[ESActorGenerico.Insertar.Resultado, ObjectId](ESPruebaUsuario() ? ESActorGenerico.Insertar(usu2))._id,
//      resolver[ESActorGenerico.Insertar.Resultado, ObjectId](ESPruebaUsuario() ? ESActorGenerico.Insertar(usu3))._id,
//      resolver[ESActorGenerico.Insertar.Resultado, ObjectId](ESPruebaUsuario() ? ESActorGenerico.Insertar(usu4))._id
//    )
//    repetir(false, l.sorted)
//  }
//
//  override def afterAll() {
////    ESCliente.apagar()
//    sistema.shutdown()
//  }
//
//  test("buscar usuario por texto: Ed") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Ed", "")).res.map(_.id).toList
//    assert(r.sorted === Nil)
//  }
//
//  test("buscar usuario por texto: ed") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("ed", "")).res.map(_.id).toList
//    assert(r.sorted === Nil)
//  }
//
//  test("buscar usuario por texto: Edu") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu3.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edu") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu3.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: Edua") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2.id, usu3.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edua") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2.id, usu3.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: Edur") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edur") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eob") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eob", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu3.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eoba") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eoba", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eobr") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eobr", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2.id, usu3 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eobrn") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eobrn", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2 .id).sorted)
//  }
//
//  test("eliminar el usuario 3") {
//    val r = resolver[ESActorGenerico.Eliminar.Resultado, Boolean](ESPruebaUsuario() ? ESActorGenerico.Eliminar(usu3 .id)).res
//    repetir(false, List(usu1.id, usu2.id, usu4.id).sorted)
//    assert(r)
//  }
//
//  test("buscar usuario por texto 2: Ed") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Ed", "")).res.map(_.id).toList
//    assert(r.sorted ===Nil)
//  }
//
//  test("buscar usuario por texto 2: ed") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("ed", "")).res.map(_.id).toList
//    assert(r.sorted ===Nil)
//  }
//
//  test("buscar usuario por texto 2: Edu") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edu") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: Edua") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edua") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: Edur") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edur") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eob") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eob", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu2.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eoba") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eoba", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu1.id, usu4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eobr") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eobr", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eobrn") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("eobrn", "")).res.map(_.id).toList
//    assert(r.sorted === List(usu2 .id).sorted)
//  }
//
//  test("actualizar") {
//    val a = ESPruebaUsuario() ? ESActorGenerico.Actualizar(usu1.copy(nombre = "Eduardo"))
//    val r = resolver[ESActorGenerico.Actualizar.Resultado, Boolean](a).res
//    repetir(false, List(usu1.id, usu2.id, usu4.id).sorted, "eduardo", "")
//    assert(r)
//  }
//
//  test("buscar usuario por texto 3: Edur") {
//    val r = resolver[ESActorGenerico.Buscar.Resultados[ESUsuario], ESUsuario](ESPruebaUsuario() ? ESActorGenerico.Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === Nil)
//  }
}