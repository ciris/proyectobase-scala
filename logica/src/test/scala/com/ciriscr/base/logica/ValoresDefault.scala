package com.ciriscr.base.logica

import com.ciriscr.base.logica.Imports._
import scala.concurrent.ExecutionContext

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        21/01/14
 * Hora:         11:00 AM
 */
object ValoresDefault {
  implicit lazy val sistemaactores = SistemaActores("Mediciris-Logica-Test")
  implicit lazy val timeout = Timeout(5000)
  implicit lazy val EC = ExecutionContext.Implicits.global
} //object
