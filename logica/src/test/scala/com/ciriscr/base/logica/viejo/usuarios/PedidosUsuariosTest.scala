package com.ciriscr.base.logica.viejo.usuarios

import org.scalatest.FunSuite
import com.ciriscr.base.util.ConBitacora
//import com.ciriscr.mediciris.logica.Imports._
//import com.ciriscr.mediciris.logica.ValoresDefault._
//import com.ciriscr.mediciris.entidades.usuarios.Usuario
//import com.ciriscr.mediciris.entidades.util.DatosContacto
//import scala.concurrent.Await
//import com.ciriscr.mediciris.bd.usuarios.UsuarioDAO
//import com.ciriscr.mediciris.entidades.cuentas.clinicas.Clinica
//import com.ciriscr.mediciris.bd.cuentas.clinicas.ClinicaDAO

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        24/01/14
 * Hora:         01:26 PM
 */
class PedidosUsuariosTest extends FunSuite with ConBitacora{
//  private lazy val pedidos = new PedidosUsuarios()
//  lazy val daou = LogicaGenerica[Usuario, UsuarioDAO](UsuarioDAO())
//  lazy val daoc = LogicaGenerica[Clinica, ClinicaDAO](ClinicaDAO())
//
//  test("Insertar dos usuarios con el mismo login"){
//    datosDePrueba{ usuario =>
//      pedidos.guardarUsuario(usuario){
//        case Some(id) =>
//          daou.eliminar(usuario)
//          fail("El usuario se insertó de nuevo")
//        case None =>
//      } //pedidos
//    } //Datos
//  } //test
//
//  test("Usuarios relacionados a clínicas: Sin clínica"){
//    val idc = new ObjectId()
//    val futuro = pedidos.relacionadosAClinica(idc, 0, 0){ a => if(a.nonEmpty) fail("La lista no es vacía") } //futuro
//    Await.result(futuro, timeout.duration)
//  } //test
//
//  test("Usuarios relacionados a clínicas"){
//    val idc = new ObjectId()
//    val usuario = new Usuario("Prueba", "Prueba", "prueba@ciriscr.com", "prueba", "123", "12", true, List(idc))
//    val clinica = new Clinica("Clinica", "", DatosContacto(Nil, Nil), Nil, Nil, new ObjectId(), new ObjectId(), false, Nil, false, idc)
//    daou.insertar(usuario)
//    daoc.insertar(clinica)
//
//      val futuro = pedidos.relacionadosAClinica(idc, 0, 0){ _.toList match{
//        case Nil => fail("No se encontró el usuario")
//        case head :: tail => assert(head === usuario, "El usuario no es el mismo")
//      }} //futuro
//      Await.result(futuro, timeout.duration)
//      daoc.eliminar(clinica)
//    daou.eliminar(usuario._id)
//  } //test
//
//  test("Relacionar un usuario a una clínica"){
//    datosDePrueba{ usuario =>
//      val idc = new ObjectId()
//      val futuro = pedidos.relacionarAClinica(usuario._id, idc){
//        case Some(id) => assert(id === usuario._id)
//        case None => fail("No se guardó el usuario")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//      daou.buscarPorId(usuario._id) match{
//        case Some(u) => assert(u.clinicasRelacionadas.contains(idc), "No está la clínica relacionada")
//        case None => fail("No se encontró el usuario")
//      } //match
//    } //datos
//  } //test
//
//  test("Obtener un usuario"){
//    datosDePrueba{ usuario =>
//      val futuro = pedidos.obtener(usuario._id){
//        case Some(u) => assert(u === usuario, "Los usuarios no son iguales")
//        case None => fail("No se encontró el usuario")
//      } //futuro
//      Await.result(futuro, timeout.duration)
//    } //datos
//  } //test
//
//  def datosDePrueba(procesar: Usuario => Any) = {
//    val usuario = new Usuario("Prueba", "Prueba", "prueba@ciriscr.com", "prueba", "123", "12", true, Nil)
//    daou.insertar(usuario)
//    try{
//      procesar(usuario)
//      daou.eliminar(usuario)
//    }catch{
//      case e: Throwable =>
//        fail(e)
//        daou.eliminar(usuario)
//    } //catch
//  } //test
} //class