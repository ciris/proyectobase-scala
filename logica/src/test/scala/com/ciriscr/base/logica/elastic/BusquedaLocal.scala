package com.ciriscr.base.logica.elastic

import org.scalatest.{BeforeAndAfterAll, FunSuite}
import collection.JavaConversions._
import org.elasticsearch.search.SearchHit
import com.ciriscr.base.logica.elastic.util.{MultiMatchQuery, WildcardQuery, TermQuery}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        08/01/14
 * Hora:         04:20 PM
 */

class BusquedaLocal extends FunSuite with BeforeAndAfterAll {

  var cliente = ESCliente

  override def afterAll() { cliente.apagar() }

  ignore("Buscar por usuario un tweet de twitter") {
    val a = cliente.buscar("twitter", "tweet", TermQuery("user.screen_name" -> "FerreyroCamila"))
//    println("=================================")
//    println(a.getHits.totalHits())
//    println("=================================")
//    a.getHits.hits().foreach(t => t.sourceAsMap().foreach(println))
//    println("=================================")
    assert(a.getHits.totalHits > 0)
  }

  ignore("Buscar tweet de usuarios parecidos") {
    val a = cliente.buscar("twitter", "tweet", WildcardQuery("user.screen_name" -> "Fer*"))
//    println("=================================")
//    println(a.getHits.totalHits())
//    println("=================================")
//    a.getHits.hits().foreach(t => t.sourceAsMap().foreach(println))
//    println("=================================")
    assert(a.getHits.totalHits() > 0)
  }

  ignore("Buscar tweets de usuario") {
    val a = cliente.buscar("twitter", "tweet", MultiMatchQuery("Fer", List("user.screen_name", "mention.screen_name", "retweet.user_screen_name")))
//    println("=================================")
//    println(a.getHits.totalHits())
//    println("=================================")
//    a.getHits.hits().foreach(t => t.sourceAsMap().foreach(println))
//    println("=================================")
    assert(a.getHits.totalHits() > 0)
  }

}
