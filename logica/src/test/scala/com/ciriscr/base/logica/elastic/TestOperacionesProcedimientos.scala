package com.ciriscr.base.logica.elastic

import org.scalatest.{BeforeAndAfterAll, FunSuite}
import akka.pattern.ask
import org.bson.types.ObjectId
import scala.concurrent.ExecutionContext.Implicits.global
import scala.annotation.tailrec
import com.ciriscr.base.logica.elastic.entidades.ESProcedimiento

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        20/01/14
 * Hora:         07:36 PM
 */

class TestOperacionesProcedimientos extends FunSuite with FutureResolver with BeforeAndAfterAll {
  import ESActorGenerico._

//  val pro1 = ESProcedimiento(new ObjectId().toString, "Blanqueamiento", "1")
//  val pro2 = ESProcedimiento(new ObjectId().toString, "Blanco", "1")
//  val pro3 = ESProcedimiento(new ObjectId().toString, "Blanqueamiento", "2")
//  val pro4 = ESProcedimiento(new ObjectId().toString, "Puente", "3")
//
//  private def borrarIndice() {
//    resolver[BorrarIndice.Resultado, Boolean](ESPruebaBusquedaFiltrada() ? BorrarIndice()).res
//    repetir(false, Nil)
//  }
//
//  private def repetir(parar: Boolean, resultado: List[String]): Boolean = {
//    repetir(parar, resultado, "obando", "")
//  }
//
//  @tailrec
//  private def repetir(parar: Boolean, resultado: List[String], texto: String, id: String): Boolean = {
//    if (!parar) {
//      val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar(texto, id)).res.map(_.id).toList.sorted
//      repetir(r == resultado, resultado, texto, id: String)
//    } else true
//  }
//
//  override def beforeAll() {
//    borrarIndice()
//    val l = List(
//      resolver[Insertar.Resultado, ObjectId](ESPruebaBusquedaFiltrada() ? Insertar(pro1))._id,
//      resolver[Insertar.Resultado, ObjectId](ESPruebaBusquedaFiltrada() ? Insertar(pro2))._id,
//      resolver[Insertar.Resultado, ObjectId](ESPruebaBusquedaFiltrada() ? Insertar(pro3))._id,
//      resolver[Insertar.Resultado, ObjectId](ESPruebaBusquedaFiltrada() ? Insertar(pro4))._id
//    )
//    repetir(false, l.sorted)
//  }
//
//  override def afterAll() {
//    ESCliente.apagar()
//    sistema.shutdown()
//  }
//
//  test("buscar usuario por texto: Ed") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("bl", "")).res.map(_.id).toList
//    assert(r.sorted === Nil)
//  }
//
//  test("buscar usuario por texto: Edu") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro3.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edu") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro3.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: Edua") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2.id, pro3.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edua") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2.id, pro3.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: Edur") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1 .id).sorted)
//  }
//
//  test("buscar usuario por texto: edur") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eob") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eob", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro3.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eoba") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eoba", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eobr") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eobr", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2.id, pro3 .id).sorted)
//  }
//
//  test("buscar usuario por texto: eobrn") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eobrn", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2 .id).sorted)
//  }
//
//  test("eliminar el usuario 3") {
//    val r = resolver[Eliminar.Resultado, Boolean](ESPruebaBusquedaFiltrada() ? Eliminar(pro3 .id)).res
//    repetir(false, List(pro1.id, pro2.id, pro4.id).sorted)
//    assert(r)
//  }
//
//  test("buscar usuario por texto 2: Ed") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Ed", "")).res.map(_.id).toList
//    assert(r.sorted ===Nil)
//  }
//
//  test("buscar usuario por texto 2: ed") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("ed", "")).res.map(_.id).toList
//    assert(r.sorted ===Nil)
//  }
//
//  test("buscar usuario por texto 2: Edu") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edu") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edu", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: Edua") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edua") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edua", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: Edur") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: edur") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("edur", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eob") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eob", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro2.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eoba") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eoba", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro1.id, pro4 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eobr") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eobr", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2 .id).sorted)
//  }
//
//  test("buscar usuario por texto 2: eobrn") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("eobrn", "")).res.map(_.id).toList
//    assert(r.sorted === List(pro2 .id).sorted)
//  }
//
//  test("actualizar") {
//    val a = ESPruebaBusquedaFiltrada() ? Actualizar(pro1.copy(nombre = "Eduardo"))
//    val r = resolver[Actualizar.Resultado, Boolean](a).res
//    repetir(false, List(pro1.id, pro2.id, pro4.id).sorted, "eduardo", "")
//    assert(r)
//  }
//
//  test("buscar usuario por texto 3: Edur") {
//    val r = resolver[Buscar.Resultados[ESProcedimiento], ESProcedimiento](ESPruebaBusquedaFiltrada() ? Buscar("Edur", "")).res.map(_.id).toList
//    assert(r.sorted === Nil)
//  }
}
