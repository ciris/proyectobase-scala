package com.ciriscr.base.logica.viejo.expedientes.citas

import org.scalatest.FunSuite
//import com.ciriscr.mediciris.logica.Imports._
//import com.ciriscr.mediciris.logica.ValoresDefault._
//import com.ciriscr.mediciris.daos.expediente.CitaDAO
//import com.ciriscr.mediciris.entidades.expediente.{Expediente, Cita}
//import org.joda.time.DateTime
//import scala.concurrent.Await
//import com.ciriscr.mediciris.bd.expediente.ExpedienteDAO
//import com.ciriscr.mediciris.entidades.cuentas.clinicas.Clinica
//import com.ciriscr.mediciris.bd.cuentas.clinicas.ClinicaDAO
//import com.ciriscr.mediciris.entidades.util.DatosContacto
//import com.ciriscr.mediciris.logica.viejo.expedientes.citas.entidades.CitaExpedienteClinica
//import com.ciriscr.mediciris.logica.pedidos.PedidosCitas


/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        29/01/14
 * Hora:         10:06 AM
 */
class PedidosCitasTest extends FunSuite{
//  val pedidos = new PedidosCitas()
//  lazy val dao = LogicaGenerica[Cita, CitaDAO](CitaDAO())
//  val cuenta, clinica, usuario, exp = new ObjectId
//
//  test("Guardar una cita en un nuevo calendario"){
//    val fecha = new DateTime(2014, 1, 10, 10, 0, 0 , 0)
//    val cita = Cita(fecha, fecha plusMinutes 90, cuenta, clinica, usuario, exp)
//    val futuro = pedidos.agregarCita(cita){
//      case ServicioCitas.AgregarCita.EspacioOcupado => fail("Por alguna extraña razón, el espacio ya estaba ocupado")
//      case ServicioCitas.AgregarCita.Agregada(id) => id match{
//        case Some(id) => assert(id === cita._id, "El id no es igual!")
//        case None =>
//          dao.eliminar(cita)
//          fail("No se pudo guardar la cita")
//      } //match
//    } //pedido
//    Await.result(futuro, timeout.duration)
//    dao.eliminar(cita)
//  } //test
//
//  test("Guardar una cita en un calendario con una cita en el mismo horario"){
//    datosPrueba{ cita =>
//      val otra = cita.copy(_id = new ObjectId)
//      val futuro = pedidos.agregarCita(otra){
//        case ServicioCitas.AgregarCita.EspacioOcupado =>
//        case ServicioCitas.AgregarCita.Agregada(id) =>
//          dao.eliminar(otra)
//          fail("Por alguna extraña razón, la cita se agregó de nuevo")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datosPrueba
//  } //test
//
//  test("Guardar una cita en un calendario con dos citas en los bordes (si hay campo)"){
//    datosPrueba{ anterior =>
//      val siguiente = anterior.copy(_id = new ObjectId, fechaInicio = anterior.fechaInicio plusHours 2)
//      dao.insertar(siguiente)
//      val enmedio = anterior.copy(_id = new ObjectId, fechaInicio = anterior.fechaInicio plusHours 1)
//      val futuro = pedidos.agregarCita(enmedio){
//        case ServicioCitas.AgregarCita.EspacioOcupado => fail("Por alguna extraña razón, el espacio ya estaba ocupado")
//        case ServicioCitas.AgregarCita.Agregada(id) => id match{
//          case Some(id) => assert(id === enmedio._id, "El id no es igual!")
//          case None => fail("No se pudo guardar la cita")
//        } //match
//      } //pedido
//      Await.result(futuro, timeout.duration)
//      dao.eliminar(siguiente)
//      dao.eliminar(enmedio)
//    } //datosPrueba
//  } //test
//
//  test("Guardar una cita en un calendario con una cita que inicia dentro del horario solicitado"){
//    datosPrueba{ cita =>
//      val nueva = cita.copy(_id = new ObjectId, fechaInicio = cita.fechaInicio minusMinutes 30, fechaFin = cita.fechaFin minusMinutes 30)
//      val futuro = pedidos.agregarCita(nueva){
//        case ServicioCitas.AgregarCita.EspacioOcupado =>
//        case ServicioCitas.AgregarCita.Agregada(id) =>
//          dao.eliminar(nueva)
//          fail("Por alguna extraña razón, la cita se agregó de nuevo")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datosPrueba
//  } //test
//
//  test("Guardar una cita en un calendario con una cita que termina dentro del horario solicitado"){
//    datosPrueba{ cita =>
//      val nueva = cita.copy(_id = new ObjectId, fechaInicio = cita.fechaInicio plusMinutes 30, fechaFin = cita.fechaFin plusMinutes 30)
//      val futuro = pedidos.agregarCita(nueva){
//        case ServicioCitas.AgregarCita.EspacioOcupado =>
//        case ServicioCitas.AgregarCita.Agregada(id) =>
//          dao.eliminar(nueva)
//          fail("Por alguna extraña razón, la cita se agregó de nuevo")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datosPrueba
//  } //test
//
//  test("Listar las citas de un usuario sin citas"){
//    val futuro = pedidos.mensualesPorClinicaYUsuario(new ObjectId, new ObjectId, 1){ it =>
//      if(it.nonEmpty) fail("Hay citas")
//    } //pedido
//    Await.result(futuro, timeout.duration)
//  } //test
//
//  test("Listar las citas de una clínica sin citas"){
//    val futuro = pedidos.mensualesPorClinica(new ObjectId, 1){ it =>
//      if(it.nonEmpty) fail("Hay citas")
//    } //pedido
//    Await.result(futuro, timeout.duration)
//  } //test
//
//  test("Listar las citas de un usuario"){
//    datosPrueba{ cita =>
//      val futuro = pedidos.mensualesPorClinicaYUsuario(cita._idClinica, cita._idUsuario, cita.fechaInicio.getMonthOfYear){ it =>
//        if(it.isEmpty) fail("No se encontró la cita")
//        else assert(it.toList === List(cita), "Las citas no son iguales")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datosPrueba
//  } //test
//
//  test("Listar las citas de una clínica"){
//    datosPrueba{ cita =>
//      val futuro = pedidos.mensualesPorClinica(cita._idClinica, cita.fechaInicio.getMonthOfYear){ it =>
//        if(it.isEmpty) fail("No se encontró la cita")
//        else assert(it.toList === List(cita), "Las citas no son iguales")
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //datosPrueba
//  } //test
//
//  test("Actualizar una cita"){
//    datosPrueba{ cita =>
//      val nueva = cita.copy(borrado = true)
//      val futuro = pedidos.actualizar(nueva){ res =>
//        res match{
//          case ServicioCitas.Actualizar.EspacioOcupado => fail("El filtro de la fecha no filtró la misma cita")
//          case ServicioCitas.Actualizar.Actualizada(id) =>
//            if(id.isEmpty) fail("NO se actualizó la cita")
//        } //match
//      } //pedido
//      Await.result(futuro, timeout.duration)
//    } //prueba
//  } //test
//
//  test("Agenda del usuario"){
//    val daoe = LogicaGenerica[Expediente, ExpedienteDAO](ExpedienteDAO())
//    val daoc = LogicaGenerica[Clinica, ClinicaDAO](ClinicaDAO())
//    val clinica = Clinica("", "", DatosContacto(Nil, Nil), Nil, Nil, usuario, cuenta)
//    val expediente = Expediente("", "", "", cuenta, clinica._id, usuario)
//    val cita = Cita(new DateTime, new DateTime, cuenta, clinica._id, usuario, expediente._id)
//    daoe.insertar(expediente)
//    daoc.insertar(clinica)
//    dao.insertar(cita)
//
//    val futuro = pedidos.agendaDelDia(cita.fechaInicio, usuario){ it =>
//      assert(it === List(CitaExpedienteClinica(cita, expediente, clinica)), "La agenda no coincide con lo que debería ser")
//    } //pedido
//    Await.result(futuro, timeout.duration)
//
//
//    daoe.eliminar(expediente)
//    daoc.eliminar(clinica)
//    dao.eliminar(cita)
//  } //test
//
//  def datosPrueba(procesar: Cita => Any) = {
//    val fecha = new DateTime(2014, 1, 10, 10, 0, 0 , 0)
//    val cita = Cita(fecha, fecha plusHours 1, cuenta, clinica, usuario, exp)
//    dao.insertar(cita)
//    try{
//      procesar(cita)
//      dao.eliminar(cita)
//    }catch{
//      case e: Throwable =>
//        dao.eliminar(cita)
//        fail(e)
//    } //catch
//  } //def
} //class