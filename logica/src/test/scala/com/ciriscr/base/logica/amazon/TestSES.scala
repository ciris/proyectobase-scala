package com.ciriscr.base.logica.amazon

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        27/03/14
 * Hora:         03:22 PM
 */

class TestSES extends FunSuite {
  trait base {
    val from: String = "eobandob@ciriscr.com"
    val to: List[String] = Nil
    val cc: List[String] = Nil
    val bcc: List[String] = Nil
    lazy val dir: Direcciones = Direcciones(to, cc, bcc)
    val mensaje = "<h1>me escupo</h1>"
  }

  test("envio de correo a 1"){
    new base {
      override val to = List("eduardo.obando@ciriscr.com")
      val res = AmazonEmail(from, dir, "Prueba de correo to -> 1", mensaje).send()
      println(s"################# El resultado: $res")
    }
  }

  ignore("envio de correo a 1, cc 1, bcc 1"){
    new base {
      override val to = List("eduardo.obando@ciriscr.com")
      override val cc = List("pablo.peraza@ciriscr.com")
      override val bcc = List("ezequiel.vieto@ciriscr.com")
      val res = AmazonEmail(from, dir, "Prueba de correo to -> 1, cc -> bcc -> 1",
        s"$mensaje\n\nLondo como principal, Pablo como cc y zeke com bcc").send()
//      println(res.getMessageId)
    }
  }

  ignore("envio de correo a 3"){
    new base {
      override val to = List("eduardo.obando@ciriscr.com", "pablo.peraza@ciriscr.com", "ezequiel.vieto@ciriscr.com")
      AmazonEmail(from, dir, "Prueba de correo to -> 3",
        s"mensaje \n\nLos 3 como principales").send()
    }
  }
}
