package com.ciriscr.base.logica.elastic.util

import com.ciriscr.base.logica.util.ActorGenerico
import com.ciriscr.base.logica.elastic.ESCliente

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        20/01/14
 * Hora:         08:03 PM
 */

private class GestorES extends ActorGenerico {

  def recibir: Receive = {
    case "Agregar" =>
  }

  override def postStop() {
    ESCliente.apagar()
  }
}
