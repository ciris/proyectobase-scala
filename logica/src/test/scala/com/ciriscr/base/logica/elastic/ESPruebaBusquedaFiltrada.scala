package com.ciriscr.base.logica.elastic

import com.ciriscr.base.logica.elastic.entidades.ESProcedimiento
import com.ciriscr.base.logica.Imports._
import com.ciriscr.base.logica.elastic.util.{Imports, MultiMatchQuery}
import Imports._
import com.ciriscr.base.logica.elastic.util.MultiMatchQuery

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        20/01/14
 * Hora:         07:26 PM
 */

//private class ESPruebaBusquedaFiltrada() extends ESActorGenerico[ESProcedimiento]("prueba", "procedimientos") {
//
//  /**
//   * busca usuarios por el nombre, correo o login
//   * @param texto texto a buscar
//   * @return Lista con los ids de los usuarios y el texto a con nombre del usuario
//   */
//  protected def buscar(texto: String, id: => String): List[ESProcedimiento] = {
//    def mapearRegistro(m: Map[String, AnyRef]): Option[ESProcedimiento] = {
//      m.get("id").map(v =>
//        ESProcedimiento(v.toString, m.get("nombre").map(_.toString).getOrElse(""),
//                        m.get("idClinica").map(_.toString).getOrElse(""))
//      )
//    }
//
//    if (texto.size >= 3) {
//      val response = ESCliente.buscar(index, tipo, MultiMatchQuery(texto, List("nombre")))
//      response.flatMap(mapearRegistro)
//    } else Nil
//  }
//
//
//}
//
//object ESPruebaBusquedaFiltrada extends FabricaActores {
//
//  /**
//   * Devuelve una función que crea un nuevo ActorRef del tipo A cuando se le aplica una conexión concreta de base de datos.
//   * El nuevo actor será un actor hijo del actor que invocó esta función, o si es invocado por el sistema, será actor
//   * del sistema.
//   * @param nombre: Option[String] nombre para el actor. Si no hay nombre, se usa el nombre por defecto de akka.
//   * @param fabrica: ActorRefFactory para instanciar los actores
//   * @return ActorRef
//   */
//  def apply(nombre: Option[String] = None)(implicit fabrica: ActorRefFactory): ActorRef = {
//    this.nuevo(nombre, Props(classOf[ESPruebaBusquedaFiltrada]))
//  } //def
//}
