package com.ciriscr.base.logica.amazon

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        28/04/14
 * Hora:         01:35 PM
 */
class TestCorreos extends FunSuite{
  test("Correo de recuperción"){
    import com.ciriscr.base.logica.correos.CorreoRecuperacion
    import org.bson.types.ObjectId
    new CorreoRecuperacion(new ObjectId(), "ezequiel.vieto@ciriscr.com").send()
  }
}
