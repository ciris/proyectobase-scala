package com.ciriscr.base.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        23/12/13
 * Hora:         09:42 AM
 */
trait ConBitacora{

  import org.slf4j._

  lazy val log = LoggerFactory.getLogger(super.getClass)

  def debug(message: Any){log.debug(message.toString)}

  def error(message: Any){log.error(message.toString)}

  def info(message: Any){log.info(message.toString)}

} //trait