package com.ciriscr.base.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo
 * Fecha:        14/11/13
 * Hora:         02:34 PM
 */

object Contador {

  def contador: Iterator[Int] = {
    def repeat(i: Int): Stream[Int] = i #:: repeat(i + 1)
    repeat(1).toIterator
  } //def
} //object
