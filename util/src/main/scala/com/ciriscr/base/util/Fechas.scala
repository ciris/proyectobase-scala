package com.ciriscr.base.util

import org.joda.time.{LocalDate, LocalTime}
import scala.concurrent.duration.Duration
import scala.util.Try

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        09/01/14
 * Hora:         03:19 PM
 */
object Fechas {

  import org.joda.time.format.DateTimeFormat
  import org.joda.time.DateTime

  implicit class ImplicitosDateTime(d: DateTime) {
    /**
     * Valida si la fecha corresponde al día de hoy
     * @return
     */
    def isToday: Boolean = d.inicioDeDia.isEqual(DateTime.now().inicioDeDia)

    /**
     * Valida si la fecha es antes de hoy
     * @return
     */
    def isBeforeToday: Boolean = d.isBefore(DateTime.now().inicioDeDia)

    /**
     * Valida si la fecha es después de hoy
     * @return
     */
    def isAfterToday: Boolean = d.isAfter(DateTime.now().inicioDeDia)

    /**
     * Devuelve la fecha del inicio del días (media noche exacta del mismo día)
     * @return DateTime
     */
    def inicioDeDia: DateTime = d.withTimeAtStartOfDay()

    /**
     * Devuelve la fecha del fin del día (23:59:59.999)
     * @return DateTime
     */
    def finDeDia: DateTime = (d plusDays 1).inicioDeDia minusMillis 1

    /**
     * Devuelve la fecha del primer día del mes
     * @return DateTime
     */
    def inicioDeMes: DateTime = d.dayOfMonth().withMinimumValue().inicioDeDia

    /**
     * Devuelve la fecha de último día del mes
     * @return
     */
    def finDeMes: DateTime = (d plusMonths 1).inicioDeMes minusMillis 1

    /**
     * Devuelve el inicio del día de hoy
     * @return
     */
    def hoyInicioDia: DateTime = DateTime.now().inicioDeDia

    /**
     * Devuelve el fin del día de hoy
     * @return
     */
    def hoyFinDia: DateTime = DateTime.now().finDeDia

    def inicioDeMesActual: DateTime = DateTime.now().inicioDeMes

    def finDeMesActual: DateTime = DateTime.now().finDeMes

    def trimestre: Int = d.getMonthOfYear match {
      case 1 | 2 | 3 => 1
      case 4 | 5 | 6 => 2
      case 7 | 8 | 9 => 3
      case 10 | 11 | 12 => 4
    }

    def cuatrimestre: Int = d.getMonthOfYear match {
      case 1 | 2 | 3 | 4 => 1
      case 5 | 6 | 7 | 8 => 2
      case 9 | 10 | 11 | 12 => 3
    }

    def semestre: Int = d.getMonthOfYear match {
      case 1 | 2 | 3 | 4 | 5 | 6 => 1
      case 7 | 8 | 9 | 10 | 11 | 12 => 2
    }

    def ano: Int = d.getYear

    def mes: Int = d.getMonthOfYear

    def dia: Int = d.getDayOfMonth
  }

  /**
   * Toma un dia y retorna la fecha al inicio del dia (media noche exacta)
   * @param dia: Int
   * @return DateTime
   */
  def inicioDeDia(dia: Int): DateTime = new DateTime().dayOfYear().setCopy(dia).inicioDeDia

  /**
   * Toma una fecha y retorna la fecha al final del dia (11:59:59)
   * @param dia: Int
   * @return DateTime
   */
  def finDeDia(dia: Int): DateTime = new DateTime().dayOfYear().setCopy(dia).finDeDia

  /**
   * Toma un mes y retorna la fecha al inicio del mes (media noche exacta del primer día del mes)
   * @param mes: Int
   * @return DateTime
   */
  def inicioDeMes(mes: Int): DateTime = new DateTime().monthOfYear().setCopy(mes).inicioDeMes
  def inicioDeMes(ano: Int, mes: Int): DateTime = new DateTime().monthOfYear().setCopy(mes).year().setCopy(ano).inicioDeMes

  /**
   * Toma una fecha y retorna la fecha al fin del mes (11:59:59 del último día del mes)
   * @param mes: Int
   * @return DateTime
   */
  def finDeMes(mes: Int): DateTime = new DateTime().monthOfYear().setCopy(mes).finDeMes
  def finDeMes(ano: Int, mes: Int): DateTime = new DateTime().monthOfYear().setCopy(mes).year().setCopy(ano).finDeMes


  object ISO{
    private lazy val formateadorDateTimeMillis = DateTimeFormat.forPattern("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSSZ")
    private lazy val formateadorDateTime = DateTimeFormat.forPattern("yyyy'-'MM'-'dd'T'HH':'mm':'ssZ")
    private lazy val formateadorLocalDate = DateTimeFormat.forPattern("dd' de 'MMMM', 'yyyy")
    private lazy val formateadorLocalTimeMillis = DateTimeFormat.forPattern("HH':'mm':'ss'.'SSS")
    private lazy val formateadorLocalTime = DateTimeFormat.forPattern("HH':'mm':'ss")

    /**
     * Convierte un DateTime a una representación string con el formato ISO:
     * yyyy'-'MM'-'dd'T'HH':'mm':'ssZ
     * @param fecha: DateTime a convertir
     * @return Representación string
     */
    def aString(fecha: DateTime): String = Try {
      formateadorDateTimeMillis.print(fecha)
    }.toOption.getOrElse(formateadorDateTime.print(fecha))

    def aString(fecha: LocalDate): String = formateadorLocalDate.print(fecha)

    /**
     * Convierte un string en formato ISO (yyyy'-'MM'-'dd'T'HH':'mm':'ssZ) a su representación en objeto DateTime
     * @param fecha: String a convertir
     * @return DateTime
     */
    def aDateTime(fecha: String): DateTime = Try {
      formateadorDateTimeMillis.parseDateTime(fecha)
    }.toOption.getOrElse(formateadorDateTime.parseDateTime(fecha))

    /**
     * Convierte un LocalTime a una representación string con el formato ISO:
     * 'HH':'mm':'ssZ
     * @param hora: LocalTime
     * @return String
     */
    def aString(hora: LocalTime): String = hora.toString(formateadorLocalTime)

    /**
     * Convierte un string en formato ISO (HH':'mm':'ss) a su representación en objeto LocalTime
     * @param hora: String
     * @return LocalTime
     */
    def aLocalTime(hora: String): LocalTime = LocalTime.parse(hora, DateTimeFormat.forPattern("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSSZ"))
  } //object
} //object
