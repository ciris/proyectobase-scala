package com.ciriscr.base.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        06/11/13
 * Hora:         03:28 PM
 */
case class Lector[A, B](aplicar: A => B) {
  def apply(a: A): B = aplicar(a)

  def map[C](transformacion: B => C): Lector[A, C] = (a: A) => transformacion(aplicar(a))

  def flatMap[C](aplanar: B => Lector[A, C]): Lector[A, C] = (a: A) => aplanar(aplicar(a))(a)

  def filter(filtrado: B => B): Lector[A, B] = (a: A) => filtrado(aplicar(a))

  def foreach(accion: B => Any): Lector[A, B] = {
    (a: A) =>
      val aplicado =  aplicar(a)
      accion(aplicado)
      aplicado
  } //def
} //case

object Lector{
  implicit def aLector[A, B](fun: A => B): Lector[A, B] = Lector(fun)

  def simple[A, B](b: B): Lector[A, B] = Lector(a => b)
} //object
