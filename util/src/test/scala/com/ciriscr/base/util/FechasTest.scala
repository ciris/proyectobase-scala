package com.ciriscr.base.util

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        10/01/14
 * Hora:         07:54 AM
 */
class FechasTest extends FunSuite with ConBitacora{

  import org.joda.time.DateTime
  test("Principio de día"){
    val normal = new DateTime(2014, 1, 10, 8, 1, 47, 0)
    val esperado = new DateTime(2014, 1, 10, 0, 0, 0, 0)
    val resultado = Fechas.inicioDeDia(normal)
    info(resultado.toString())
    assert(resultado.isEqual(esperado))
  } //test

  test("Fin de día"){
    val normal = new DateTime(2014, 1, 10, 8, 1, 47, 0)
    val esperado = new DateTime(2014, 1, 10, 23, 59, 59, 999)
    val resultado = Fechas.finDeDia(normal)
    info(resultado.toString())
    assert(resultado.isEqual(esperado))
  } //test

  test("Principio de mes"){
    val normal = new DateTime(2014, 1, 10, 8, 1, 47, 0)
    val esperado = new DateTime(2014, 1, 1, 0, 0, 0, 0)
    val resultado = Fechas.inicioDeMes(normal)
    info(resultado.toString())
    assert(resultado.isEqual(esperado))
  } //test

  test("Fin de mes"){
    val normal = new DateTime(2014, 1, 10, 8, 1, 47, 0)
    val esperado = new DateTime(2014, 1, 31, 23, 59, 59, 999)
    val resultado = Fechas.finDeMes(normal)
    info(resultado.toString())
    assert(resultado.isEqual(esperado))
  } //test

} //class
