#!/bin/bash

BASE_HOME=/root/base
RAMA=desarrollo
NOMBRE_PROCESO=base
PUERTO=8088
MEMORIA=512

function matarProceso {
  echo "matando los procesos: $1"
  PROCESOS=`ps -fea |grep $1 | awk '{print $2}'`
  sudo kill -9 $PROCESOS
}

cd $BASE_HOME
git fetch
git checkout $RAMA
git pull origin $RAMA
matarProceso $NOMBRE_PROCESO
export bduri="mongodb://ciriscr.com:29531"
cd aplicacion
bower install --allow-root
grunt less:build
cd ..
sbt clean "aplicacion/stage"
$BASE_HOME/aplicacion/target/universal/stage/bin/aplicacion -mem $MEMORIA -J-server -Dhttp.port=$PUERTO &
