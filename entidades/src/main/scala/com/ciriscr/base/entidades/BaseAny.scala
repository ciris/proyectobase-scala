package com.ciriscr.base.entidades

import org.bson.types.ObjectId

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        22/01/14
 * Hora:         12:08 PM
 */

trait BaseAny {
  val _id: ObjectId
}
