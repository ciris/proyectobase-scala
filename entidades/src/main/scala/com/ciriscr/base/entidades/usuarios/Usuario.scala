package com.ciriscr.base.entidades.usuarios

import org.bson.types.ObjectId
import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.entidades.util.BorradoLogico

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        13/12/13
 * Hora:         08:49 AM
 */
case class Usuario(nombre: String,
                   apellidos: String,
                   correo: String,
                   login: String,
                   password: String,
                   cedula: String,
                   configuracion: Configuracion = Configuracion(),
                   habilitado: Boolean = true,
                   borrado: Boolean = false,
                   _id: ObjectId = new ObjectId) extends BaseAny with BorradoLogico {
  require(nombre.nonEmpty)
  require(correo.nonEmpty)
  require(login.nonEmpty)
  require(password.nonEmpty)
}

case class Configuracion(enviarRecordatorios: Boolean = false, asistidos: List[ObjectId] = Nil)

object Usuario {
  lazy val PassSinCambio: String = "esto es una clave tonta para demostrar que no se debe cambiar"
}
