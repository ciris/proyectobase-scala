package com.ciriscr.base.entidades.usuarios

import org.bson.types.ObjectId
import org.joda.time.DateTime
import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.entidades.util.BorradoLogico
import com.novus.salat.annotations.Persist

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        13/12/13
 * Hora:         08:49 AM
 */
case class SolicitudRecuperarClave(_idUsuario: ObjectId,
                                   fechaCreacion: DateTime = new DateTime(),
                                   borrado: Boolean = false,
                                   _id: ObjectId = new ObjectId()) extends BaseAny with BorradoLogico
