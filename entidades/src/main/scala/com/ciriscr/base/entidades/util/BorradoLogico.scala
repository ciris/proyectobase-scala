package com.ciriscr.base.entidades.util

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        27/01/14
 * Hora:         12:03 PM
 */

trait BorradoLogico {
  val borrado: Boolean
}
