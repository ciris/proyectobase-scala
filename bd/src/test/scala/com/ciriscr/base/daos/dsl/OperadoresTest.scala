package com.ciriscr.base.daos.dsl

import org.scalatest.FunSuite
import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        12/01/14
 * Hora:         11:37 AM
 */
class OperadoresTest extends FunSuite with ConBitacora{
  import org.joda.time.DateTime
  import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers

  lazy val fecha = new DateTime()
  RegisterJodaTimeConversionHelpers()
  test("$lt"){
    import com.mongodb.DBObject
    val res: DBObject = {        
      import com.ciriscr.base.bd.Imports._
      "campo" $lt "valor"
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $lt "valor"
    }
    assert(res.toString === esperado.toString)
  }

  test("$lt de fecha"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $lt fecha
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $lt fecha
    }
    assert(res.toString === esperado.toString)
    debug(esperado)
  }

  test("$lte"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $lte "valor"
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $lte "valor"
    }
    assert(res.toString === esperado.toString)
  }

  test("$gt"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $gt "valor"
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $gt "valor"
    }
    assert(res.toString === esperado.toString)
  }

  test("$gte"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $gte "valor"
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $gte "valor"
    }
    assert(res.toString === esperado.toString)
  }

  test("$lt + $gt"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $lt "valor" $gt "valor2"
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $lt "valor" $gt "valor2"
    }
    assert(res.toString === esperado.toString)
  }

  test("$lt + $gt de fecha"){
    import com.mongodb.DBObject

    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      "campo" $lt fecha $gt fecha
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      "campo" $lt fecha $gt fecha
    }
    assert(res.toString === esperado.toString)
  }

  test("$or simple de 1"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" -> "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" -> "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$or simple de 2"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" -> "valor", "campo2" -> "valor2")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" -> "valor", "campo2" -> "valor2")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$or simple de 2: mismo campo"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" -> "valor", "campo" -> "valor2")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" -> "valor", "campo" -> "valor2")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$or de 1 $ne"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" $ne "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" $ne "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$or de 2 $ne y 1 $lt"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" $ne "valor", "campo2" $lt "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" $ne "valor", "campo2" $lt "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$or de 2 $ne y 1 $lt: mismo campo"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $or("campo" $ne "valor", "campo" $lt "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $or("campo" $ne "valor", "campo" $lt "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and simple de 1"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" -> "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" -> "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and simple de 2"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" -> "valor", "campo2" -> "valor2")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" -> "valor", "campo2" -> "valor2")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and simple de 2: mismo campo"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" -> "valor", "campo" -> "valor2")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" -> "valor", "campo" -> "valor2")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and de 1 $ne"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" $ne "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" $ne "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and de 2 $ne y 1 $lt"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" $ne "valor", "campo2" $lt "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" $ne "valor", "campo2" $lt "valor")
    }
    assert(res.toString === esperado.toString)
  } //test

  test("$and de 2 $ne y 1 $lt: mismo campo"){
    import com.mongodb.DBObject
    val res: DBObject = {
      import com.ciriscr.base.bd.Imports._
      $and("campo" $ne "valor", "campo" $lt "valor")
    }
    val esperado: DBObject = {
      import com.mongodb.casbah.Imports._
      $and("campo" $ne "valor", "campo" $lt "valor")
    }
    assert(res.toString === esperado.toString)
  } //test
}
