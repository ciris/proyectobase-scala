package com.ciriscr.base.bd.dsl

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        20/11/13
 * Hora:         11:33 AM
 */
trait Operadores {
  lazy val Vacío = Map.empty[String, Any]
  def $or(tuplas: Map[String, Any]*): Map[String, Any] = comandosAnidados("$or", tuplas)
  def $and(tuplas: Map[String, Any]*): Map[String, Any] = comandosAnidados("$and", tuplas)
  def $nor(tuplas: Map[String, Any]*): Map[String, Any] = comandosAnidados("$nor", tuplas)

  implicit def listaDeTuplas2aQuery(lista: List[(String, Any)]): Seq[Map[String, Any]] = lista.toSeq.map(a => Map(a))

  implicit class OperadoresSimples(campo: String){
    def $lt(valor: Any): Map[String, Any] = comandoEnMedio("$lt", campo -> valor)
    def $lte(valor: Any): Map[String, Any] = comandoEnMedio("$lte", campo -> valor)
    def $gt(valor: Any): Map[String, Any] = comandoEnMedio("$gt", campo -> valor)
    def $gte(valor: Any): Map[String, Any] = comandoEnMedio("$gte", campo -> valor)
    def $ne(valor: Any): Map[String, Any] = comandoEnMedio("$ne", campo -> valor)
    def $not(valor: Any): Map[String, Any] = comandoEnMedio("$not", campo -> valor)

    def <(valor: Any): Map[String, Any] = $lt(valor)
    def <=(valor: Any): Map[String, Any] = $lte(valor)
    def >(valor: Any): Map[String, Any] = $gt(valor)
    def >=(valor: Any): Map[String, Any] = $gte(valor)
  } //class

  implicit class UnionDeOperadores(mapa: Map[String, Any]){
    def $lt(valor: Any): Map[String, Any] = unir("$lt", valor)
    def $lte(valor: Any): Map[String, Any] = unir("$lte", valor)
    def $gt(valor: Any): Map[String, Any] = unir("$gt", valor)
    def $gte(valor: Any): Map[String, Any] = unir("$gte", valor)

    def <(valor: Any): Map[String, Any] = $lt(valor)
    def <=(valor: Any): Map[String, Any] = $lte(valor)
    def >(valor: Any): Map[String, Any] = $gt(valor)
    def >=(valor: Any): Map[String, Any] = $gte(valor)

    /**
     * Une un campo -> valor al mapa original.
     * Por ejemplo, si se recibe un Map("fecha" -> Map("$lt" -> "Hoy")),
     * se estaría unión el Map op -> valor Map("$gt" -> valor) con el segundo elemento
     * de la tupla original, dando como resultado: Map("fecha" -> Map("$lt" -> "hoy", "$gt" -> valor))
     * @param op Operador a agregar
     * @param valor valor del operador
     * @return Map[String, Map[String, Any] ]
     */
    private def unir(op: String, valor: Any): Map[String, Any] = {
      mapa.map{
        case (campo, mapa) =>
          mapa match{
            case a: Map[_, _] => campo -> (a ++ Map(op -> valor))
            case a => throw new IllegalArgumentException(s"Sólo se pueden unir mapas: $a")
          } //match
      } //map
    } //def
  } //class

  private def comandoEnMedio(comando: String, tupla: (String, Any)): Map[String, Any] = Map(tupla._1 -> Map(comando -> tupla._2))
  private def comandosAnidados(comando: String, comandos: Seq[Map[String, Any]]) = Map(comando -> comandos)
}//trait
