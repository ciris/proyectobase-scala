package com.ciriscr.base.bd.usuarios

import com.ciriscr.base.bd.Imports._
import com.ciriscr.base.bd.{DAOCompanion, BDBase}
import com.ciriscr.base.entidades.usuarios.SolicitudRecuperarClave

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        24/04/14
 * Hora:         11:49 AM
 */
abstract class SolicitudRecuperarClaveDAO extends BDBase[SolicitudRecuperarClave]("SolicitudRecuperarClave")

object SolicitudRecuperarClaveDAO extends DAOCompanion[SolicitudRecuperarClaveDAO]{
  protected def dao: SolicitudRecuperarClaveDAO = new SolicitudRecuperarClaveDAO {}
  def asegurarIndices(): Unit = {}
} //object
