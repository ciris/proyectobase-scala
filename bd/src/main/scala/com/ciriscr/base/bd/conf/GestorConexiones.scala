package com.ciriscr.base.bd.conf

import com.ciriscr.base.util.ConBitacora
import com.mongodb.casbah.Imports._
import com.ciriscr.base.entorno.ValoresEntorno

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        14/10/13
 * Hora:         01:30 PM
 */
object GestorConexiones extends ConBitacora{

  import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers

  private[conf] val variableEntorno = "bduri"
  RegisterJodaTimeConversionHelpers()
  lazy val conexion = {
    debug("Leyendo el string de conexión de la base de datos")
    val uri = ValoresEntorno.bd_uri
    debug(s"Conectado con $uri")
    MongoClient(MongoClientURI(uri))
  } //val

}//object