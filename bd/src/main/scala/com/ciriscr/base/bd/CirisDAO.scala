package com.ciriscr.base.bd

import com.novus.salat.dao.ModelCompanion
import com.mongodb.casbah.MongoDB
import com.novus.salat.Context
import com.ciriscr.base.bd.Imports._
import com.mongodb.casbah.Imports._
import com.ciriscr.base.bd.Imports.ObjectId

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        11/12/13
 * Hora:         01:48 PM
 */

abstract class CirisDAO[A <: AnyRef](val baseDatos: MongoDB, val nombreColeccion: String)
                                    (implicit mot: Manifest[A], mid: Manifest[ObjectId], ctx: Context)
               extends ModelCompanion[A, ObjectId]{

  import com.novus.salat.dao.{SalatDAO, DAO}

  def dao: DAO[A, ObjectId] = new SalatDAO[A, ObjectId](baseDatos(nombreColeccion))(mot, mid, ctx) {}
  /**
   * Asegura que la colección tenga el índice solicitado.
   * @param campo: String campo a indexar
   */
  def asegurarIndices(campo: String): Unit = this.dao.collection.ensureIndex(campo)

  /**
   * Asegura que la colección tenga el índice solicitado
   * @param campos: Map[String, Any] campos a indexar
   */
  def asegurarIndices(campos: Map[String, Any]): Unit = this.dao.collection.ensureIndex(campos)

  /**
   * Asegura que la colección tenga el índice solicitado
   * @param campos: Map[String, Any] campos a indexar
   * @param nombre: String nombre del índice
   */
  def asegurarIndices(campos: Map[String, Any], nombre: String, unico: Boolean = false): Unit =
    this.dao.collection.ensureIndex(campos, nombre, unico)

  /**
   * Asegura que la colección tenga el índice solicitado
   * @param campos: Map[String, Any] campos a indexar
   * @param opciones: Map[String, Any] opciones del índice
   */
  def asegurarIndices(campos: Map[String, Any], opciones: Map[String, Any]): Unit =
    this.dao.collection.ensureIndex(campos, opciones)

  /**
   * Busca los documentos que cumplan con ''query'' y actualiza el primer registro encontrado.
   * @param query   filtros de documentos
   * @param update  que actualizar
   * @return  devuelve el registro viejo antes de actualizar
   */
  def findAndModify[B <% DBObject, C <% DBObject](query: B, update: C): Option[A] =
    this.dao.collection.findAndModify(query, update).map(x => this.dao._grater.asObject(x))

  /**
   * Busca los documentos que cumplan con ''query'', lo ordena con ''sort'' y actualiza el primer registro encontrado.
   * @param query   filtros de documentos
   * @param sort    ordenado de documentos
   * @param update  que actualizar
   * @return  devuelve el registro viejo antes de actualizar
   */
  def findAndModify[B <% DBObject, C <% DBObject, D <% DBObject](query: B, sort: C, update: D): Option[A] =
    this.dao.collection.findAndModify(query, sort, update).map(x => this.dao._grater.asObject(x))

  /**
   * Busca los documentos que cumplan con ''query'', lo ordena con ''sort'' y sobre el primer documento encontrado
   * hace algo de lo siguiente:
   *  - Elimina el documento (si ''remove'' está en true)
   *  - Actualiza y devuelve el documento viejo (''update'' no vacío y ''returnNew'' false)
   *  - Actualiza y devuelve el documento nuevo (''update'' no vacío y ''returnNew'' true)
   *  - Alguna de las 2 anteriores y lo inserta en caso de no existir (''upsert'' true)
   * @param query   filtros de documentos
   * @param fields  campos que traerse
   * @param sort    ordenado de documentos
   * @param remove  si borra el documento
   * @param update  que actualizar
   * @param returnNew si devuelve el documento actualizado o el que está antes de actualizar
   * @param upsert  si lo inserta en caso de no existir
   * @return si se usa ''returnNew=true'' se devuelve el documento actualizado, sino el documento antes de actualizarse
   * @note ''remove'' en true no es compatible con ''update'' no vacío o con ''returnNew'' true
   */
  def findAndModify[B <% DBObject, C <% DBObject, D <% DBObject, E <% DBObject](query: B, fields: C, sort: D,
                                                                                remove: Boolean, update: E,
                                                                                returnNew: Boolean, upsert: Boolean): Option[A] =
    this.dao.collection.findAndModify(query, fields, sort, remove, update, returnNew, upsert).map(x => this.dao._grater.asObject(x))

  /**
   * Busca los documentos que cumplan con ''query'', lo ordena con ''sort'' y actualiza el primer registro encontrado.
   * @param query   filtros de documentos
   * @param sort    ordenado de documentos
   * @param update  que actualizar
   * @param returnNew si devuelve el documento actualizado o el que está antes de actualizar
   * @param upsert  si lo inserta en caso de no existir
   * @return el documento viejo o nuevo dependiendo del parametro ''returnNew''
   */
  def findAndModify[B <% DBObject, C <% DBObject, D <% DBObject](query: B, sort: C, update: D, returnNew: Boolean,
                                                                 upsert: Boolean): Option[A] =
    this.findAndModify(query, DBObject.empty, sort, false, update, returnNew, upsert)
} //class