package com.ciriscr.base.bd

import com.ciriscr.base.bd.Imports._
import com.novus.salat.Context
import com.ciriscr.base.entidades.BaseAny


/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        13/12/13
 * Hora:         10:18 AM
 */
abstract class BDBase[A <: BaseAny](nombreColeccion: String)
                                   (implicit mot: Manifest[A], mid: Manifest[ObjectId], ctx: Context)
  extends CirisDAO[A](GestorConexiones.conexion("Base"), nombreColeccion)
