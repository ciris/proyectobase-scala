package com.ciriscr.base.bd

import com.ciriscr.base.util.ConBitacora
import com.ciriscr.base.bd.Imports._
import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.entidades.util.BorradoLogico
import com.novus.salat.dao.SalatMongoCursor
import scala.util.Try

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        20/11/13
 * Hora:         10:34 AM
 */
sealed abstract class LogicaGenerica[A <: BaseAny with BorradoLogico, +B <: CirisDAO[A]](dao: B) extends ConBitacora{
  private def debugQuery(filtros: Map[String, Any], operacion: String): Unit = {
    import com.mongodb.casbah.Imports._
    val temp: DBObject = filtros
    debug(s"Query $operacion: $temp")
  } //def

  def contar(filtros: Map[String, Any]): Long = {
    try{
      val filters = filtros + ("borrado" -> false)
      debugQuery(filters, "contar")
      dao.count(filters)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.contar $e")
        0
    } //catch
  } //def

  /**
   * Realiza una búsqueda en el DAO con los filtros y ordenado por orden.
   * @param filtros: Map[String, Any]
   * @param orden: Map[String, Any] = Map.empty[String, Any]
   * @param limit: Int = 0 cantidad de registros a traer
   * @param skip: Int = 0 cantidad de registros a ignorar al principio
   * @return Iterator[A]
   */
  def buscar(filtros: Map[String, Any],
             orden: Map[String, Any] = Map.empty[String, Any],
             limit: Int = 0, skip: Int = 0): (Int, Iterator[A]) = {
    try{
      val filters = filtros + ("borrado" -> false)
      debugQuery(filters, "buscar")
      val it = dao.find(filters)
      val res = if(orden.isEmpty) it.skip(skip).limit(limit) else it.sort(orden).skip(skip).limit(limit)
      res.count -> res
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.buscar $e")
        0 -> Iterator.empty
    } //catch
  } //def

  /**
   * Realiza una búsqueda en el DAO con los filtros y devuelve el primer resultado encontrado.
   * @param filtros: Map[String, Any]
   * @return Option[ A ]
   */
  def buscarUno(filtros: Map[String, Any]): Option[A] = {
    try{
      val filters = filtros + ("borrado" -> false)
      debugQuery(filters, "buscarUno")
      dao.findOne(filters)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.buscarUno $e")
        None
    } //catch
  } //def

  /**
   * Realiza una búsqueda de una entidad por medio de su id
   * @param id: ObjectId
   * @return Option[ A ]
   */
  def buscarPorId(id: ObjectId): Option[A] = {
    buscarPorIdForzado(id) match {
      case Some(a) if !a.borrado => Some(a)
      case _ => None
    }
  } //def

  /**
   * Este debe ser usado con precaución, devuelva el A de fin validar si está borrado o no
   * @param id  id a buscar
   * @return  A a como está en la base de datos, fin filtros adicionales
   */
  def buscarPorIdForzado(id: ObjectId): Option[A] = {
    try{
      dao.findOneById(id)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.buscarUno $e")
        None
    } //catch
  }

  /**
   * Inserta un documento de tipo A
   * @param entidad: A
   * @return Option[ObjectId]
   */
  def insertar(entidad: A): Option[ObjectId] = {
    try{
      dao.insert(entidad, WriteConcern.Safe)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.insertar $e")
        None
    } //catch
  } //def

  /**
   * Actualiza los documentos de bases de datos con la entidad A
   * @param entidad: A nuevo valor para la bd
   * @param query: el query para seleccionar los elementos a actualizar
   * @param upsert: Si el objeto no existe, insertarlo
   * @param multi: Si se deben actualizar varios objetos
   * @return Boolean
   */
  def actualizar(entidad: A,
                 query: Map[String, Any],
                 upsert: Boolean = false,
                 multi: Boolean = false): Option[ObjectId] = {
    try{
      debugQuery(query, "actualizar")
      dao.update(query, entidad, upsert, multi, WriteConcern.Safe)
      Some(entidad._id)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.actualizar $e")
        None
    } //catch
  } //def

  /**
   * Actualiza los documentos de bases de datos con la entidad A
   * @param valor: conjuto de operaciones a realizar
   * @param query: el query para seleccionar los elementos a actualizar
   * @param upsert: Si el objeto no existe, insertarlo
   * @param multi: Si se deben actualizar varios objetos
   * @return Boolean
   */
  def actualizar(valor: Map[String, Any],
                 query: Map[String, Any],
                 upsert: Boolean,
                 multi: Boolean): List[ObjectId] = {
    try{
      val ids = if (multi) buscar(query)._2.map(_._id).toList else buscarUno(query).map(_._id).toList
      debugQuery(query, "actualizar")
      dao.update(query, valor, upsert, multi, WriteConcern.Safe)
      ids
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.actualizar $e")
        Nil
    } //catch
  } //def

  /**
   * Busca y actualiza el primer registro que cumpla con lo específicado
   * @param query
   * @param update
   * @param orden
   * @param upsert
   * @param retornarNuevo
   * @return
   */
  def buscarYActualizar(query: Map[String, Any], update: Map[String, Any], orden: Map[String, Any],
                        upsert: Boolean, retornarNuevo: Boolean): Option[A] ={
    debugQuery(query, "buscar y actualizar query ->")
    debugQuery(update, "buscar y actualizar update ->")
    debugQuery(orden, "buscar y actualizar sort ->")
    dao.findAndModify(query, orden, update, retornarNuevo, upsert)
  }

  /**
   * Elimina una entidad de la base de datos
   * @param entidad: A entidad a eliminar
   * @return Boolean
   */
  def eliminar(entidad: A): Option[ObjectId] = eliminar(entidad._id)

  /**
   * Elimina una entidad por medio de su id
   * @param id: ObjectId de la entidad a eliminar
   * @return Boolean
   */
  def eliminar(id: ObjectId): Option[ObjectId] = eliminar("_id" -> id).headOption

  /**
   * Elimina las entidades que concuerden con los filtros
   * @param filtros: Filtros para seleccionar entidades
   * @return Boolean
   */
  def eliminar(filtros: Map[String, Any]): List[ObjectId] = {
    try{
      debugQuery(filtros, "eliminar")
      actualizar(Map("$set" -> Map("borrado" -> true)), filtros, false, true)
    }catch{
      case e: Throwable =>
        error(s"LogicaGenerica.eliminar $e")
        Nil
    } //catch
  } //def

  def eliminarForzado(entidad: A): Option[ObjectId] = Try { dao.remove(entidad) }.toOption.map(r => entidad._id)
  def eliminarForzado(id: ObjectId): Option[ObjectId] = Try { dao.remove(Map("_id" -> id)) }.toOption.map(r => id)

}//trait

object LogicaGenerica{
  /**
   * Construye u objeto de lógica genérica a partir de una DAO concreto.
   * @param dao: El DAO
   * @tparam A El tipo de entidad
   * @tparam B EL tipo de DAO
   * @return LogicaGenerica[A, B]
   */
  def apply[A <: BaseAny with BorradoLogico, B <: CirisDAO[A]](dao: B): LogicaGenerica[A, B] = new LogicaGenerica[A, B](dao){}

}//object