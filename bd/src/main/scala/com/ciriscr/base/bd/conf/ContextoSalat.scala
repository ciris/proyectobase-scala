package com.ciriscr.base.bd.conf

import com.ciriscr.base.util.ConBitacora

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        25/10/13
 * Hora:         11:05 AM
 */
object ContextoSalat extends ConBitacora{

  import com.novus.salat.Context

  implicit lazy val ctx = new Context {
    val name = "ContextoSalatMediciris"
  } //def

  /**
   * Registra un ClassLoader en el contexto de Salat.
   * Util para usarlo desde Play
   * @param cargador: ClassLoader a registrar
   */
  def registrarClassLoader(cargador: ClassLoader){
    info("Registrando un cargador de clases con Salat")
    ctx.registerClassLoader(cargador)
  } //def
}//object
