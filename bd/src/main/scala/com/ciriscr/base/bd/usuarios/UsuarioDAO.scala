package com.ciriscr.base.bd.usuarios

import com.ciriscr.base.bd.Imports._
import com.ciriscr.base.entidades.usuarios.Usuario
import com.ciriscr.base.bd.{DAOCompanion, BDBase}

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        19/12/13
 * Hora:         03:38 PM
 */
abstract class UsuarioDAO extends BDBase[Usuario]("Usuario")

object UsuarioDAO extends DAOCompanion[UsuarioDAO]{
  protected def dao = new UsuarioDAO {}
  def asegurarIndices(): Unit = {
    dao.asegurarIndices("login" -> 1, "LoginUnico", true)
  } //def
} //object