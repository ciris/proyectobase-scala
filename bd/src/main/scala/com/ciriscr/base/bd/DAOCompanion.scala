package com.ciriscr.base.bd

import com.ciriscr.base.entidades.BaseAny

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        31/01/14
 * Hora:         07:54 AM
 */
trait DAOCompanion[A  <: CirisDAO[_ <: BaseAny]] {
  asegurarIndices
  def apply(): A = dao
  protected def dao: A
  def asegurarIndices(): Unit
} //trait
