package com.ciriscr.base

import com.ciriscr.base.entidades.BaseAny
import com.ciriscr.base.entidades.util.BorradoLogico

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp 
 * Fecha:        23/12/13
 * Hora:         09:50 AM
 */
package object bd {

  import com.ciriscr.base.bd.dsl.Operadores

  object Imports extends Imports

  trait Imports extends Implicits with Operadores{
    val GestorConexiones = conf.GestorConexiones
    val WriteConcern = com.mongodb.casbah.WriteConcern
    val LogicaGenerica = com.ciriscr.base.bd.LogicaGenerica

    type MongoClient = com.mongodb.casbah.MongoClient
    type ObjectId = org.bson.types.ObjectId
    type SalatMongoCursor[A <: AnyRef] = com.novus.salat.dao.SalatMongoCursor[A]
    type CirisDAO[A <: AnyRef] = com.ciriscr.base.bd.CirisDAO[A]
    type LogicaGenerica[A <: BaseAny with BorradoLogico, +B <: CirisDAO[A]] = com.ciriscr.base.bd.LogicaGenerica[A, B]


  } //trait

  trait Implicits{

    import com.ciriscr.base.bd.conf.ContextoSalat

    implicit lazy val ctx = ContextoSalat.ctx

    implicit def deTuplaAMap(tupla: (String, Any)): Map[String, Any] = Map(tupla)

    implicit def deSeqTuplaAMap(tuplas: Seq[(String, Any)]): Map[String, Any] = Map(tuplas:_*)

    import com.mongodb.casbah.Imports._
    implicit def deMapADBObject(mapa: Map[String, Any]): DBObject = MongoDBObject(mapa.toList)
    implicit def deTuplaADBObject(tupla: (String, Any)): DBObject = MongoDBObject(tupla)
    implicit def deSeqTuplaADBObject(tupla: Seq[(String, Any)]): DBObject = MongoDBObject(tupla:_*)
  } //trait
} //package object
