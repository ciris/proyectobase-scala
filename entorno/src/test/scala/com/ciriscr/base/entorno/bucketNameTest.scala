package com.ciriscr.base.entorno

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/19/14
 * Hora:         10:53 PM
 */

class bucketNameTest extends FunSuite {

  trait ValidarBucketName {
    val texto: String
    def validar: Boolean = {
      val longitudCorrecta: Boolean = texto.length >= 3 && texto.length < 64
      val isIP: Boolean = texto.matches("[0-9]{1,3}(.[0-9]{1,3}){3}")
      val tagsValidos: Boolean = texto.split('.').forall(t => {
        val contenido = t.matches("[a-z0-9-]+")
        val inicio = t.headOption.exists(c => s"$c".matches("[a-z0-9]"))
        val fin = t.lastOption.exists(c => s"$c".matches("[a-z0-9]"))
        contenido && inicio && fin
      })
      val inicio = texto.headOption.exists(c => s"$c".matches("[a-z0-9]"))
      val fin = texto.lastOption.exists(c => s"$c".matches("[a-z0-9]"))
      !isIP && longitudCorrecta && tagsValidos && inicio && fin
    }
  }

  test("1 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "myawsbucket"
      assert(validar)
    }
  }

  test("2 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "my.aws.bucket"
      assert(validar)
    }
  }

  test("3 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "myawsbucket.1"
      assert(validar)
    }
  }

  test("4 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "192.168.5.4"
      assert(!validar)
    }
  }

  test("5 bucket name test") {
    new ValidarBucketName {
      override val texto: String = ".myawsbucket"
      assert(!validar)
    }
  }

  test("6 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "myawsbucket."
      assert(!validar)
    }
  }

  test("7 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "my..examplebucket"
      assert(!validar)
    }
  }

  test("8 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "my.aws-.bucket"
      assert(!validar)
    }
  }

  test("9 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "my.aws.-bucket"
      assert(!validar)
    }
  }

  test("10 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "my"
      assert(!validar)
    }
  }

  test("11 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "myawsbucket-"
      assert(!validar)
    }
  }

  test("12 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "-myawsbucket"
      assert(!validar)
    }
  }

  test("13 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "mediciris"
      assert(validar)
    }
  }

  test("14 bucket name test") {
    new ValidarBucketName {
      override val texto: String = "Mediciris"
      assert(!validar)
    }
  }
}