package com.ciriscr.base.entorno

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/20/14
 * Hora:         5:14 PM
 */

class EspaciosValidatorTest extends FunSuite {

  trait ValidarSinEspacios {
    val texto: String

    def validar: Boolean = !texto.contains(' ')
  }

  test("1 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = "nombre.seguido"
      assert(validar)
    }
  }

  test("2 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = "nombre con espacios"
      assert(!validar)
    }
  }

  test("3 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = " espacioAdelante"
      assert(!validar)
    }
  }

  test("4 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = "espacioAtras "
      assert(!validar)
    }
  }

  test("5 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = " espacios en todos lados "
      assert(!validar)
    }
  }

  test("6 validación sin espacios") {
    new ValidarSinEspacios {
      override val texto: String = ""
      assert(validar)
    }
  }
}