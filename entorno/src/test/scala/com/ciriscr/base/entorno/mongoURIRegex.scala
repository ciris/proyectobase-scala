package com.ciriscr.base.entorno

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        7/19/14
 * Hora:         8:58 PM
 */

class mongoURIRegex extends FunSuite {

  trait Validar {
    val texto: String
    private val regex =
      "mongodb://(\\w+:\\w+@)*\\w+(\\.\\w+)*(:[0-9]{3,5})?(,\\w+(\\.\\w+)*(:[0-9]{3,5}))*(/\\w*)*(\\?(\\w+=\\w+)*(&\\w+=\\w+)+)*"

    def validar: Boolean = texto.matches(regex)
  }

  test("1 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com"
      assert(validar)
    }
  }

  test("2 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com:29531"
      assert(validar)
    }
  }

  test("3 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531,192.168.135.162:29531,192.168.135.265:29531"
      assert(validar)
    }
  }

  test("4 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531"
      assert(validar)
    }
  }

  test("5 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com:29531/pigskin"
      assert(validar)
    }
  }

  test("6 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr:29531/pigskin"
      assert(validar)
    }
  }

  test("7 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com/pigskin"
      assert(validar)
    }
  }

  test("8 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr/pigskin"
      assert(validar)
    }
  }

  test("9 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531,192.168.135.162:29531,192.168.135.265:29531/pigskin"
      assert(validar)
    }
  }

  test("10 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531/pigskin"
      assert(validar)
    }
  }

  test("11 validación regex") {
    new Validar {
      override val texto: String = "mongodb://usuario:clave@ciriscr.com:29531/pigskin"
      assert(validar)
    }
  }

  test("12 validación regex") {
    new Validar {
      override val texto: String = "mongodb://usuario:clave@192.168.134.99:29531,192.168.135.165:29531,192.168.135.162:29531,192.168.135.265:29531/pigskin"
      assert(validar)
    }
  }

  test("13 validación regex") {
    new Validar {
      override val texto: String = "mongodb://usuario:clave@192.168.134.99:29531,192.168.135.165:29531/pigskin"
      assert(validar)
    }
  }

  test("14 validación regex") {
    new Validar {
      override val texto: String = "mongodb://usuario:clave@ds045089.mongolab.com:45089/pigskin"
      assert(validar)
    }
  }

  test("15 validación regex") {
    new Validar {
      override val texto: String = "mongodb://dbuser:dbpassword@ds045089.mongolab.com:45089/eateasy"
      assert(validar)
    }
  }

  test("16 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com:29531/pigskin?replicaSet=test&connectTimeoutMS=300000"
      assert(validar)
    }
  }

  test("17 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531,192.168.135.162:29531,192.168.135.265:29531/pigskin?replicaSet=test&connectTimeoutMS=300000"
      assert(validar)
    }
  }

  test("18 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531/pigskin?replicaSet=test&connectTimeoutMS=300000"
      assert(validar)
    }
  }

  test("19 validación regex") {
    new Validar {
      override val texto: String = "mongodb://ciriscr.com:29531/?replicaSet=test&connectTimeoutMS=300000"
      assert(validar)
    }
  }

  test("20 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531,192.168.135.162:29531,192.168.135.265:29531/?replicaSet=test&connectTimeoutMS=300000&connectTimeoutMS=300000&connectTimeoutMS=300000"
      assert(validar)
    }
  }

  test("21 validación regex") {
    new Validar {
      override val texto: String = "mongodb://192.168.134.99:29531,192.168.135.165:29531/?replicaSet=test&connectTimeoutMS=300000"
      assert(validar)
    }
  }
}