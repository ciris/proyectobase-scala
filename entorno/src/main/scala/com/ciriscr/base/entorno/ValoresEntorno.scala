package com.ciriscr.base.entorno

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo
 * Fecha:        7/18/14
 * Hora:         3:46 PM
 */

object ValoresEntorno {
  private lazy val db_uri_string = "bduri"
  private lazy val amazon_bucket_string = "amazon"
  private lazy val scripts_string = "scripts"
  private lazy val ambiente_string = "ambiente"
  private lazy val aws_secret_string = "awssecret"
  private lazy val aws_access_string = "awsaccess"

  private lazy val db_uri_regex = (s: String) => Validador.validarMongoDBuri(s)
  private lazy val bucketValidator = (s: String) => Validador.validarDNSComplaince(s)
  private lazy val sinEspaciosValidator = (s: String) => Validador.validarSinEspacios(s)
  private lazy val ambienteValidator = (s: String) => sinEspaciosValidator(s) && Validador.validarSoloMayusculas(s)

  lazy val bd_uri: String = Ambiente.validarAmbienteOSino(db_uri_regex)(db_uri_string, "mongodb://localhost:27017")
  lazy val amazonBucket: String = Ambiente.validarAmbienteOSino(bucketValidator)(amazon_bucket_string, "mediciris")
  lazy val ambienteFrontEnd: String = Ambiente.validarAmbienteOSino(sinEspaciosValidator)(scripts_string, "")
  lazy val ambienteBackEnd: String = Ambiente.validarAmbienteOSino(ambienteValidator)(ambiente_string, "DEBUG")
  lazy val awsSecret: String = Ambiente.validarAmbienteOSino(sinEspaciosValidator)(aws_secret_string, "")
  lazy val awsAccess: String = Ambiente.validarAmbienteOSino(sinEspaciosValidator)(aws_access_string, "")

}

private object Validador {
  private def dnsComplaince = (s: String) => {
    val longitudCorrecta: Boolean = s.length >= 3 && s.length < 64
    val isIP: Boolean = s.matches("[0-9]{1,3}(.[0-9]{1,3}){3}")
    val tagsValidos: Boolean = s.split('.').forall(t => {
      val contenido = t.matches("[a-z0-9-]+")
      val inicio = t.headOption.exists(c => s"$c".matches("[a-z0-9]"))
      val fin = t.lastOption.exists(c => s"$c".matches("[a-z0-9]"))
      contenido && inicio && fin
    })
    println(s"$s -> $longitudCorrecta -> $isIP")
    val inicio = s.headOption.exists(c => s"$c".matches("[a-z0-9]"))
    val fin = s.lastOption.exists(c => s"$c".matches("[a-z0-9]"))
    !isIP && longitudCorrecta && tagsValidos && inicio && fin
  }
  private def mongodbURI = (s: String) =>
    s.matches("""mongodb://(\w+:\w+@)*\w+(\.\w+)*(:[0-9]{3,5})?(,\w+(\.\w+)*(:[0-9]{3,5}))*(/\w*)*(\?(\w+=\w+)*(&\w+=\w+)+)*""")

  private def sinEspacios = (s: String) => !s.contains(' ')
  private def soloMayusculas = (s: String) => s.toUpperCase == s
  private def soloMinusculas = (s: String) => s.toLowerCase == s
  private def correo = (s: String) => s.matches("""[a-z0-9\._-]+@[a-z0-9]+(.[a-z0-9]+)+""")

  def validarDNSComplaince(t: String) = dnsComplaince(t)
  def validarSinEspacios(t: String) = sinEspacios(t)
  def validarSoloMayusculas(t: String) = soloMayusculas(t)
  def validarSoloMinisculas(t: String) = soloMinusculas(t)
  def validarMongoDBuri(t: String) = mongodbURI(t)
  def validarCorreo(t: String) = correo(t)
}
