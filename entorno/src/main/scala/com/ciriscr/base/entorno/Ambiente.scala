package com.ciriscr.base.entorno

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  piousp
 * Fecha:        08/05/14
 * Hora:         11:11 AM
 */
object Ambiente {

  import scala.util.Properties

  /**
   * Lee una variable de ambiente
   * @param variable: String la variable a leer
   * @return String con el valor cargado desde el ambiente. Si el ambiente no existe, retorna el valor de sino
   */
  def ambienteOSino(variable: String, sino: String): String = validarAmbienteOSino(_ => true)(variable, sino)

  def validarAmbienteOSino(validador: (String) => Boolean)(variable: String, sino: String): String = {
    require(validador(sino))
    leerVariableDeAmbiente(variable).filter(validador).getOrElse(sino)
  }

  /**
   * Lee una variable de ambiente
   * @param variable: String la variable a leer
   * @return Some(String) si se encontró el valor en el ambiente, None en caso contrario
   */
  def leerVariableDeAmbiente(variable: String): Option[String] = Properties.envOrNone(variable)
} //object
