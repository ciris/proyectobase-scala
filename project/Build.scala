import sbt._
import Keys._
import play.Play.autoImport._
import PlayKeys._
import sbtassembly.Plugin._
import AssemblyKeys._
import com.untyped.sbtjs.Plugin.jsSettings
import com.untyped.sbtless.Plugin.lessSettings

/**
  Author: Piousp
  Company: Ciris Informatic Solutions
*/
object BaseBuild extends Build {

	override val settings: Seq[sbt.Def.Setting[_]] = (super.settings ++ Ajustes.deConstruccion).toSeq

  // -------------------------------------------------------------------------------------------------------------------
  // Root Project
  // -------------------------------------------------------------------------------------------------------------------
  lazy val root = Project(id = "Base", base = file("."), settings = Ajustes.basicos.toSeq)
    .settings(libraryDependencies ++= Dependencias.base)
    .aggregate(util, entorno, entidades, bd, logica, aplicacion)

  // -------------------------------------------------------------------------------------------------------------------
  // Modules
  // -------------------------------------------------------------------------------------------------------------------
  lazy val util = Modulo("util", "0.0.1")

  lazy val entorno = Modulo("entorno", "0.0.1", Dependencias.entorno)

  lazy val entidades = Modulo("entidades", "0.0.1", Dependencias.bd)
    .dependsOn(util, entorno)

  lazy val bd = Modulo("bd", "0.0.1", Dependencias.bd)
    .dependsOn(entidades)

  lazy val logica = Modulo("logica", "0.0.1", Dependencias.logica)
    .dependsOn(bd)

  lazy val aplicacion = ModuloPlay("aplicacion", "0.0.1", Dependencias.play)
    .dependsOn(logica)

  private def Modulo(nombre: String, versionModulo: String, extraLibs:  Traversable[ModuleID] = Nil): Project = {
    Project(nombre, base=file(nombre), settings=ajustesBasicos(versionModulo, Nil, extraLibs))
  } //def

  private def ModuloAssembly(nombre: String, versionModulo: String, extraLibs:  Traversable[ModuleID] = Nil): Project = {
    Project(nombre, base=file(nombre), settings=ajustesBasicos(versionModulo, assemblySettings, extraLibs))
  } //def

  private def ModuloPlay(nombre: String, versionModulo: String, extraLibs:  Traversable[ModuleID] = Nil): Project = {
    Project(nombre, base=file(nombre)).enablePlugins(play.PlayScala)
      .settings(ajustesBasicos(versionModulo, jsSettings ++ lessSettings, extraLibs):_*)
  } //def

  private def ajustesBasicos(versionModulo: String,
                             extraAjustes: Seq[sbt.Def.Setting[_]],
                             extraLibs:  Traversable[ModuleID]): Seq[sbt.Def.Setting[_]] = {
    Ajustes.basicos ++ extraAjustes ++
      Seq(version := versionModulo, libraryDependencies ++= Dependencias.base ++ extraLibs)
  } //def
}	//object
