resolvers += Resolver.url("untyped", url("http://ivy.untyped.com"))(Resolver.ivyStylePatterns)

addSbtPlugin("com.untyped" % "sbt-js"     % "0.6")

addSbtPlugin("com.untyped" % "sbt-less"   % "0.6")