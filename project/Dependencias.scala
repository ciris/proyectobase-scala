import sbt._
import Keys._

/**
* Author: Piousp
* Company: Ciris Informatic Solutions
*/
object Dependencias{

  private object Librerias{

    private lazy val akkaVersion = "2.3.4"
    
    lazy val config = compile("com.typesafe" % "config" % "1.2.1")
    lazy val logback = compile("ch.qos.logback" % "logback-classic" % "1.0.13")
    lazy val logbackCore = compile("ch.qos.logback" % "logback-core" % "1.0.13")
    lazy val akka = compile("com.typesafe.akka" %% "akka-actor" % akkaVersion)
    lazy val jasypt =  compile("org.jasypt" % "jasypt" % "1.9.2")
    lazy val salat = compile("com.novus" %% "salat" % "1.9.8")
    lazy val scalaTest = test("org.scalatest" %% "scalatest" % "2.2.0")
    lazy val specs2 = test("org.specs2" %% "specs2" % "2.3.11")
    lazy val elasticSearchAPI = compile("org.elasticsearch" % "elasticsearch" % "1.3.1")
    lazy val jodatime = compile("joda-time" % "joda-time" % "2.3")
    lazy val jodaconvert = compile("org.joda" % "joda-convert" % "1.2")
    lazy val aws = compile("com.amazonaws" % "aws-java-sdk" % "1.8.4")
    lazy val thumbnailator = compile("net.coobird" % "thumbnailator" % "0.4.7")

    private def compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")
    private def provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
    private def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
    private def runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
    private def container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")
  }	//object
  
//##############################################################################
//  Librerías
  import Librerias._
  lazy val base = logback ++ logbackCore ++ scalaTest ++ jodatime ++ jodaconvert
  lazy val entorno = config
  lazy val bd = salat
  lazy val logica = akka ++ jasypt ++ elasticSearchAPI ++ aws ++ thumbnailator
  lazy val play = specs2
} //Dependencias

object Resolvers{

	val basic = Seq(
		Resolver.mavenLocal,
    Resolver.typesafeRepo("releases"),
    Resolver.sonatypeRepo("releases"),
    Resolver.typesafeRepo("snapshots"),
    Resolver.sonatypeRepo("snapshots")
	)
}	//object